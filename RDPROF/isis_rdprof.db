# SHORT type Waveform 
record(waveform, "$(P)::WF:SHORT:0")
{
	field(DESC, "DM 0 ~ 0")
	field(DTYP, "asynInt16ArrayIn")
	field(INP,  "@asyn($(port), 0, 5.0) FINS_DM_READ")
	field(NELM, "500")
	field(FTVL, "SHORT")
	field(SCAN, "1 second")
}

# $READER:STATUS
# Not used
# record(bi, "READER:STATUS")

# $SYSTEM:INFO
record(bi, "$(P)::SYSTEM:INFO")
{
	field(SCAN, "1 second")
	field(DESC, "RING PROF MONS - PXI ")
	field(ZNAM, "/")
	field(ONAM, "\\")
	field(VAL, "0")
}

# $RPM:PXI_DRIVE
record(longout, "$(P)::RPM:PXI_DRIVE")
{
	field(PINI, "YES")
	field(VAL, "0")
	field(DESC, "Set motor drive (1-4)")
}

# $RPM:PXI_READ_POS
record(longin, "$(P)::RPM:PXI_READ_POS")
{
	field(SCAN, "1 second")
	field(DESC, "Read position")
}

# $RPM:PXI_SET_POS
record(longout, "$(P)::RPM:PXI_SET_POS")
{
	field(PINI, "YES")
	field(VAL, "0")
	field(DESC, "Set position")
}

# $RPM:PXI_SELECTED_CH
record(longout, "$(P)::RPM:PXI_SELECTED_CH")
{
	field(PINI, "YES")
	field(VAL, "0")
	field(DESC, "RPM in use")
}

# $RPM:PXI_STOP
record(bo, "$(P)::RPM:PXI_STOP")
{
	field(DESC, "Emergency Stop (drive)")
	field(ZNAM, " ")
	field(ONAM, " ")
}

# $RPM:PXI_GO
record(bo, "$(P)::RPM:PXI_GO")
{
	field(DESC, "Go (drive)")
	field(ZNAM, " ")
	field(ONAM, " ")
}

# $RPM:PXI_DATA
record(waveform, "$(P)::RPM:PXI_DATA")
{
	field(SCAN, "1 second")
	field(DESC, "Read RPM data")
	field(NELM, "100")
	field(FTVL, "FLOAT")
	field(EGU, "V")
	field(PREC, "2")
}

# $PERSONNEL_INTERLOCK
record(bi, "$(P)::PERSONNEL_INTERLOCK")
{
	field(SCAN, "1 second")
	field(DESC, "Personnel interlock status")
	field(ZNAM, "Incomplete")
	field(ONAM, "Complete")
	field(VAL, "0")
}

# $VACUUM_INTERLOCK
record(bi, "$(P)::VACUUM_INTERLOCK")
{
	field(SCAN, "1 second")
	field(DESC, "Vacuum interlock status")
	field(ZNAM, "Incomplete")
	field(ONAM, "Complete")
	field(VAL, "0")
}

# $30KV:SET_VOLTS
record(ao, "$(P)::30KV:SET_VOLTS")
{
	field(DESC, "Set 30KV supply voltage")
	field(EGU, "KV")
	field(PREC, "1")
	field(DRVH, "30")
	field(DRVL, "0")
}

# $30KV:READ_VOLTS
record(ai, "$(P)::30KV:READ_VOLTS")
{
	field(SCAN, "1 second")
	field(DESC, "Read 30KV supply voltage")
	field(EGU, "KV")
	field(PREC, "1")
}

# $30KV:READ_CURRENT
record(ai, "$(P)::30KV:READ_CURRENT")
{
	field(SCAN, "1 second")
	field(DESC, "Read 30KV supply current")
	field(EGU, "mA")
	field(PREC, "3")
}

# $30KV:SET_ON
record(bo, "$(P)::30KV:SET_ON")
{
	field(DESC, "Turn on 30KV")
	field(ZNAM, "OFF")
	field(ONAM, "ON")
	field(VAL, "0")
}

# $30KV:MAINS
record(bo, "$(P)::30KV:MAINS")
{
	field(DESC, "Turn Mains on to 30KV")
	field(ZNAM, "OFF")
	field(ONAM, "ON")
	field(VAL, "0")
}

# $30KV:READ_ON
record(bi, "$(P)::30KV:READ_ON")
{
	field(SCAN, "1 second")
	field(DESC, "READ 30KV on/off status")
	field(ZNAM, "OFF")
	field(ONAM, "ON")
	field(VAL, "0")
}

# $30KV:CONTROL
record(bo, "$(P)::30KV:CONTROL")
{
	field(DESC, "30KV Remote Control")
	field(ZNAM, "Local")
	field(ONAM, "Remote")
	field(VAL, "0")
}

# $30KV:POLARITY
record(bo, "$(P)::30KV:POLARITY")
{
	field(DESC, "30KV Polarity")
	field(ZNAM, "Positive")
	field(ONAM, "Negative")
	field(VAL, "0")
}

# $R5HPM1_2KV:SET_VOLTS
record(ao, "$(P)::R5HPM1_2KV:SET_VOLTS")
{
	field(DESC, "Set R5HPM1 2KV supply voltage")
	field(EGU, "KV")
	field(PREC, "2")
	field(DRVH, "2.5")
	field(DRVL, "0")
}

# $R5HPM1_2KV:READ_VOLTS
record(ai, "$(P)::R5HPM1_2KV:READ_VOLTS")
{
	field(SCAN, "1 second")
	field(DESC, "Read R5HPM1 2KV supply voltage")
	field(EGU, "KV")
	field(PREC, "2")
}

# $R5HPM1_2KV:SET_ON
record(bo, "$(P)::R5HPM1_2KV:SET_ON")
{
	field(DESC, "Turn R5HPM1 2KV on")
	field(ZNAM, "OFF")
	field(ONAM, "ON")
	field(VAL, "0")
}

# $R5HPM1_2KV:READ_ON
record(bi, "$(P)::R5HPM1_2KV:READ_ON")
{
	field(SCAN, "1 second")
	field(DESC, "READ R5HPM1 2KV on/off status")
	field(ZNAM, "OFF")
	field(ONAM, "ON")
	field(VAL, "0")
}

# $R5HPM2_2KV:SET_VOLTS
record(ao, "$(P)::R5HPM2_2KV:SET_VOLTS")
{
	field(DESC, "Set R5HPM2 2KV supply voltage")
	field(EGU, "KV")
	field(PREC, "2")
	field(DRVH, "2.5")
	field(DRVL, "0")
}

# $R5HPM2_2KV:READ_VOLTS
record(ai, "$(P)::R5HPM2_2KV:READ_VOLTS")
{
	field(SCAN, "1 second")
	field(DESC, "Read R5HPM2 2KV supply voltage")
	field(EGU, "KV")
	field(PREC, "2")
}

# $R5HPM2_2KV:SET_ON
record(bo, "$(P)::R5HPM2_2KV:SET_ON")
{
	field(DESC, "Turn R5HPM2 2KV on")
	field(ZNAM, "OFF")
	field(ONAM, "ON")
	field(VAL, "0")
}

# $R5HPM2_2KV:READ_ON
record(bi, "$(P)::R5HPM2_2KV:READ_ON")
{
	field(SCAN, "1 second")
	field(DESC, "READ R5HPM2 2KV on/off status")
	field(ZNAM, "OFF")
	field(ONAM, "ON")
	field(VAL, "0")
}

# $R6HPM1_2KV:SET_VOLTS
record(ao, "$(P)::R6HPM1_2KV:SET_VOLTS")
{
	field(DESC, "Set R6HPM1 2KV supply voltage")
	field(EGU, "KV")
	field(PREC, "2")
	field(DRVH, "2.5")
	field(DRVL, "0")
}

# $R6HPM1_2KV:READ_VOLTS
record(ai, "$(P)::R6HPM1_2KV:READ_VOLTS")
{
	field(SCAN, "1 second")
	field(DESC, "Read R6HPM1 2KV supply voltage")
	field(EGU, "KV")
	field(PREC, "2")
}

# $R6HPM1_2KV:SET_ON
record(bo, "$(P)::R6HPM1_2KV:SET_ON")
{
	field(DESC, "Turn R6HPM1 2KV on")
	field(ZNAM, "OFF")
	field(ONAM, "ON")
	field(VAL, "0")
}

# $R6HPM1_2KV:READ_ON
record(bi, "$(P)::R6HPM1_2KV:READ_ON")
{
	field(SCAN, "1 second")
	field(DESC, "READ R6HPM1 2KV on/off status")
	field(ZNAM, "OFF")
	field(ONAM, "ON")
	field(VAL, "0")
}

# $R6HPM2_2KV:SET_VOLTS
record(ao, "$(P)::R6HPM2_2KV:SET_VOLTS")
{
	field(DESC, "Set R6HPM2 2KV supply voltage")
	field(EGU, "KV")
	field(PREC, "2")
	field(DRVH, "2.5")
	field(DRVL, "0")
}

# $R6HPM2_2KV:READ_VOLTS
record(ai, "$(P)::R6HPM2_2KV:READ_VOLTS")
{
	field(SCAN, "1 second")
	field(DESC, "Read R6HPM2 2KV supply voltage")
	field(EGU, "KV")
	field(PREC, "2")
}

# $R6HPM2_2KV:SET_ON
record(bo, "$(P)::R6HPM2_2KV:SET_ON")
{
	field(DESC, "Turn R6HPM2 2KV on")
	field(ZNAM, "OFF")
	field(ONAM, "ON")
	field(VAL, "0")
}

# $R6HPM2_2KV:READ_ON
record(bi, "$(P)::R6HPM2_2KV:READ_ON")
{
	field(SCAN, "1 second")
	field(DESC, "READ R6HPM2 2KV on/off status")
	field(ZNAM, "OFF")
	field(ONAM, "ON")
	field(VAL, "0")
}

# $R6VPM1_2KV:SET_VOLTS
record(ao, "$(P)::R6VPM1_2KV:SET_VOLTS")
{
	field(DESC, "Set R6VPM1 2KV supply voltage")
	field(EGU, "KV")
	field(PREC, "2")
	field(DRVH, "2.5")
	field(DRVL, "0")
}

# $R6VPM1_2KV:READ_VOLTS
record(ai, "$(P)::R6VPM1_2KV:READ_VOLTS")
{
	field(SCAN, "1 second")
	field(DESC, "Read R6VPM1 2KV supply voltage")
	field(EGU, "KV")
	field(PREC, "2")
}

# $R6VPM1_2KV:SET_ON
record(bo, "$(P)::R6VPM1_2KV:SET_ON")
{
	field(DESC, "Turn R6VPM1 2KV on")
	field(ZNAM, "OFF")
	field(ONAM, "ON")
	field(VAL, "0")
}

# $R6VPM1_2KV:READ_ON
record(bi, "$(P)::R6VPM1_2KV:READ_ON")
{
	field(SCAN, "1 second")
	field(DESC, "READ R6VPM1 2KV on/off status")
	field(ZNAM, "OFF")
	field(ONAM, "ON")
	field(VAL, "0")
}

# $R6VPM2_2KV:SET_VOLTS
record(ao, "$(P)::R6VPM2_2KV:SET_VOLTS")
{
	field(DESC, "Set R6VPM2 2KV supply voltage")
	field(EGU, "KV")
	field(PREC, "2")
	field(DRVH, "2.5")
	field(DRVL, "0")
}

# $R6VPM2_2KV:READ_VOLTS
record(ai, "$(P)::R6VPM2_2KV:READ_VOLTS")
{
	field(SCAN, "1 second")
	field(DESC, "Read R6VPM2 2KV supply voltage")
	field(EGU, "KV")
	field(PREC, "2")
}

# $R6VPM2_2KV:SET_ON
record(bo, "$(P)::R6VPM2_2KV:SET_ON")
{
	field(DESC, "Turn R6VPM2 2KV on")
	field(ZNAM, "OFF")
	field(ONAM, "ON")
	field(VAL, "0")
}

# $R6VPM2_2KV:READ_ON
record(bi, "$(P)::R6VPM2_2KV:READ_ON")
{
	field(SCAN, "1 second")
	field(DESC, "READ R6VPM2 2KV on/off status")
	field(ZNAM, "OFF")
	field(ONAM, "ON")
	field(VAL, "0")
}