 
  Database(P): ISEPTUM
  IP Address: None
 
NUM  PV Name                                                       Address  Type        Scan        DESC                                    
  1  SYSTEM:INFO                                                            longin      1 second                                            
  2  SYSTEM:INFO:HPARAM1                                                    longout                                                         
  3  COMMON:ALARM                                                  0        longin      1 second    Injection septum - COMMON:ALARM         
  4  COMMON:DCCT_OK                                                1        bi          1 second    Injection septum - COMMON:DCCT_OK       
  5  COMMON:STOP                                                   1        bi          1 second    Injection septum - COMMON:STOP          
  6  COMMON:WATER_FLOW                                             1        bi          1 second    Injection septum - COMMON:WATER_FLOW    
  7  COMMON:MAGNET_OVERTEMP                                        1        bi          1 second    Injection septum - COMMON:MAGNET_OVERTEMP
  8  COMMON:MAGNET_FLOW                                            1        bi          1 second    Injection septum - COMMON:MAGNET_FLOW   
  9  COMMON:POWER_ON                                               1        bi          1 second    Injection septum - COMMON:POWER_ON      
 10  COMMON:EARTH_LEAKAGE                                          1        bi          1 second    Injection septum - COMMON:EARTH_LEAKAGE 
 11  COMMON:MAINS_OVERVOLT                                         1        bi          1 second    Injection septum - COMMON:MAINS_OVERVOLT
 12  COMMON:MAINS_PHASE                                            1        bi          1 second    Injection septum - COMMON:MAINS_PHASE   
 13  COMMON:DOOR_OPEN                                              1        bi          1 second    Injection septum - COMMON:DOOR_OPEN     
 14  COMMON:FAST_TRIP                                              1        bi          1 second    Injection septum - COMMON:FAST_TRIP     
 15  COMMON:FILTER_ALARM                                           1        bi          1 second    Injection septum - COMMON:FILTER_ALARM  
 16  COMMON:READ_CURRENT                                           2        ai          1 second    Injection septum - COMMON:READ_CURRENT  
 17  COMMON:READ_CURRENT_SCALED                                    2        ai          1 second    Injection septum - COMMON:READ_CURRENT_SCALED
 18  COMMON:AD5                                                    3        longin      1 second    Injection septum - COMMON:AD5           
 19  COMMON:READ_VOLTAGE                                           2        ai          1 second    Injection septum - COMMON:READ_VOLTAGE  
 20  COMMON:NCONVERTERS                                            4        longin      1 second    Injection septum - COMMON:NCONVERTERS   
 21  COMMON:ALARM_SUMMARY                                          5        longin      1 second    Injection septum - COMMON:ALARM_SUMMARY 
 22  COMMON:RESET                                                           bo                      Injection septum - COMMON:RESET         
 23  COMMON:RESET:HPARAM1                                                   longout                                                         
 24  COMMON:SET_POWER_ON                                                    bo                      Injection septum - COMMON:SET_POWER_ON  
 25  COMMON:SET_POWER_ON:HPARAM1                                            longout                                                         
 26  COMMON:SET_CURRENT                                                     ao                      Injection septum - COMMON:SET_CURRENT   
 27  COMMON:SET_CURRENT_RAMPED                                              ao                      Injection septum - COMMON:SET_CURRENT_RAMPED
 28  COMMON:SET_CURRENT_RAMPED:HPARAM1                                      longout                                                         
 29  CONVERTER_01:READ_CURRENT                                     2        ao                      Injection septum - CONVERTER_01:READ_CURRENT
 30  CONVERTER_01:ALARM                                            0        longout                 Injection septum - CONVERTER_01:ALARM   
 31  CONVERTER_01:NTC                                              3        longout                 Injection septum - CONVERTER_01:NTC     
 32  CONVERTER_01:OVER_CURRENT                                     1        bo                      Injection septum - CONVERTER_01:OVER_CURRENT
 33  CONVERTER_01:WATER_FLOW                                       1        bo                      Injection septum - CONVERTER_01:WATER_FLOW
 34  CONVERTER_01:AC_FUSE                                          1        bo                      Injection septum - CONVERTER_01:AC_FUSE 
 35  CONVERTER_01:TRANSFORMER_HOT                                  1        bo                      Injection septum - CONVERTER_01:TRANSFORMER_HOT
 36  CONVERTER_01:IGBT_HOT                                         1        bo                      Injection septum - CONVERTER_01:IGBT_HOT
 37  CONVERTER_01:POWER_ON                                         1        bo                      Injection septum - CONVERTER_01:POWER_ON
 38  CONVERTER_01:DC_FUSE                                          1        bo                      Injection septum - CONVERTER_01:DC_FUSE 
 39  CONVERTER_01:DC_LOW_VOLTS                                     1        bo                      Injection septum - CONVERTER_01:DC_LOW_VOLTS
 40  CONVERTER_01:PRECHARGE                                        1        bo                      Injection septum - CONVERTER_01:PRECHARGE
 41  CONVERTER_01:CONTACTOR                                        1        bo                      Injection septum - CONVERTER_01:CONTACTOR
 42  CONVERTER_02:READ_CURRENT                                     2        ao                      Injection septum - CONVERTER_02:READ_CURRENT
 43  CONVERTER_02:ALARM                                            0        longout                 Injection septum - CONVERTER_02:ALARM   
 44  CONVERTER_02:NTC                                              3        longout                 Injection septum - CONVERTER_02:NTC     
 45  CONVERTER_02:OVER_CURRENT                                     1        bo                      Injection septum - CONVERTER_02:OVER_CURRENT
 46  CONVERTER_02:WATER_FLOW                                       1        bo                      Injection septum - CONVERTER_02:WATER_FLOW
 47  CONVERTER_02:AC_FUSE                                          1        bo                      Injection septum - CONVERTER_02:AC_FUSE 
 48  CONVERTER_02:TRANSFORMER_HOT                                  1        bo                      Injection septum - CONVERTER_02:TRANSFORMER_HOT
 49  CONVERTER_02:IGBT_HOT                                         1        bo                      Injection septum - CONVERTER_02:IGBT_HOT
 50  CONVERTER_02:POWER_ON                                         1        bo                      Injection septum - CONVERTER_02:POWER_ON
 51  CONVERTER_02:DC_FUSE                                          1        bo                      Injection septum - CONVERTER_02:DC_FUSE 
 52  CONVERTER_02:DC_LOW_VOLTS                                     1        bo                      Injection septum - CONVERTER_02:DC_LOW_VOLTS
 53  CONVERTER_02:PRECHARGE                                        1        bo                      Injection septum - CONVERTER_02:PRECHARGE
 54  CONVERTER_02:CONTACTOR                                        1        bo                      Injection septum - CONVERTER_02:CONTACTOR
 55  CONVERTER_03:READ_CURRENT                                     2        ao                      Injection septum - CONVERTER_03:READ_CURRENT
 56  CONVERTER_03:ALARM                                            0        longout                 Injection septum - CONVERTER_03:ALARM   
 57  CONVERTER_03:NTC                                              3        longout                 Injection septum - CONVERTER_03:NTC     
 58  CONVERTER_03:OVER_CURRENT                                     1        bo                      Injection septum - CONVERTER_03:OVER_CURRENT
 59  CONVERTER_03:WATER_FLOW                                       1        bo                      Injection septum - CONVERTER_03:WATER_FLOW
 60  CONVERTER_03:AC_FUSE                                          1        bo                      Injection septum - CONVERTER_03:AC_FUSE 
 61  CONVERTER_03:TRANSFORMER_HOT                                  1        bo                      Injection septum - CONVERTER_03:TRANSFORMER_HOT
 62  CONVERTER_03:IGBT_HOT                                         1        bo                      Injection septum - CONVERTER_03:IGBT_HOT
 63  CONVERTER_03:POWER_ON                                         1        bo                      Injection septum - CONVERTER_03:POWER_ON
 64  CONVERTER_03:DC_FUSE                                          1        bo                      Injection septum - CONVERTER_03:DC_FUSE 
 65  CONVERTER_03:DC_LOW_VOLTS                                     1        bo                      Injection septum - CONVERTER_03:DC_LOW_VOLTS
 66  CONVERTER_03:PRECHARGE                                        1        bo                      Injection septum - CONVERTER_03:PRECHARGE
 67  CONVERTER_03:CONTACTOR                                        1        bo                      Injection septum - CONVERTER_03:CONTACTOR
 68  CONVERTER_04:READ_CURRENT                                     2        ao                      Injection septum - CONVERTER_04:READ_CURRENT
 69  CONVERTER_04:ALARM                                            0        longout                 Injection septum - CONVERTER_04:ALARM   
 70  CONVERTER_04:NTC                                              3        longout                 Injection septum - CONVERTER_04:NTC     
 71  CONVERTER_04:OVER_CURRENT                                     1        bo                      Injection septum - CONVERTER_04:OVER_CURRENT
 72  CONVERTER_04:WATER_FLOW                                       1        bo                      Injection septum - CONVERTER_04:WATER_FLOW
 73  CONVERTER_04:AC_FUSE                                          1        bo                      Injection septum - CONVERTER_04:AC_FUSE 
 74  CONVERTER_04:TRANSFORMER_HOT                                  1        bo                      Injection septum - CONVERTER_04:TRANSFORMER_HOT
 75  CONVERTER_04:IGBT_HOT                                         1        bo                      Injection septum - CONVERTER_04:IGBT_HOT
 76  CONVERTER_04:POWER_ON                                         1        bo                      Injection septum - CONVERTER_04:POWER_ON
 77  CONVERTER_04:DC_FUSE                                          1        bo                      Injection septum - CONVERTER_04:DC_FUSE 
 78  CONVERTER_04:DC_LOW_VOLTS                                     1        bo                      Injection septum - CONVERTER_04:DC_LOW_VOLTS
 79  CONVERTER_04:PRECHARGE                                        1        bo                      Injection septum - CONVERTER_04:PRECHARGE
 80  CONVERTER_04:CONTACTOR                                        1        bo                      Injection septum - CONVERTER_04:CONTACTOR
 81  CONVERTER_05:READ_CURRENT                                     2        ao                      Injection septum - CONVERTER_05:READ_CURRENT
 82  CONVERTER_05:ALARM                                            0        longout                 Injection septum - CONVERTER_05:ALARM   
 83  CONVERTER_05:NTC                                              3        longout                 Injection septum - CONVERTER_05:NTC     
 84  CONVERTER_05:OVER_CURRENT                                     1        bo                      Injection septum - CONVERTER_05:OVER_CURRENT
 85  CONVERTER_05:WATER_FLOW                                       1        bo                      Injection septum - CONVERTER_05:WATER_FLOW
 86  CONVERTER_05:AC_FUSE                                          1        bo                      Injection septum - CONVERTER_05:AC_FUSE 
 87  CONVERTER_05:TRANSFORMER_HOT                                  1        bo                      Injection septum - CONVERTER_05:TRANSFORMER_HOT
 88  CONVERTER_05:IGBT_HOT                                         1        bo                      Injection septum - CONVERTER_05:IGBT_HOT
 89  CONVERTER_05:POWER_ON                                         1        bo                      Injection septum - CONVERTER_05:POWER_ON
 90  CONVERTER_05:DC_FUSE                                          1        bo                      Injection septum - CONVERTER_05:DC_FUSE 
 91  CONVERTER_05:DC_LOW_VOLTS                                     1        bo                      Injection septum - CONVERTER_05:DC_LOW_VOLTS
 92  CONVERTER_05:PRECHARGE                                        1        bo                      Injection septum - CONVERTER_05:PRECHARGE
 93  CONVERTER_05:CONTACTOR                                        1        bo                      Injection septum - CONVERTER_05:CONTACTOR
 94  CONVERTER_06:READ_CURRENT                                     2        ao                      Injection septum - CONVERTER_06:READ_CURRENT
 95  CONVERTER_06:ALARM                                            0        longout                 Injection septum - CONVERTER_06:ALARM   
 96  CONVERTER_06:NTC                                              3        longout                 Injection septum - CONVERTER_06:NTC     
 97  CONVERTER_06:OVER_CURRENT                                     1        bo                      Injection septum - CONVERTER_06:OVER_CURRENT
 98  CONVERTER_06:WATER_FLOW                                       1        bo                      Injection septum - CONVERTER_06:WATER_FLOW
 99  CONVERTER_06:AC_FUSE                                          1        bo                      Injection septum - CONVERTER_06:AC_FUSE 
100  CONVERTER_06:TRANSFORMER_HOT                                  1        bo                      Injection septum - CONVERTER_06:TRANSFORMER_HOT
101  CONVERTER_06:IGBT_HOT                                         1        bo                      Injection septum - CONVERTER_06:IGBT_HOT
102  CONVERTER_06:POWER_ON                                         1        bo                      Injection septum - CONVERTER_06:POWER_ON
103  CONVERTER_06:DC_FUSE                                          1        bo                      Injection septum - CONVERTER_06:DC_FUSE 
104  CONVERTER_06:DC_LOW_VOLTS                                     1        bo                      Injection septum - CONVERTER_06:DC_LOW_VOLTS
105  CONVERTER_06:PRECHARGE                                        1        bo                      Injection septum - CONVERTER_06:PRECHARGE
106  CONVERTER_06:CONTACTOR                                        1        bo                      Injection septum - CONVERTER_06:CONTACTOR
107  CONVERTER_07:READ_CURRENT                                     2        ao                      Injection septum - CONVERTER_07:READ_CURRENT
108  CONVERTER_07:ALARM                                            0        longout                 Injection septum - CONVERTER_07:ALARM   
109  CONVERTER_07:NTC                                              3        longout                 Injection septum - CONVERTER_07:NTC     
110  CONVERTER_07:OVER_CURRENT                                     1        bo                      Injection septum - CONVERTER_07:OVER_CURRENT
111  CONVERTER_07:WATER_FLOW                                       1        bo                      Injection septum - CONVERTER_07:WATER_FLOW
112  CONVERTER_07:AC_FUSE                                          1        bo                      Injection septum - CONVERTER_07:AC_FUSE 
113  CONVERTER_07:TRANSFORMER_HOT                                  1        bo                      Injection septum - CONVERTER_07:TRANSFORMER_HOT
114  CONVERTER_07:IGBT_HOT                                         1        bo                      Injection septum - CONVERTER_07:IGBT_HOT
115  CONVERTER_07:POWER_ON                                         1        bo                      Injection septum - CONVERTER_07:POWER_ON
116  CONVERTER_07:DC_FUSE                                          1        bo                      Injection septum - CONVERTER_07:DC_FUSE 
117  CONVERTER_07:DC_LOW_VOLTS                                     1        bo                      Injection septum - CONVERTER_07:DC_LOW_VOLTS
118  CONVERTER_07:PRECHARGE                                        1        bo                      Injection septum - CONVERTER_07:PRECHARGE
119  CONVERTER_07:CONTACTOR                                        1        bo                      Injection septum - CONVERTER_07:CONTACTOR
120  CONVERTER_08:READ_CURRENT                                     2        ao                      Injection septum - CONVERTER_08:READ_CURRENT
121  CONVERTER_08:ALARM                                            0        longout                 Injection septum - CONVERTER_08:ALARM   
122  CONVERTER_08:NTC                                              3        longout                 Injection septum - CONVERTER_08:NTC     
123  CONVERTER_08:OVER_CURRENT                                     1        bo                      Injection septum - CONVERTER_08:OVER_CURRENT
124  CONVERTER_08:WATER_FLOW                                       1        bo                      Injection septum - CONVERTER_08:WATER_FLOW
125  CONVERTER_08:AC_FUSE                                          1        bo                      Injection septum - CONVERTER_08:AC_FUSE 
126  CONVERTER_08:TRANSFORMER_HOT                                  1        bo                      Injection septum - CONVERTER_08:TRANSFORMER_HOT
127  CONVERTER_08:IGBT_HOT                                         1        bo                      Injection septum - CONVERTER_08:IGBT_HOT
128  CONVERTER_08:POWER_ON                                         1        bo                      Injection septum - CONVERTER_08:POWER_ON
129  CONVERTER_08:DC_FUSE                                          1        bo                      Injection septum - CONVERTER_08:DC_FUSE 
130  CONVERTER_08:DC_LOW_VOLTS                                     1        bo                      Injection septum - CONVERTER_08:DC_LOW_VOLTS
131  CONVERTER_08:PRECHARGE                                        1        bo                      Injection septum - CONVERTER_08:PRECHARGE
132  CONVERTER_08:CONTACTOR                                        1        bo                      Injection septum - CONVERTER_08:CONTACTOR
133  CONVERTER_09:READ_CURRENT                                     2        ao                      Injection septum - CONVERTER_09:READ_CURRENT
134  CONVERTER_09:ALARM                                            0        longout                 Injection septum - CONVERTER_09:ALARM   
135  CONVERTER_09:NTC                                              3        longout                 Injection septum - CONVERTER_09:NTC     
136  CONVERTER_09:OVER_CURRENT                                     1        bo                      Injection septum - CONVERTER_09:OVER_CURRENT
137  CONVERTER_09:WATER_FLOW                                       1        bo                      Injection septum - CONVERTER_09:WATER_FLOW
138  CONVERTER_09:AC_FUSE                                          1        bo                      Injection septum - CONVERTER_09:AC_FUSE 
139  CONVERTER_09:TRANSFORMER_HOT                                  1        bo                      Injection septum - CONVERTER_09:TRANSFORMER_HOT
140  CONVERTER_09:IGBT_HOT                                         1        bo                      Injection septum - CONVERTER_09:IGBT_HOT
141  CONVERTER_09:POWER_ON                                         1        bo                      Injection septum - CONVERTER_09:POWER_ON
142  CONVERTER_09:DC_FUSE                                          1        bo                      Injection septum - CONVERTER_09:DC_FUSE 
143  CONVERTER_09:DC_LOW_VOLTS                                     1        bo                      Injection septum - CONVERTER_09:DC_LOW_VOLTS
144  CONVERTER_09:PRECHARGE                                        1        bo                      Injection septum - CONVERTER_09:PRECHARGE
145  CONVERTER_09:CONTACTOR                                        1        bo                      Injection septum - CONVERTER_09:CONTACTOR
146  CONVERTER_10:READ_CURRENT                                     2        ao                      Injection septum - CONVERTER_10:READ_CURRENT
147  CONVERTER_10:ALARM                                            0        longout                 Injection septum - CONVERTER_10:ALARM   
148  CONVERTER_10:NTC                                              3        longout                 Injection septum - CONVERTER_10:NTC     
149  CONVERTER_10:OVER_CURRENT                                     1        bo                      Injection septum - CONVERTER_10:OVER_CURRENT
150  CONVERTER_10:WATER_FLOW                                       1        bo                      Injection septum - CONVERTER_10:WATER_FLOW
151  CONVERTER_10:AC_FUSE                                          1        bo                      Injection septum - CONVERTER_10:AC_FUSE 
152  CONVERTER_10:TRANSFORMER_HOT                                  1        bo                      Injection septum - CONVERTER_10:TRANSFORMER_HOT
153  CONVERTER_10:IGBT_HOT                                         1        bo                      Injection septum - CONVERTER_10:IGBT_HOT
154  CONVERTER_10:POWER_ON                                         1        bo                      Injection septum - CONVERTER_10:POWER_ON
155  CONVERTER_10:DC_FUSE                                          1        bo                      Injection septum - CONVERTER_10:DC_FUSE 
156  CONVERTER_10:DC_LOW_VOLTS                                     1        bo                      Injection septum - CONVERTER_10:DC_LOW_VOLTS
157  CONVERTER_10:PRECHARGE                                        1        bo                      Injection septum - CONVERTER_10:PRECHARGE
158  CONVERTER_10:CONTACTOR                                        1        bo                      Injection septum - CONVERTER_10:CONTACTOR
159  CONVERTER_11:READ_CURRENT                                     2        ao                      Injection septum - CONVERTER_11:READ_CURRENT
160  CONVERTER_11:ALARM                                            0        longout                 Injection septum - CONVERTER_11:ALARM   
161  CONVERTER_11:NTC                                              3        longout                 Injection septum - CONVERTER_11:NTC     
162  CONVERTER_11:OVER_CURRENT                                     1        bo                      Injection septum - CONVERTER_11:OVER_CURRENT
163  CONVERTER_11:WATER_FLOW                                       1        bo                      Injection septum - CONVERTER_11:WATER_FLOW
164  CONVERTER_11:AC_FUSE                                          1        bo                      Injection septum - CONVERTER_11:AC_FUSE 
165  CONVERTER_11:TRANSFORMER_HOT                                  1        bo                      Injection septum - CONVERTER_11:TRANSFORMER_HOT
166  CONVERTER_11:IGBT_HOT                                         1        bo                      Injection septum - CONVERTER_11:IGBT_HOT
167  CONVERTER_11:POWER_ON                                         1        bo                      Injection septum - CONVERTER_11:POWER_ON
168  CONVERTER_11:DC_FUSE                                          1        bo                      Injection septum - CONVERTER_11:DC_FUSE 
169  CONVERTER_11:DC_LOW_VOLTS                                     1        bo                      Injection septum - CONVERTER_11:DC_LOW_VOLTS
170  CONVERTER_11:PRECHARGE                                        1        bo                      Injection septum - CONVERTER_11:PRECHARGE
171  CONVERTER_11:CONTACTOR                                        1        bo                      Injection septum - CONVERTER_11:CONTACTOR
172  CONVERTER_12:READ_CURRENT                                     2        ao                      Injection septum - CONVERTER_12:READ_CURRENT
173  CONVERTER_12:ALARM                                            0        longout                 Injection septum - CONVERTER_12:ALARM   
174  CONVERTER_12:NTC                                              3        longout                 Injection septum - CONVERTER_12:NTC     
175  CONVERTER_12:OVER_CURRENT                                     1        bo                      Injection septum - CONVERTER_12:OVER_CURRENT
176  CONVERTER_12:WATER_FLOW                                       1        bo                      Injection septum - CONVERTER_12:WATER_FLOW
177  CONVERTER_12:AC_FUSE                                          1        bo                      Injection septum - CONVERTER_12:AC_FUSE 
178  CONVERTER_12:TRANSFORMER_HOT                                  1        bo                      Injection septum - CONVERTER_12:TRANSFORMER_HOT
179  CONVERTER_12:IGBT_HOT                                         1        bo                      Injection septum - CONVERTER_12:IGBT_HOT
180  CONVERTER_12:POWER_ON                                         1        bo                      Injection septum - CONVERTER_12:POWER_ON
181  CONVERTER_12:DC_FUSE                                          1        bo                      Injection septum - CONVERTER_12:DC_FUSE 
182  CONVERTER_12:DC_LOW_VOLTS                                     1        bo                      Injection septum - CONVERTER_12:DC_LOW_VOLTS
183  CONVERTER_12:PRECHARGE                                        1        bo                      Injection septum - CONVERTER_12:PRECHARGE
184  CONVERTER_12:CONTACTOR                                        1        bo                      Injection septum - CONVERTER_12:CONTACTOR
185  CONVERTER_13:READ_CURRENT                                     2        ao                      Injection septum - CONVERTER_13:READ_CURRENT
186  CONVERTER_13:ALARM                                            0        longout                 Injection septum - CONVERTER_13:ALARM   
187  CONVERTER_13:NTC                                              3        longout                 Injection septum - CONVERTER_13:NTC     
188  CONVERTER_13:OVER_CURRENT                                     1        bo                      Injection septum - CONVERTER_13:OVER_CURRENT
189  CONVERTER_13:WATER_FLOW                                       1        bo                      Injection septum - CONVERTER_13:WATER_FLOW
190  CONVERTER_13:AC_FUSE                                          1        bo                      Injection septum - CONVERTER_13:AC_FUSE 
191  CONVERTER_13:TRANSFORMER_HOT                                  1        bo                      Injection septum - CONVERTER_13:TRANSFORMER_HOT
192  CONVERTER_13:IGBT_HOT                                         1        bo                      Injection septum - CONVERTER_13:IGBT_HOT
193  CONVERTER_13:POWER_ON                                         1        bo                      Injection septum - CONVERTER_13:POWER_ON
194  CONVERTER_13:DC_FUSE                                          1        bo                      Injection septum - CONVERTER_13:DC_FUSE 
195  CONVERTER_13:DC_LOW_VOLTS                                     1        bo                      Injection septum - CONVERTER_13:DC_LOW_VOLTS
196  CONVERTER_13:PRECHARGE                                        1        bo                      Injection septum - CONVERTER_13:PRECHARGE
197  CONVERTER_13:CONTACTOR                                        1        bo                      Injection septum - CONVERTER_13:CONTACTOR
198  CONVERTER_14:READ_CURRENT                                     2        ao                      Injection septum - CONVERTER_14:READ_CURRENT
199  CONVERTER_14:ALARM                                            0        longout                 Injection septum - CONVERTER_14:ALARM   
200  CONVERTER_14:NTC                                              3        longout                 Injection septum - CONVERTER_14:NTC     
201  CONVERTER_14:OVER_CURRENT                                     1        bo                      Injection septum - CONVERTER_14:OVER_CURRENT
202  CONVERTER_14:WATER_FLOW                                       1        bo                      Injection septum - CONVERTER_14:WATER_FLOW
203  CONVERTER_14:AC_FUSE                                          1        bo                      Injection septum - CONVERTER_14:AC_FUSE 
204  CONVERTER_14:TRANSFORMER_HOT                                  1        bo                      Injection septum - CONVERTER_14:TRANSFORMER_HOT
205  CONVERTER_14:IGBT_HOT                                         1        bo                      Injection septum - CONVERTER_14:IGBT_HOT
206  CONVERTER_14:POWER_ON                                         1        bo                      Injection septum - CONVERTER_14:POWER_ON
207  CONVERTER_14:DC_FUSE                                          1        bo                      Injection septum - CONVERTER_14:DC_FUSE 
208  CONVERTER_14:DC_LOW_VOLTS                                     1        bo                      Injection septum - CONVERTER_14:DC_LOW_VOLTS
209  CONVERTER_14:PRECHARGE                                        1        bo                      Injection septum - CONVERTER_14:PRECHARGE
210  CONVERTER_14:CONTACTOR                                        1        bo                      Injection septum - CONVERTER_14:CONTACTOR
211  CONVERTER_15:READ_CURRENT                                     2        ao                      Injection septum - CONVERTER_15:READ_CURRENT
212  CONVERTER_15:ALARM                                            0        longout                 Injection septum - CONVERTER_15:ALARM   
213  CONVERTER_15:NTC                                              3        longout                 Injection septum - CONVERTER_15:NTC     
214  CONVERTER_15:OVER_CURRENT                                     1        bo                      Injection septum - CONVERTER_15:OVER_CURRENT
215  CONVERTER_15:WATER_FLOW                                       1        bo                      Injection septum - CONVERTER_15:WATER_FLOW
216  CONVERTER_15:AC_FUSE                                          1        bo                      Injection septum - CONVERTER_15:AC_FUSE 
217  CONVERTER_15:TRANSFORMER_HOT                                  1        bo                      Injection septum - CONVERTER_15:TRANSFORMER_HOT
218  CONVERTER_15:IGBT_HOT                                         1        bo                      Injection septum - CONVERTER_15:IGBT_HOT
219  CONVERTER_15:POWER_ON                                         1        bo                      Injection septum - CONVERTER_15:POWER_ON
220  CONVERTER_15:DC_FUSE                                          1        bo                      Injection septum - CONVERTER_15:DC_FUSE 
221  CONVERTER_15:DC_LOW_VOLTS                                     1        bo                      Injection septum - CONVERTER_15:DC_LOW_VOLTS
222  CONVERTER_15:PRECHARGE                                        1        bo                      Injection septum - CONVERTER_15:PRECHARGE
223  CONVERTER_15:CONTACTOR                                        1        bo                      Injection septum - CONVERTER_15:CONTACTOR