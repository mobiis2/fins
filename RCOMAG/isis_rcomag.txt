 
  Database(P): RCOMAG
  IP Address: None
 
NUM  PV Name                                                       Address  Type        Scan        DESC                                    
  1  MIMIC:DAT                                                              waveform    1 second    MIMIC FUNCTION GENERATOR                
  2  MIMIC:DAT:HPARAM1                                                      longout                                                         
  3  RT7HC:DEMAND                                                           waveform    1 second    Normal 7Hcos Demand array               
  4  RT7HCE:DEMAND                                                          waveform    1 second    Experimental 7Hcos Demand array         
  5  RT7HS:DEMAND                                                           waveform    1 second    Normal 7Hsin Demand array               
  6  RT7HSE:DEMAND                                                          waveform    1 second    Experimental 7Hsin Demand array         
  7  RT7VC:DEMAND                                                           waveform    1 second    Normal 7Vcos Demand array               
  8  RT7VCE:DEMAND                                                          waveform    1 second    Experimental 7Vcos Demand array         
  9  RT7VS:DEMAND                                                           waveform    1 second    Normal 7Vcos Demand array               
 10  RT7VSE:DEMAND                                                          waveform    1 second    Experimental 7Vcos Demand array         
 11  RT8HC:DEMAND                                                           waveform    1 second    Normal 8Hcos Demand array               
 12  RT8HCE:DEMAND                                                          waveform    1 second    Experimental 8Hcos Demand array         
 13  RT8HS:DEMAND                                                           waveform    1 second    Normal 8Hsin Demand array               
 14  RT8HSE:DEMAND                                                          waveform    1 second    Experimental 8Hsin Demand array         
 15  RT8VC:DEMAND                                                           waveform    1 second    Normal 8VHcos Demand array              
 16  RT8VCE:DEMAND                                                          waveform    1 second    Experimental 8VHcos Demand array        
 17  RT8VS:DEMAND                                                           waveform    1 second    Normal 8VHcos Demand array              
 18  RT8VSE:DEMAND                                                          waveform    1 second    Experimental 8VHcos Demand array        
 19  NORM_HORIZ:Q_VALS                                                      waveform    1 second    Normal Horizontal Q-Value array         
 20  EXPT_HORIZ:Q_VALS                                                      waveform    1 second    Experimental Horizontal Q-Value array   
 21  NORM_VERT:Q_VALS                                                       waveform    1 second    Normal Vertical Q-Value array           
 22  EXPT_VERT:Q_VALS                                                       waveform    1 second    Experimental Vertical Q-Value array     