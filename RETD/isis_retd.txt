 
  Database(P): RETD
  IP Address: None
 
NUM  PV Name                                                       Address  Type        Scan        DESC                                    
  1  SYSTEM:INFO                                                            bo                      XML Test Database                       
  2  TYPED:INIT                                                             longout                 Init CPS Type D                         
  3  EXTRACT:DELAY                                                          longout                 Set the delay                           
  4  TIMER:CH1_DELP                                                         longout                 Timer CH1 Delta-P                       
  5  TIMER:CH1_MS_K                                                         longout                 Timer CH1 MS/K                          
  6  TIMER:CH1_MMS                                                          longout                 Timer CH1 MMS                           
  7  TIMER:CH1_ENB                                                          bo                      Timer CH1 ENABLE                        
  8  TIMER:CH1_STA                                                          longin      1 second    CH1 - Start extraction window           
  9  TIMER:CH2_DELP                                                         longout                 Timer CH2 Delta-P                       
 10  TIMER:CH2_MS_K                                                         longout                 Timer CH2 MS/K                          
 11  TIMER:CH2_MMS                                                          longout                 Timer CH2 MMS                           
 12  TIMER:CH2_ENB                                                          bo                      Timer CH2 ENABLE                        
 13  TIMER:CH2_STA                                                          longin      1 second    CH2 - Extraction back up pulse          
 14  TIMER:CH3_DELP                                                         longout                 Timer CH3 Delta-P                       
 15  TIMER:CH3_MS_K                                                         longout                 Timer CH3 MS/K                          
 16  TIMER:CH3_MMS                                                          longout                 Timer CH3 MMS                           
 17  TIMER:CH3_ENB                                                          bo                      Timer CH3 ENABLE                        
 18  TIMER:CH3_STA                                                          longin      1 second    CH3 - Stop extraction window            
 19  TIMER:CH4_DELP                                                         longout                 Timer CH4 Delta-P                       
 20  TIMER:CH4_MS_K                                                         longout                 Timer CH4 MS/K                          
 21  TIMER:CH4_MMS                                                          longout                 Timer CH4 MMS                           
 22  TIMER:CH4_ENB                                                          bo                      Timer CH4 ENABLE                        
 23  TIMER:CH4_STA                                                          longin      1 second    CH4 - BLM cRIO trigger                  
 24  TIMER_S6:CH1_DELP                                                      longout                 Timer CH1 Delta-P                       
 25  TIMER_S6:CH1_MS_K                                                      longout                 Timer CH1 MS/K                          
 26  TIMER_S6:CH1_MMS                                                       longout                 Timer CH1 MMS                           
 27  TIMER_S6:CH1_ENB                                                       bo                      Timer CH1 ENABLE                        
 28  TIMER_S6:CH1_STA                                                       longin      1 second    CH1 - inner DWS MS                      
 29  TIMER_S6:CH2_DELP                                                      longout                 Timer CH2 Delta-P                       
 30  TIMER_S6:CH2_MS_K                                                      longout                 Timer CH2 MS/K                          
 31  TIMER_S6:CH2_MMS                                                       longout                 Timer CH2 MMS                           
 32  TIMER_S6:CH2_ENB                                                       bo                      Timer CH2 ENABLE                        
 33  TIMER_S6:CH2_STA                                                       longin      1 second    CH2 - inner DWS GMS                     
 34  TIMER_S6:CH3_DELP                                                      longout                 Timer CH3 Delta-P                       
 35  TIMER_S6:CH3_MS_K                                                      longout                 Timer CH3 MS/K                          
 36  TIMER_S6:CH3_MMS                                                       longout                 Timer CH3 MMS                           
 37  TIMER_S6:CH3_ENB                                                       bo                      Timer CH3 ENABLE                        
 38  TIMER_S6:CH3_STA                                                       longin      1 second    CH3 - spare MS                          
 39  TIMER_S6:CH4_DELP                                                      longout                 Timer CH4 Delta-P                       
 40  TIMER_S6:CH4_MS_K                                                      longout                 Timer CH4 MS/K                          
 41  TIMER_S6:CH4_MMS                                                       longout                 Timer CH4 MMS                           
 42  TIMER_S6:CH4_ENB                                                       bo                      Timer CH4 ENABLE                        
 43  TIMER_S6:CH4_STA                                                       longin      1 second    CH4 - spare GMS                         
 44  TYPEK:INIT_CH1                                                         longout                 Initialise Ch.1                         
 45  TYPEK:INIT_CH1:HPARAM1                                                 longout                                                         
 46  TYPEK:INIT_CH2                                                         longout                 Initialise Ch.2                         
 47  TYPEK:INIT_CH2:HPARAM1                                                 longout                                                         
 48  DPSU_POS15:RESET                                                       longout                 Reset PSU                               
 49  DPSU_POS15:RESET:HPARAM1                                               longout                                                         
 50  DPSU_POS15:ON_OFF                                                      bo                      PSU On/Off                              
 51  DPSU_POS15:ON_OFF:HPARAM1                                              longout                                                         
 52  DPSU_POS15:SETI                                                        ao                      Set Output Current                      
 53  DPSU_POS15:SETI:HPARAM1                                                longout                                                         
 54  DPSU_POS15:SETV                                                        ao                      Set Output Voltage                      
 55  DPSU_POS15:SETV:HPARAM1                                                longout                                                         
 56  DPSU_POS15:READI                                                       ai          1 second    Read Output Current                     
 57  DPSU_POS15:READI:HPARAM1                                               longout                                                         
 58  DPSU_POS15:READSETI                                                    ai          1 second    Read Set Current                        
 59  DPSU_POS15:READSETI:HPARAM1                                            longout                                                         
 60  DPSU_POS15:READV                                                       ai          1 second    Read Output Voltage                     
 61  DPSU_POS15:READV:HPARAM1                                               longout                                                         
 62  DPSU_POS15:READSETV                                                    ai          1 second                                            
 63  DPSU_POS15:READSETV:HPARAM1                                            longout                                                         
 64  DPSU_POS15:READSTAT                                                    longin      1 second    Read Status                             
 65  DPSU_POS15:READSTAT:HPARAM1                                            longout                                                         
 66  DPSU_NEG15:RESET                                                       longout                 Reset PSU                               
 67  DPSU_NEG15:RESET:HPARAM1                                               longout                                                         
 68  DPSU_NEG15:ON_OFF                                                      bo                      PSU On/Off                              
 69  DPSU_NEG15:ON_OFF:HPARAM1                                              longout                                                         
 70  DPSU_NEG15:SETI                                                        ao                      Set Output Current                      
 71  DPSU_NEG15:SETI:HPARAM1                                                longout                                                         
 72  DPSU_NEG15:SETV                                                        ao                      Set Output Voltage                      
 73  DPSU_NEG15:SETV:HPARAM1                                                longout                                                         
 74  DPSU_NEG15:READI                                                       ai          1 second    Read Output Current                     
 75  DPSU_NEG15:READI:HPARAM1                                               longout                                                         
 76  DPSU_NEG15:READSETI                                                    ai          1 second    Read Set Current                        
 77  DPSU_NEG15:READSETI:HPARAM1                                            longout                                                         
 78  DPSU_NEG15:READV                                                       ai          1 second    Read Output Voltage                     
 79  DPSU_NEG15:READV:HPARAM1                                               longout                                                         
 80  DPSU_NEG15:READSETV                                                    ai          1 second                                            
 81  DPSU_NEG15:READSETV:HPARAM1                                            longout                                                         
 82  DPSU_NEG15:READSTAT                                                    longin      1 second    Read Status                             
 83  DPSU_NEG15:READSTAT:HPARAM1                                            longout                                                         
 84  DPSU_POS5:RESET                                                        longout                 Reset PSU                               
 85  DPSU_POS5:RESET:HPARAM1                                                longout                                                         
 86  DPSU_POS5:ON_OFF                                                       bo                      PSU On/Off                              
 87  DPSU_POS5:ON_OFF:HPARAM1                                               longout                                                         
 88  DPSU_POS5:SETI                                                         ao                      Set Output Current                      
 89  DPSU_POS5:SETI:HPARAM1                                                 longout                                                         
 90  DPSU_POS5:SETV                                                         ao                      Set Output Voltage                      
 91  DPSU_POS5:SETV:HPARAM1                                                 longout                                                         
 92  DPSU_POS5:READI                                                        ai          1 second    Read Output Current                     
 93  DPSU_POS5:READI:HPARAM1                                                longout                                                         
 94  DPSU_POS5:READSETI                                                     ai          1 second    Read Set Current                        
 95  DPSU_POS5:READSETI:HPARAM1                                             longout                                                         
 96  DPSU_POS5:READV                                                        ai          1 second    Read Output Voltage                     
 97  DPSU_POS5:READV:HPARAM1                                                longout                                                         
 98  DPSU_POS5:READSETV                                                     ai          1 second                                            
 99  DPSU_POS5:READSETV:HPARAM1                                             longout                                                         
100  DPSU_POS5:READSTAT                                                     longin      1 second    Read Status                             
101  DPSU_POS5:READSTAT:HPARAM1                                             longout                                                         
102  DPSU_POS24:RESET                                                       longout                 Reset PSU                               
103  DPSU_POS24:RESET:HPARAM1                                               longout                                                         
104  DPSU_POS24:ON_OFF                                                      bo                      PSU On/Off                              
105  DPSU_POS24:ON_OFF:HPARAM1                                              longout                                                         
106  DPSU_POS24:SETI                                                        ao                      Set Output Current                      
107  DPSU_POS24:SETI:HPARAM1                                                longout                                                         
108  DPSU_POS24:SETV                                                        ao                      Set Output Voltage                      
109  DPSU_POS24:SETV:HPARAM1                                                longout                                                         
110  DPSU_POS24:READI                                                       ai          1 second    Read Output Current                     
111  DPSU_POS24:READI:HPARAM1                                               longout                                                         
112  DPSU_POS24:READSETI                                                    ai          1 second    Read Set Current                        
113  DPSU_POS24:READSETI:HPARAM1                                            longout                                                         
114  DPSU_POS24:READV                                                       ai          1 second    Read Output Voltage                     
115  DPSU_POS24:READV:HPARAM1                                               longout                                                         
116  DPSU_POS24:READSETV                                                    ai          1 second                                            
117  DPSU_POS24:READSETV:HPARAM1                                            longout                                                         
118  DPSU_POS24:READSTAT                                                    longin      1 second    Read Status                             
119  DPSU_POS24:READSTAT:HPARAM1                                            longout                                                         
120  DPSU_NEG24:RESET                                                       longout                 Reset PSU                               
121  DPSU_NEG24:RESET:HPARAM1                                               longout                                                         
122  DPSU_NEG24:ON_OFF                                                      bo                      PSU On/Off                              
123  DPSU_NEG24:ON_OFF:HPARAM1                                              longout                                                         
124  DPSU_NEG24:SETI                                                        ao                      Set Output Current                      
125  DPSU_NEG24:SETI:HPARAM1                                                longout                                                         
126  DPSU_NEG24:SETV                                                        ao                      Set Output Voltage                      
127  DPSU_NEG24:SETV:HPARAM1                                                longout                                                         
128  DPSU_NEG24:READI                                                       ai          1 second    Read Output Current                     
129  DPSU_NEG24:READI:HPARAM1                                               longout                                                         
130  DPSU_NEG24:READSETI                                                    ai          1 second    Read Set Current                        
131  DPSU_NEG24:READSETI:HPARAM1                                            longout                                                         
132  DPSU_NEG24:READV                                                       ai          1 second    Read Output Voltage                     
133  DPSU_NEG24:READV:HPARAM1                                               longout                                                         
134  DPSU_NEG24:READSETV                                                    ai          1 second                                            
135  DPSU_NEG24:READSETV:HPARAM1                                            longout                                                         
136  DPSU_NEG24:READSTAT                                                    longin      1 second    Read Status                             
137  DPSU_NEG24:READSTAT:HPARAM1                                            longout                                                         