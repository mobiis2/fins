 
  Database(P): RMMMON
  IP Address: None
 
NUM  PV Name                                                       Address  Type        Scan        DESC                                    
  1  SYSTEM:INFO_1                                                          bo                      RMMMON                                  
  2  SERIAL:INIT_AC                                                         longout                 Serial card initialise AC               
  3  SERIAL:INIT_AC:HPARAM1                                                 longout                                                         
  4  SERIAL:INIT_DC                                                         longout                 Serial card initialise DC               
  5  SERIAL:INIT_DC:HPARAM1                                                 longout                                                         
  6  AC:READ_CURRENT                                                        ai          1 second    Read MMPS AC read current (DCCT)        
  7  DC:READ_CURRENT                                                        ai          1 second    Read MMPS DC read current (DCCT)        
  8  ACDC:READ_INJ_CURRENT                                                  ao                      Read MMPS Inj. current (DCCT)           
  9  ACDC:READ_EXT_CURRENT                                                  ao                      Read MMPS Ext. current (DCCT)           
 10  SYSTEM:INFO_2                                                          bo                      UPS Monitor R6A                         
 11  TYPEG_1:INIT                                                           longout                 Type G init channel                     
 12  TYPEG_2:INIT                                                           longout                 Type G init channel                     
 13  UPS:MON_OUT1                                                           ai          1 second    Output 1 monitor                        
 14  UPS:MON_OUT2                                                           ai          1 second    Output 2 monitor                        
 15  UPS:MON_OUT3                                                           ai          1 second    Output 3 monitor                        
 16  UPS:MON_OUT4                                                           ai          1 second    Output 4 monitor                        