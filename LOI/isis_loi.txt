 
  Database(P): LOI
  IP Address: None
 
NUM  PV Name                                                       Address  Type        Scan        DESC                                    
  1  SYSTEM:LOAD:HPARAM1                                                    longout                                                         
  2  SYSTEM:STATUS                                                          longout                 Remote database status                  
  3  SYSTEM:STATUS:HPARAM1                                                  longout                                                         
  4  SYSTEM:ERROR                                                           longout                 System error channel                    
  5  SYSTEM:PCC                                                             longout                 PCC channel                             
  6  C48_GP_STEF:DAT                                                        waveform    1 second    General Purpose F-module                
  7  C48_MSF_STEF:DAT                                                       waveform    1 second    Master System function F-module         
  8  C48_GAPV_STEF:DAT                                                      waveform    1 second    R2RF Gap Volts Normal STE F-module      
  9  C48_GP_STET:CH1_DELP                                                   longout                 General Purpose timer CH1 Delta-P       
 10  C48_GP_STET:CH2_DELP                                                   longout                 General Purpose timer CH2 Delta-P       
 11  C48_GP_STET:CH1_MS_K                                                   longout                 General Purpose timer CH1 k             
 12  C48_GP_STET:CH2_MS_K                                                   longout                 General Purpose timer CH2 k             
 13  C48_GP_STET:CH1_ENB                                                    bo                      General Purpose timer CH1 Enable        
 14  C48_GP_STET:CH2_ENB                                                    bo                      General Purpose timer CH2 Enable        
 15  C48_GP_STET:CH1_SST                                                    bo                      General Purpose timer CH1 Single-Shot   
 16  C48_GP_STET:CH2_SST                                                    bo                      General Purpose timer CH2 Single-Shot   
 17  C48_GP_STET:CH1_MMS                                                    longout                 General Purpose timer CH1 m*MS          
 18  C48_GP_STET:CH2_MMS                                                    longout                 General Purpose timer CH2 m*MS          
 19  C48_GP_STET:CH1_STA                                                    longout                 General Purpose timer CH1 Status        
 20  C48_GP_STET:CH2_STA                                                    longout                 General Purpose timer CH2 Status        
 21  C48_MSF_STET:CH1_DELP                                                  longout                 Master System func timer CH1 Delta-P    
 22  C48_MSF_STET:CH2_DELP                                                  longout                 Master System func timer CH2 Delta-P    
 23  C48_MSF_STET:CH1_MS_K                                                  longout                 Master System func timer CH1 k          
 24  C48_MSF_STET:CH2_MS_K                                                  longout                 Master System func timer CH2 k          
 25  C48_MSF_STET:CH1_ENB                                                   bo                      Master System func timer CH1 Enable     
 26  C48_MSF_STET:CH2_ENB                                                   bo                      Master System func timer CH2 Enable     
 27  C48_MSF_STET:CH1_SST                                                   bo                      Master System func timer CH1 Single-Shot
 28  C48_MSF_STET:CH2_SST                                                   bo                      Master System func timer CH2 Single-Shot
 29  C48_MSF_STET:CH1_MMS                                                   longout                 Master System func timer CH1 m*MS       
 30  C48_MSF_STET:CH2_MMS                                                   longout                 Master System func timer CH2 m*MS       
 31  C48_MSF_STET:CH1_STA                                                   longout                 Master System func timer CH1 Status     
 32  C48_MSF_STET:CH2_STA                                                   longout                 Master System func timer CH2 Status     
 33  SUPER_PERIOD:NUM                                                       longout                 RF Super period number.                 
 34  LIQRES:PIP                                                             stringout               Liquid Resistor PIP channel             
 35  LIQRES:MSG                                                             stringout               Liquid Resistor message channel         
 36  LIQRES:MUX                                                             stringout               Liquid Resistor CuSO4 multiplexor item  
 37  AGS_4:READ_VOLTS                                                       ao                      APS 4 VOLTS read via relay.             
 38  AGS_5:READ_VOLTS                                                       ao                      APS 5 VOLTS read via relay.             
 39  AGS_6:READ_VOLTS                                                       ao                      APS 6 VOLTS read via relay.             
 40  AGS_8:READ_VOLTS                                                       ao                      APS 8 VOLTS read via relay.             
 41  DYN_GRAPH:X_AXIS                                                       waveform    1 second                                            
 42  DYN_GRAPH:Y_AXIS                                                       waveform    1 second                                            
 43  DYN_DISPLAY_GRAPH:PIP                                                  stringout               Dynamic graph PIP control channel.      
 44  MANLOAD:TXT                                                            stringout               Name of channels to be loaded.          
 45  FUNC_MSG:TXT                                                           stringout               Display general message.                
 46  FUNC_MSG:SET_TXT                                                       stringout               Display function (FID) currently set.   
 47  FUNC_VALS:CAP_TXT                                                      stringout               Caption for function values screen.     
 48  SAVEFILE:NAME                                                          stringout               User i/p: name for function data file.  
 49  ARRAY_ELEMENT:X1                                                       ao                      1st x coordinate                        
 50  ARRAY_ELEMENT:X2                                                       ao                      2nd x coordinate                        
 51  ARRAY_ELEMENT:X3                                                       ao                      3rd x coordinate                        
 52  ARRAY_ELEMENT:X4                                                       ao                      4th x coordinate                        
 53  ARRAY_ELEMENT:X5                                                       ao                      5th x coordinate                        
 54  ARRAY_ELEMENT:X6                                                       ao                      6th x coordinate                        
 55  ARRAY_ELEMENT:X7                                                       ao                      7th x coordinate                        
 56  ARRAY_ELEMENT:X8                                                       ao                      8th x coordinate                        
 57  ARRAY_ELEMENT:X9                                                       ao                      9th x coordinate                        
 58  ARRAY_ELEMENT:X10                                                      ao                      10th x coordinate                       
 59  ARRAY_ELEMENT:X11                                                      ao                      11th x coordinate                       
 60  ARRAY_ELEMENT:X12                                                      ao                      12th x coordinate                       
 61  ARRAY_ELEMENT:X13                                                      ao                      13th x coordinate                       
 62  ARRAY_ELEMENT:X14                                                      ao                      14th x coordinate                       
 63  ARRAY_ELEMENT:X15                                                      ao                      15th x coordinate                       
 64  ARRAY_ELEMENT:X16                                                      ao                      16th x coordinate                       
 65  ARRAY_ELEMENT:X17                                                      ao                      17th x coordinate                       
 66  ARRAY_ELEMENT:X18                                                      ao                      18th x coordinate                       
 67  ARRAY_ELEMENT:X19                                                      ao                      19th x coordinate                       
 68  ARRAY_ELEMENT:X20                                                      ao                      20th x coordinate                       
 69  ARRAY_ELEMENT:Y1                                                       ao                      1st y coordinate                        
 70  ARRAY_ELEMENT:Y2                                                       ao                      2nd y coordinate                        
 71  ARRAY_ELEMENT:Y3                                                       ao                      3rd y coordinate                        
 72  ARRAY_ELEMENT:Y4                                                       ao                      4th y coordinate                        
 73  ARRAY_ELEMENT:Y5                                                       ao                      5th y coordinate                        
 74  ARRAY_ELEMENT:Y6                                                       ao                      6th y coordinate                        
 75  ARRAY_ELEMENT:Y7                                                       ao                      7th y coordinate                        
 76  ARRAY_ELEMENT:Y8                                                       ao                      8th y coordinate                        
 77  ARRAY_ELEMENT:Y9                                                       ao                      9th y coordinate                        
 78  ARRAY_ELEMENT:Y10                                                      ao                      10th y coordinate                       
 79  ARRAY_ELEMENT:Y11                                                      ao                      11th y coordinate                       
 80  ARRAY_ELEMENT:Y12                                                      ao                      12th y coordinate                       
 81  ARRAY_ELEMENT:Y13                                                      ao                      13th y coordinate                       
 82  ARRAY_ELEMENT:Y14                                                      ao                      14th y coordinate                       
 83  ARRAY_ELEMENT:Y15                                                      ao                      15th y coordinate                       
 84  ARRAY_ELEMENT:Y16                                                      ao                      16th y coordinate                       
 85  ARRAY_ELEMENT:Y17                                                      ao                      17th y coordinate                       
 86  ARRAY_ELEMENT:Y18                                                      ao                      18th y coordinate                       
 87  ARRAY_ELEMENT:Y19                                                      ao                      19th y coordinate                       
 88  ARRAY_ELEMENT:Y20                                                      ao                      20th y coordinate                       
 89  FUNC_ID:NUM                                                            longout                 Function identification number.         
 90  FUNC:RANGE                                                             longout                 Function range eg +-10 V.               
 91  DISPLAY_SMALL:PIP                                                      stringout               Small PIP control channel.              
 92  DISPLAY_LARGE:PIP                                                      stringout               Large PIP control channel.              
 93  LOAD:FLAG                                                              longout                 Confirm LOAD FGEN function.             
 94  VT_MOD:VARG                                                            longout                 Modification case.                      
 95  VT_MOD:ELEMENTS                                                        longout                 Number of elements in function.         
 96  VT_MOD:INPUT_TIME                                                      ao                      Time input by user.                     
 97  GENVT_MOD:INPUT_VOLTS                                                  ao                      User input: General V/T Volts (V).      
 98  GENVT_TIME:DAT                                                         waveform    1 second    General V/T array: times (ms).          
 99  GENVT_VOLTS:DAT                                                        waveform    1 second    General V/T array: voltages (V).        
100  GENVT:ZERO_FLAG                                                        longout                 Zero function flag.                     