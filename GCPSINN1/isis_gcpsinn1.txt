 
  Database(P): GCPSINN1
  IP Address: None
 
NUM  PV Name                                                       Address  Type        Scan        DESC                                    
  1  SYSTEM:INFO                                                            bo                      XML test database - crate 1             
  2  TIMER_1:CH1_DELP                                                       longout                 Timer CH1 Delta-P                       
  3  TIMER_1:CH1_MS_K                                                       longout                 Timer CH1 MS/K                          
  4  TIMER_1:CH1_MMS                                                        longout                 Timer CH1 MMS                           
  5  TIMER_1:CH1_ENB                                                        bo                      Timer CH1 ENABLE                        
  6  TIMER_1:CH1_STA                                                        longin      1 second    Timer CH1 Status - EPB2 Profile Monitors
  7  TIMER_1:CH2_DELP                                                       longout                 Timer CH2 Delta-P                       
  8  TIMER_1:CH2_MS_K                                                       longout                 Timer CH2 MS/K                          
  9  TIMER_1:CH2_MMS                                                        longout                 Timer CH2 MMS                           
 10  TIMER_1:CH2_ENB                                                        bo                      Timer CH2 ENABLE                        
 11  TIMER_1:CH2_STA                                                        longin      1 second    Timer CH2 Status - EPB2 Profile Monitors
 12  TIMER_1:CH3_DELP                                                       longout                 Timer CH3 Delta-P                       
 13  TIMER_1:CH3_MS_K                                                       longout                 Timer CH3 MS/K                          
 14  TIMER_1:CH3_MMS                                                        longout                 Timer CH3 MMS                           
 15  TIMER_1:CH3_ENB                                                        bo                      Timer CH3 ENABLE                        
 16  TIMER_1:CH3_STA                                                        longin      1 second    Timer CH3 Status - EPB2 Profile Monitors
 17  TIMER_1:CH4_DELP                                                       longout                 Timer CH4 Delta-P                       
 18  TIMER_1:CH4_MS_K                                                       longout                 Timer CH4 MS/K                          
 19  TIMER_1:CH4_MMS                                                        longout                 Timer CH4 MMS                           
 20  TIMER_1:CH4_ENB                                                        bo                      Timer CH4 ENABLE                        
 21  TIMER_1:CH4_STA                                                        longin      1 second    Timer CH4 Status - EPB2 Profile Monitors
 22  TIMER_2:CH1_DELP                                                       longout                 Timer CH1 Delta-P                       
 23  TIMER_2:CH1_MS_K                                                       longout                 Timer CH1 MS/K                          
 24  TIMER_2:CH1_MMS                                                        longout                 Timer CH1 MMS                           
 25  TIMER_2:CH1_ENB                                                        bo                      Timer CH1 ENABLE                        
 26  TIMER_2:CH1_STA                                                        longin      1 second    Timer CH1 Status - Sample EPB2 Profile Monitors
 27  TIMER_2:CH2_DELP                                                       longout                 Timer CH2 Delta-P                       
 28  TIMER_2:CH2_MS_K                                                       longout                 Timer CH2 MS/K                          
 29  TIMER_2:CH2_MMS                                                        longout                 Timer CH2 MMS                           
 30  TIMER_2:CH2_ENB                                                        bo                      Timer CH2 ENABLE                        
 31  TIMER_2:CH2_STA                                                        longin      1 second    Timer CH2 Status                        
 32  TIMER_2:CH3_DELP                                                       longout                 Timer CH3 Delta-P                       
 33  TIMER_2:CH3_MS_K                                                       longout                 Timer CH3 MS/K                          
 34  TIMER_2:CH3_MMS                                                        longout                 Timer CH3 MMS                           
 35  TIMER_2:CH3_ENB                                                        bo                      Timer CH3 ENABLE                        
 36  TIMER_2:CH3_STA                                                        longin      1 second    Timer 2 CH3 Status - Multi-Channel profile monitors
 37  TIMER_2:CH4_DELP                                                       longout                 Timer CH4 Delta-P                       
 38  TIMER_2:CH4_MS_K                                                       longout                 Timer CH4 MS/K                          
 39  TIMER_2:CH4_MMS                                                        longout                 Timer CH4 MMS                           
 40  TIMER_2:CH4_ENB                                                        bo                      Timer CH4 ENABLE                        
 41  TIMER_2:CH4_STA                                                        longin      1 second    Timer 2 CH4 Status - Pos. Mon. High gain
 42  TYPEF:STATUS                                                           longin      1 second    Type-F Status                           
 43  TYPEF:NORMAL_DAT                                                       waveform    1 second    Data download to real component of function generator
 44  TYPEF:EXPT_DAT                                                         waveform    1 second    Data download to test component of function generator
 45  TYPEF:DC                                                               longout                 Write a fixed DC value to DAC           
 46  TYPEF:REGISTER_WRITE                                                   longout                 One write channel to set-up RAM operating channel
 47  REGISTER:READ_DAC_MODE                                                 longin      1 second    Register status read back, DAC operation mode
 48  TYPEF:REGISTER_READ                                                    longin      1 second    Register status read back, RAM swapping register
 49  TYPEF:REGISTER_MULTIPLY                                                dfanout                 Duplicate register for interpretation in other channels
 50  TYPEF:PULSING_SET                                                      bo                                                              
 51  TYPEF:PULSING_READ_C                                                   sub                      sub                                    
 52  TYPEF:PULSING_READ                                                     bi                                                              
 53  TYPEF:MAIN_FUNCTION_SET                                                bo                      Switching direction. 0 is Real to Test. 1 is Test to Real
 54  TYPEF:MAIN_FUNCTION_READ_C                                             sub                      sub                                    
 55  TYPEF:MAIN_FUNCTION_READ                                               bi                                                              
 56  TYPEF:PULSING:FREQUENCY_SET                                            longout                 Switching frequency MS/2k, range 0-7    
 57  TYPEF:PULSING:FREQUENCY_READ_C                                         sub                      sub                                    
 58  TYPEF:PULSING:FREQUENCY_READ                                           bi                                                              
 59  TYPEF:START_SET                                                        longout                 DP position of switching point reference to prior MS
 60  TYPEF:START_READ_C                                                     sub                      sub                                    
 61  TYPEF:START_READ                                                       bi                                                              
 62  TYPEF:DEPTH_SET                                                        longout                 Depth of ram switching, default is 3999 (4000 DPs), 0 means no switching output from one of the memory blocks, in that case switching direction decided which block
 63  TYPEF:DEPTH_READ_C                                                     sub                      sub                                    
 64  TYPEF:DEPTH_READ                                                       bi                                                              
 65  TYPEF:DC_ON_READ_C                                                     sub                      sub                                    
 66  TYPEF:DC_ON_READ                                                       bi                                                              
 67  GRAPH:DC_LEVEL                                                         ao                                                              
 68  GRAPH:NORMAL_X_AXIS                                                    waveform    1 second                                            
 69  GRAPH:NORMAL_Y_AXIS                                                    waveform    1 second                                            
 70  GRAPH:EXPT_X_AXIS                                                      waveform    1 second                                            
 71  GRAPH:EXPT_Y_AXIS                                                      waveform    1 second                                            
 72  INJSEP:STATUS                                                          longin      1 second    Type-F Status                           
 73  INJSEP:NORMAL_DAT                                                      waveform    1 second    Data download to real component of function generator
 74  INJSEP:EXPT_DAT                                                        waveform    1 second    Data download to test component of function generator
 75  INJSEP:DC                                                              longout                 Injection septum DC level set           
 76  INJSEP:REGISTER_WRITE                                                  longout                 One write channel to set-up RAM operating channel
 77  INJSEP:READ_DAC_MODE                                                   longin      1 second    Register status read back, DAC operation mode
 78  INJSEP:REGISTER_READ                                                   longin      1 second    Register status read back, RAM swapping register
 79  INJSEP:REGISTER_MULTIPLY                                               dfanout                 Duplicate register for interpretation in other channels
 80  INJSEP:PULSING_SET                                                     bo                                                              
 81  INJSEP:PULSING_READ_C                                                  sub                      sub                                    
 82  INJSEP:PULSING_READ                                                    bi                                                              
 83  INJSEP:MAIN_FUNCTION_SET                                               bo                      Switching direction. 0 is Real to Test. 1 is Test to Real
 84  INJSEP:MAIN_FUNCTION_READ_C                                            sub                      sub                                    
 85  INJSEP:MAIN_FUNCTION_READ                                              bi                                                              
 86  INJSEP:PULSING:FREQUENCY_SET                                           longout                 Switching frequency MS/2k, range 0-7    
 87  INJSEP:PULSING:FREQUENCY_READ_C                                        sub                      sub                                    
 88  INJSEP:PULSING:FREQUENCY_READ                                          bi                                                              
 89  INJSEP:START_SET                                                       longout                 DP position of switching point reference to prior MS
 90  INJSEP:START_READ_C                                                    sub                      sub                                    
 91  INJSEP:START_READ                                                      bi                                                              
 92  INJSEP:DEPTH_SET                                                       longout                 Depth of ram switching, default is 3999 (4000 DPs), 0 means no switching output from one of the memory blocks, in that case switching direction decided which block
 93  INJSEP:DEPTH_READ_C                                                    sub                      sub                                    
 94  INJSEP:DEPTH_READ                                                      bi                                                              
 95  INJSEP:DC_ON_READ_C                                                    sub                      sub                                    
 96  INJSEP:DC_ON_READ                                                      bi                                                              