 
  Database(P): CPSD
  IP Address: None
 
NUM  PV Name                                                       Address  Type        Scan        DESC                                    
  1  SYSTEM:INFO_1                                                          bo                      CPS Type-D test                         
  2  TYPED:READ_WORD                                                        longin      1 second                                            
  3  TYPED:READ_IST                                                         longin      1 second                                            
  4  TYPED:READ_OST                                                         longin      1 second                                            
  5  TYPED:ISPC1:WRITE                                                      longout                                                         
  6  TYPED:ISPC2:WRITE                                                      longout                                                         
  7  TYPED:ISPC3:WRITE                                                      longout                                                         
  8  TYPED:ISPC4:WRITE                                                      longout                                                         
  9  TYPED:COMMAND_WRITE                                                    longout                                                         
 10  TYPED:OSPC1:WRITE                                                      longout                                                         
 11  TYPED:OSPC2:WRITE                                                      longout                                                         
 12  TYPED:OSPC3:WRITE                                                      longout                                                         
 13  TYPED:OSPC4:WRITE                                                      longout                                                         
 14  TYPEDS:READ_REAR:MULTIPLY                                              dfanout                                                         
 15  TYPEDS:READ_REAR:MULTIPLY_1                                            dfanout                                                         
 16  TYPEDS:READ_REAR:MULTIPLY_2                                            dfanout                                                         
 17  TYPEDS:READ_REAR:BIT0_C                                                sub                                                             
 18  TYPEDS:READ_REAR:BIT0                                                  bi                                                              
 19  TYPEDS:READ_REAR:BIT1_C                                                sub                                                             
 20  TYPEDS:READ_REAR:BIT1                                                  bi                                                              
 21  TYPEDS:READ_REAR:BIT2_C                                                sub                                                             
 22  TYPEDS:READ_REAR:BIT2                                                  bi                                                              
 23  TYPEDS:READ_REAR:BIT3_C                                                sub                                                             
 24  TYPEDS:READ_REAR:BIT3                                                  bi                                                              
 25  TYPEDS:READ_REAR:BIT4_C                                                sub                                                             
 26  TYPEDS:READ_REAR:BIT4                                                  bi                                                              
 27  TYPEDS:READ_REAR:BIT5_C                                                sub                                                             
 28  TYPEDS:READ_REAR:BIT5                                                  bi                                                              
 29  TYPEDS:READ_REAR:BIT6_C                                                sub                                                             
 30  TYPEDS:READ_REAR:BIT6                                                  bi                                                              
 31  TYPEDS:READ_REAR:BIT7_C                                                sub                                                             
 32  TYPEDS:READ_REAR:BIT7                                                  bi                                                              
 33  TYPEDS:READ_REAR:BIT8_C                                                sub                                                             
 34  TYPEDS:READ_REAR:BIT8                                                  bi                                                              
 35  TYPEDS:READ_REAR:BIT9_C                                                sub                                                             
 36  TYPEDS:READ_REAR:BIT9                                                  bi                                                              
 37  TYPEDS:READ_REAR:BIT10_C                                               sub                                                             
 38  TYPEDS:READ_REAR:BIT10                                                 bi                                                              
 39  TYPEDS:READ_REAR:BIT11_C                                               sub                                                             
 40  TYPEDS:READ_REAR:BIT11                                                 bi                                                              
 41  TYPEDS:READ_REAR:BIT12_C                                               sub                                                             
 42  TYPEDS:READ_REAR:BIT12                                                 bi                                                              
 43  TYPEDS:READ_REAR:BIT13_C                                               sub                                                             
 44  TYPEDS:READ_REAR:BIT13                                                 bi                                                              
 45  TYPEDS:READ_REAR:BIT14_C                                               sub                                                             
 46  TYPEDS:READ_REAR:BIT14                                                 bi                                                              
 47  TYPEDS:READ_REAR:BIT15_C                                               sub                                                             
 48  TYPEDS:READ_REAR:BIT15                                                 bi                                                              
 49  TYPEDS:READ_FRONT:MULTIPLY                                             dfanout                                                         
 50  TYPEDS:READ_FRONT:MULTIPLY_1                                           dfanout                                                         
 51  TYPEDS:READ_FRONT:MULTIPLY_2                                           dfanout                                                         
 52  TYPEDS:READ_FRONT:BIT0_C                                               sub                                                             
 53  TYPEDS:READ_FRONT:BIT0                                                 bi                                                              
 54  TYPEDS:READ_FRONT:BIT1_C                                               sub                                                             
 55  TYPEDS:READ_FRONT:BIT1                                                 bi                                                              
 56  TYPEDS:READ_FRONT:BIT2_C                                               sub                                                             
 57  TYPEDS:READ_FRONT:BIT2                                                 bi                                                              
 58  TYPEDS:READ_FRONT:BIT3_C                                               sub                                                             
 59  TYPEDS:READ_FRONT:BIT3                                                 bi                                                              
 60  TYPEDS:READ_FRONT:BIT4_C                                               sub                                                             
 61  TYPEDS:READ_FRONT:BIT4                                                 bi                                                              
 62  TYPEDS:READ_FRONT:BIT5_C                                               sub                                                             
 63  TYPEDS:READ_FRONT:BIT5                                                 bi                                                              
 64  TYPEDS:READ_FRONT:BIT6_C                                               sub                                                             
 65  TYPEDS:READ_FRONT:BIT6                                                 bi                                                              
 66  TYPEDS:READ_FRONT:BIT7_C                                               sub                                                             
 67  TYPEDS:READ_FRONT:BIT7                                                 bi                                                              
 68  TYPEDS:READ_FRONT:BIT8_C                                               sub                                                             
 69  TYPEDS:READ_FRONT:BIT8                                                 bi                                                              
 70  TYPEDS:READ_FRONT:BIT9_C                                               sub                                                             
 71  TYPEDS:READ_FRONT:BIT9                                                 bi                                                              
 72  TYPEDS:READ_FRONT:BIT10_C                                              sub                                                             
 73  TYPEDS:READ_FRONT:BIT10                                                bi                                                              
 74  TYPEDS:READ_FRONT:BIT11_C                                              sub                                                             
 75  TYPEDS:READ_FRONT:BIT11                                                bi                                                              
 76  TYPEDS:READ_FRONT:BIT12_C                                              sub                                                             
 77  TYPEDS:READ_FRONT:BIT12                                                bi                                                              
 78  TYPEDS:READ_FRONT:BIT13_C                                              sub                                                             
 79  TYPEDS:READ_FRONT:BIT13                                                bi                                                              
 80  TYPEDS:READ_FRONT:BIT14_C                                              sub                                                             
 81  TYPEDS:READ_FRONT:BIT14                                                bi                                                              
 82  TYPEDS:READ_FRONT:BIT15_C                                              sub                                                             
 83  TYPEDS:READ_FRONT:BIT15                                                bi                                                              