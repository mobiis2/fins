 
  Database(P): IHEDS
  IP Address: None
 
NUM  PV Name                                                       Address  Type        Scan        DESC                                    
  1  BUTTON:ID                                                              longout                 Button number.                          
  2  DISPLAY:PIP                                                            stringout               General purpose PIP channel.            
  3  MSG_CHAN:TXT                                                           stringout               General purpose text channel.           
  4  START_PULSE_TIMER:STA                                                  longout                 Chopped Beam Start-Pulse timer ON/OFF status.
  5  PULSE_LEN_TIMER:STA                                                    stringout               Chopped Beam Pulse-Length timer derived status.
  6  POSMONS_GAIN_TIMER:STA                                                 longout                 Position Monitors Pulsed Gain timer derived status
  7  PHASE_TIMER:STA                                                        longout                 R1RF Phase Function timer derived status.
  8  PULSE_LENGTH:TIME                                                      longout                 Beam Pulse Length (micro or nanosecs).  
  9  PULSE_LENGTH_IP:TIME                                                   longout                 Beam Pulse Length input by user.        
 10  PULSE_LENGTH_IP:UNITS                                                  stringout               Beam Pulse Length Units input by user.  
 11  PULSE_START:TIME                                                       longout                 Beam Pulse Start-Time (microsecs).      
 12  PULSE_START:TIME_DP                                                    longout                 Beam Pulse Start-Time (delta-p).        
 13  PULSE_START_IP:TIME                                                    longout                 Beam Pulse Start-Time input by user.    
 14  RF_TIMING:DELAY                                                        longout                 GUI channel                             