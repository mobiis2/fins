 
  Database(P): R80COOLT1
  IP Address: 172.16.88.50
 
NUM  PV Name                                                       Address  Type        Scan        DESC                                    
  1  REGISTER:00000                                                0        longin      1 second    Status register D00000                  
  2  REGISTER:00001                                                1        longin      1 second    Status register D00001                  
  3  00000:MULTIPLE                                                         dfanout                 redirection channel reg 00002           
  4  00000_A:MULTIPLE                                                       dfanout                 Redirection channel reg 00000 LS BYTE   
  5  00000_B:MULTIPLE                                                       dfanout                 Redirection channel reg 00000 MS BYTE   
  6  00001:MULTIPLE                                                         dfanout                 redirection channel reg 00001           
  7  00001_A:MULTIPLE                                                       dfanout                 Redirection channel reg 00001 LS BYTE   
  8  00001_B:MULTIPLE                                                       dfanout                 Redirection channel reg 00001 MS BYTE   
  9  HEART:PULSE_C                                                          sub                                                             
 10  HEART:PULSE                                                            bi                                                              
 11  POND:LLS_ALARM_C                                                       sub                                                             
 12  POND:LLS_ALARM                                                         bi                                                              
 13  POND:HIGH_ALARM_C                                                      sub                                                             
 14  POND:HIGH_ALARM                                                        bi                                                              
 15  POND:LOW_ALARM_C                                                       sub                                                             
 16  POND:LOW_ALARM                                                         bi                                                              
 17  POND:LOW_WARN_C                                                        sub                                                             
 18  POND:LOW_WARN                                                          bi                                                              
 19  POND:TEMP_HIGH_ALARM_C                                                 sub                                                             
 20  POND:TEMP_HIGH_ALARM                                                   bi                                                              
 21  POND:TEMP_LOW_ALARM_C                                                  sub                                                             
 22  POND:TEMP_LOW_ALARM                                                    bi                                                              
 23  CONDUCT:HIGH_ALARM_C                                                   sub                                                             
 24  CONDUCT:HIGH_ALARM                                                     bi                                                              
 25  OUTPUT:TEMP_HIGH_ALARM_C                                               sub                                                             
 26  OUTPUT:TEMP_HIGH_ALARM                                                 bi                                                              
 27  DOLPHIN:FAULT_ALARM_C                                                  sub                                                             
 28  DOLPHIN:FAULT_ALARM                                                    bi                                                              
 29  SPRAY_PUMP:TRIP_ALARM_C                                                sub                                                             
 30  SPRAY_PUMP:TRIP_ALARM                                                  bi                                                              
 31  SIDE_STREAM:TRIP_ALARM_C                                               sub                                                             
 32  SIDE_STREAM:TRIP_ALARM                                                 bi                                                              
 33  DOSING:LOW_FLOW_ALARM_C                                                sub                                                             
 34  DOSING:LOW_FLOW_ALARM                                                  bi                                                              
 35  OUTPUT:TEMP_LOW_ALARM_C                                                sub                                                             
 36  OUTPUT:TEMP_LOW_ALARM                                                  bi                                                              
 37  00000:MULTIPLE_CALC                                                    calcout     1 second    Redirect_chan script                    
 38  00001:MULTIPLE_CALC                                                    calcout     1 second    Redirect_chan script                    