 
  Database(P): MEBT_RJ
  IP Address: None
 
NUM  PV Name                                                       Address  Type        Scan        DESC                                    
  1  SYSTEM:INFO                                                            bo                      MEBT Chopper Controller Test Database   
  2  TYPED_SYNC:INIT                                                        longout                 Used to Initialise the Type D Controlling the Sync Delay
  3  TYPED_OTHER:INIT                                                       longout                 Used to Initialise the Type D Controlling the RFQ Delay
  4  TYPES:INIT                                                             longout                 Used to Initialise the Type S Monitoring the Controller
  5  TYPES:INIT:HPARAM1                                                     longout                                                         
  6  TYPED_SYNC:ON_CH1                                                      longout                 ON - Bunches 0 to 29                    
  7  TYPED_SYNC:ON_CH2                                                      longout                 ON - Bunches 30 to 59                   
  8  TYPED_SYNC:ON_CH2:HPARAM1                                              longout                                                         
  9  TYPED_SYNC:ON_CH3                                                      longout                 ON - Bunches 60 to 89                   
 10  TYPED_SYNC:ON_CH3:HPARAM1                                              longout                                                         
 11  TYPED_SYNC:ON_CH4                                                      longout                 ON - Bunches 90 to 119                  
 12  TYPED_SYNC:ON_CH4:HPARAM1                                              longout                                                         
 13  TYPED_SYNC:ON_CH5                                                      longout                 ON - Bunches 120 to 159                 
 14  TYPED_SYNC:ON_CH5:HPARAM1                                              longout                                                         
 15  TYPED_SYNC:ON_CH6                                                      longout                 ON - Bunches 160 to 179                 
 16  TYPED_SYNC:ON_CH6:HPARAM1                                              longout                                                         
 17  TYPED_SYNC:ON_CH7                                                      longout                 ON - Bunches 180 to 209                 
 18  TYPED_SYNC:ON_CH7:HPARAM1                                              longout                                                         
 19  TYPED_SYNC:ON_CH8                                                      longout                 ON - Bunches 210 to 239                 
 20  TYPED_SYNC:ON_CH8:HPARAM1                                              longout                                                         
 21  TYPED_SYNC:ON_CH9                                                      longout                 ON - Bunches 240 to 269                 
 22  TYPED_SYNC:ON_CH9:HPARAM1                                              longout                                                         
 23  TYPED_SYNC:ON_CH10                                                     longout                 ON - Bunches 270 to 299                 
 24  TYPED_SYNC:ON_CH10:HPARAM1                                             longout                                                         
 25  TYPED_SYNC:ON_CH11                                                     longout                 ON - Bunches 300 to 329                 
 26  TYPED_SYNC:ON_CH11:HPARAM1                                             longout                                                         
 27  TYPED_SYNC:ON_CH12                                                     longout                 ON - Bunches 330 to 359                 
 28  TYPED_SYNC:ON_CH12:HPARAM1                                             longout                                                         
 29  TYPED_SYNC:ON_CH13                                                     longout                 ON - Bunches 360 to 389                 
 30  TYPED_SYNC:ON_CH13:HPARAM1                                             longout                                                         
 31  TYPED_SYNC:ON_CH14                                                     longout                 ON - Bunch 390                          
 32  TYPED_SYNC:ON_CH14:HPARAM1                                             longout                                                         
 33  TYPED_SYNC:OFF_CH1                                                     longout                 OFF - Bunches 0 to 29                   
 34  TYPED_SYNC:OFF_CH1:HPARAM1                                             longout                                                         
 35  TYPED_SYNC:OFF_CH2                                                     longout                 OFF - Bunches 30 to 59                  
 36  TYPED_SYNC:OFF_CH2:HPARAM1                                             longout                                                         
 37  TYPED_SYNC:OFF_CH3                                                     longout                 OFF - Bunches 60 to 89                  
 38  TYPED_SYNC:OFF_CH3:HPARAM1                                             longout                                                         
 39  TYPED_SYNC:OFF_CH4                                                     longout                 OFF - Bunches 90 to 119                 
 40  TYPED_SYNC:OFF_CH4:HPARAM1                                             longout                                                         
 41  TYPED_SYNC:OFF_CH5                                                     longout                 OFF - Bunches 120 to 159                
 42  TYPED_SYNC:OFF_CH5:HPARAM1                                             longout                                                         
 43  TYPED_SYNC:OFF_CH6                                                     longout                 OFF - Bunches 160 to 179                
 44  TYPED_SYNC:OFF_CH6:HPARAM1                                             longout                                                         
 45  TYPED_SYNC:OFF_CH7                                                     longout                 OFF - Bunches 180 to 209                
 46  TYPED_SYNC:OFF_CH7:HPARAM1                                             longout                                                         
 47  TYPED_SYNC:OFF_CH8                                                     longout                 OFF - Bunches 210 to 239                
 48  TYPED_SYNC:OFF_CH8:HPARAM1                                             longout                                                         
 49  TYPED_SYNC:OFF_CH9                                                     longout                 OFF - Bunches 240 to 269                
 50  TYPED_SYNC:OFF_CH9:HPARAM1                                             longout                                                         
 51  TYPED_SYNC:OFF_CH10                                                    longout                 OFF - Bunches 270 to 299                
 52  TYPED_SYNC:OFF_CH10:HPARAM1                                            longout                                                         
 53  TYPED_SYNC:OFF_CH11                                                    longout                 OFF - Bunches 300 to 329                
 54  TYPED_SYNC:OFF_CH11:HPARAM1                                            longout                                                         
 55  TYPED_SYNC:OFF_CH12                                                    longout                 OFF - Bunches 330 to 359                
 56  TYPED_SYNC:OFF_CH12:HPARAM1                                            longout                                                         
 57  TYPED_SYNC:OFF_CH13                                                    longout                 OFF - Bunches 360 to 389                
 58  TYPED_SYNC:OFF_CH13:HPARAM1                                            longout                                                         
 59  TYPED_SYNC:OFF_CH14                                                    longout                 OFF - Bunch 390                         
 60  TYPED_SYNC:OFF_CH14:HPARAM1                                            longout                                                         
 61  TYPED_SYNC:CMD_SET                                                     bo                      Starts the MEBT Data Transmission       
 62  TYPED_OTHER:SET_RFQ_DELAY                                              longout                 Set the RFQ Delay                       
 63  TYPED_OTHER:SET_PRE_DELAY                                              longout                 Set the RFQ Delay                       
 64  TYPED_OTHER:SET_PRE_DELAY:HPARAM1                                      longout                                                         
 65  TIMER_1:CH1_DELP                                                       longout                 T-Channel 1 Delta P                     
 66  TIMER_1:CH1_MS_K                                                       longout                 T-Channel 1 MS/k                        
 67  TIMER_1:CH1_ENB                                                        bo                      T-Channel 1 enable                      
 68  TIMER_1:CH1_MMS                                                        longout                 T-Channel 1 MMS                         
 69  TIMER_1:CH1_STA                                                        longout                 T-Channel 1 Status                      
 70  TIMER_1:CH2_DELP                                                       longout                 T-Channel 2 Delta P                     
 71  TIMER_1:CH2_MS_K                                                       longout                 T-Channel 2 MS/k                        
 72  TIMER_1:CH2_ENB                                                        bo                      T-Channel 2 enable                      
 73  TIMER_1:CH2_MMS                                                        longout                 T-Channel 2 MMS                         
 74  TIMER_1:CH2_STA                                                        longout                 T-Channel 2 Status                      
 75  TIMER_1:CH3_DELP                                                       longout                 T-Channel 3 Delta P                     
 76  TIMER_1:CH3_MS_K                                                       longout                 T-Channel 3 MS/k                        
 77  TIMER_1:CH3_ENB                                                        bo                      T-Channel 3 enable                      
 78  TIMER_1:CH3_MMS                                                        longout                 T-Channel 3 MMS                         
 79  TIMER_1:CH3_STA                                                        longout                 T-Channel 3 Status                      
 80  TIMER_1:CH4_DELP                                                       longout                 T-Channel 4 Delta P                     
 81  TIMER_1:CH4_MS_K                                                       longout                 T-Channel 4 MS/k                        
 82  TIMER_1:CH4_ENB                                                        bo                      T-Channel 4 enable                      
 83  TIMER_1:CH4_MMS                                                        longout                 T-Channel 4 MMS                         
 84  TIMER_1:CH4_STA                                                        longout                 T-Channel 4 Status                      
 85  TYPES:STATUS                                                           longin      1 second    Read Status of MEBT Controller          
 86  TYPES:STATUS_MULTIPLY                                                  dfanout                                                         
 87  TYPES:STATUS_0_C                                                       sub                     Read Status 1 of MEBT Controller sub    
 88  TYPES:STATUS_0                                                         bi                      Read Status 1 of MEBT Controller        
 89  TYPES:STATUS_1_C                                                       sub                     Read Status 2 of MEBT Controller sub    
 90  TYPES:STATUS_1                                                         bi                      Read Status 2 of MEBT Controller        
 91  TYPES:STATUS_2_C                                                       sub                     Read Status 3 of MEBT Controller sub    
 92  TYPES:STATUS_2                                                         bi                      Read Status 3 of MEBT Controller        