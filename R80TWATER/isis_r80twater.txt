 
  Database(P): R80TWATER
  IP Address: 172.16.88.141
 
NUM  PV Name                                                       Address  Type        Scan        DESC                                    
  1  PLC:CLOCK                                                     19700    longin      1 second    PLC Clock                               
  2  REGISTER:19701                                                19701    longin      1 second    plant alarm bits                        
  3  REGISTER:19702                                                19702    longin      1 second    plant alarm bits                        
  4  REGISTER:19703                                                19703    longin      1 second    plant alarm bits                        
  5  TGT_CCT:PUMP1_FREQ_FB                                         19704    ai          1 second    Pump CP3A frequency feedback            
  6  TGT_CCT:PUMP2_FREQ_FB                                         19705    ai          1 second    Pump CP3B frequency feedback            
  7  TGT_CCT:SUPPLY_PRESSURE_PT02                                  19706    ai          1 second    Target supply pressure                  
  8  TGT_CCT:RETURN_PRESSURE_PT01                                  19707    ai          1 second    Target return pressure                  
  9  TGT_CCT:RETURN_TEMPERATURE_TT02                               19708    ai          1 second    Target return temperature               
 10  HEAT_EXCHR_1:OUTPUT_TEMPERATURE_TT01                          19709    ai          1 second    Heat exchanger HX05A output temperature 
 11  TGT_CCT:SUPPLY_TEMPERATURE_TT03                               19710    ai          1 second    Target supply temperature               
 12  TGT_CCT:FLOW_FT01                                             19711    ai          1 second    Target flow                             
 13  TOWRWAT:SUPPLY_TEMPERATURE_TT03                               19712    ai          1 second    Tower supply temperature                
 14  TOWRWAT:RETURN_TEMPERATURE_TT04                               19713    ai          1 second    Tower return temperature                
 15  CHILLWAT:SUPPLY_TEMPERATURE_TT08                              19714    ai          1 second    Chiller supply temperature              
 16  CHILLWAT:RETURN_TEMPERATURE_TT09                              19715    ai          1 second    Chiller return temperature              
 17  HEADTANK:LEVEL_LT01                                           19716    ai          1 second    Header tank level                       
 18  HEAT_EXCHR:VALVE_POS_HX05                                     19720    ai          1 second    Heat Exchanger HX05 valve (CV19) position
 19  HEAT_EXCHR:VALVE_POS_HX05A                                    19721    ai          1 second    Heat Exchanger HX05 valve (CV09) position
 20  CHILLWAT:FLOW_FT04                                            19722    ai          1 second    Chilled water flow                      
 21  TROLLEY:RAW_SUP_FLOW                                          19723    ai          1 second    Trolley raw water flow                  
 22  TROLLEY:RAW_RET_FLOW                                          19724    ai          1 second    Trolley raw water flow                  
 23  19701:MULTIPLE                                                         dfanout                 redirection channel reg 19701           
 24  19702:MULTIPLE                                                         dfanout                 redirection channel reg 19702           
 25  19701_A:MULTIPLE                                                       dfanout                 Redirection channel reg 19701 LS BYTE   
 26  19701_B:MULTIPLE                                                       dfanout                 Redirection channel reg 19701 MS BYTE   
 27  19702_A:MULTIPLE                                                       dfanout                 Redirection channel reg 19702 LS BYTE   
 28  HEADTANK:HI_C                                                          sub                                                             
 29  HEADTANK:HI                                                            bi                                                              
 30  HEADTANK:LO_C                                                          sub                                                             
 31  HEADTANK:LO                                                            bi                                                              
 32  HEADTANK:LOLO_C                                                        sub                                                             
 33  HEADTANK:LOLO                                                          bi                                                              
 34  HEADTANK:LLS_C                                                         sub                                                             
 35  HEADTANK:LLS                                                           bi                                                              
 36  HEADTANK:PUMP1_OVERLOAD_C                                              sub                                                             
 37  HEADTANK:PUMP1_OVERLOAD                                                bi                                                              
 38  HEADTANK:PUMP2_OVERLOAD_C                                              sub                                                             
 39  HEADTANK:PUMP2_OVERLOAD                                                bi                                                              
 40  TGT_CCT:SUPPLY_PRESS_LO_C                                              sub                                                             
 41  TGT_CCT:SUPPLY_PRESS_LO                                                bi                                                              
 42  TGT_CCT:SUPPLY_PRESS_HI_C                                              sub                                                             
 43  TGT_CCT:SUPPLY_PRESS_HI                                                bi                                                              
 44  TGT_CCT:RETURN_PRESS_LO_C                                              sub                                                             
 45  TGT_CCT:RETURN_PRESS_LO                                                bi                                                              
 46  TGT_CCT:RETURN_PRESS_LOTRIP_C                                          sub                                                             
 47  TGT_CCT:RETURN_PRESS_LOTRIP                                            bi                                                              
 48  TGT_CCT:RETURN_PRESS_HI_C                                              sub                                                             
 49  TGT_CCT:RETURN_PRESS_HI                                                bi                                                              
 50  TGT_CCT:FLOW_LO_C                                                      sub                                                             
 51  TGT_CCT:FLOW_LO                                                        bi                                                              
 52  TGT_CCT:TEMP_HI_TT01_C                                                 sub                                                             
 53  TGT_CCT:TEMP_HI_TT01                                                   bi                                                              
 54  TGT_CCT:TEMP_HI_TT02_C                                                 sub                                                             
 55  TGT_CCT:TEMP_HI_TT02                                                   bi                                                              
 56  TGT_CCT:TEMP_HI_TT03_C                                                 sub                                                             
 57  TGT_CCT:TEMP_HI_TT03                                                   bi                                                              
 58  TOWRWAT:TEMP_HI_TT03_C                                                 sub                                                             
 59  TOWRWAT:TEMP_HI_TT03                                                   bi                                                              
 60  TOWRWAT:TEMP_HI_TT04_C                                                 sub                                                             
 61  TOWRWAT:TEMP_HI_TT04                                                   bi                                                              
 62  CHILLWAT:TEMP_HI_TT09_C                                                sub                                                             
 63  CHILLWAT:TEMP_HI_TT09                                                  bi                                                              
 64  CHILLWAT:TEMP_HI_TT08_C                                                sub                                                             
 65  CHILLWAT:TEMP_HI_TT08                                                  bi                                                              
 66  EMERGENCY:STOP_C                                                       sub                                                             
 67  EMERGENCY:STOP                                                         bi                                                              
 68  TGT_CCT:FIL_DIFF_PRESS_C                                               sub                                                             
 69  TGT_CCT:FIL_DIFF_PRESS                                                 bi                                                              
 70  TGT_CCT:TEMP_LO_TT03_C                                                 sub                                                             
 71  TGT_CCT:TEMP_LO_TT03                                                   bi                                                              
 72  CHILLWAT:FLOW_LO_C                                                     sub                                                             
 73  CHILLWAT:FLOW_LO                                                       bi                                                              
 74  TROLLEY:FLOW_LO_C                                                      sub                                                             
 75  TROLLEY:FLOW_LO                                                        bi                                                              
 76  19701:MULTIPLE_CALC                                                    calcout     1 second    Redirect_chan script                    
 77  19702:MULTIPLE_CALC                                                    calcout     1 second    Redirect_chan script                    