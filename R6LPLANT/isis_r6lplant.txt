 
  Database(P): R6LPLANT
  IP Address: 130.246.55.169
 
NUM  PV Name                                                       Address  Type        Scan        DESC                                    
  1  ALARMS:RESET                                                  19790    bo                      Remote alarm reset                      
  2  ALARMS:RESET_R                                                19790    bi          .1 second   Remote alarm reset readback             
  3  ALARMS:RESET_C                                                         calcout                 Remote alarm reset clear                
  4  HEARTBEAT:READ                                                19700    longin      1 second    R6 Link water heartbeat                 
  5  WATER_1:READ                                                  19701    longin      1 second    Mag and R80 water hardware read         
  6  WATER_2:READ                                                  19702    longin      1 second    PSU water hardware read                 
  7  WATER_1:MULTIPLY                                                       dfanout                 Mag and R80 water multiply              
  8  WATER_1:MULTIPLY_SUB1                                                  dfanout                                                         
  9  WATER_1:MULTIPLY_SUB2                                                  dfanout                                                         
 10  WATER_2:MULTIPLY                                                       dfanout                 PSU water multiply                      
 11  WATER_2:MULTIPLY_SUB1                                                  dfanout                                                         
 12  WATER_2:MULTIPLY_SUB2                                                  dfanout                                                         
 13  MAG_DEMIN:TEMP_HIGH_C                                                  sub                     Mag Demin high temp alarm sub           
 14  MAG_DEMIN:TEMP_HIGH                                                    bi                      Mag Demin high temp alarm               
 15  MAG_DEMIN:TEMP_LOW_C                                                   sub                     Mag Demin low temp alarm sub            
 16  MAG_DEMIN:TEMP_LOW                                                     bi                      Mag Demin low temp alarm                
 17  MAG_DEMIN:PRESSURE_LOW_C                                               sub                     Mag Demin low pressure alarm sub        
 18  MAG_DEMIN:PRESSURE_LOW                                                 bi                      Mag Demin low pressure alarm            
 19  MAG:COND_HIGH_C                                                        sub                     Mag conductivity high sub               
 20  MAG:COND_HIGH                                                          bi                      Mag conductivity high                   
 21  MAG:O2_HIGH_C                                                          sub                     Mag oxygen high alarm sub               
 22  MAG:O2_HIGH                                                            bi                      Mag oxygen high alarm                   
 23  MAG_TANK:LLLS_C                                                        sub                     Mag tank llls alarm sub                 
 24  MAG_TANK:LLLS                                                          bi                      Mag tank llls alarm                     
 25  R80_CHILLED:TEMP_HIGH_C                                                sub                     R80 chilled high temp alarm sub         
 26  R80_CHILLED:TEMP_HIGH                                                  bi                      R80 chilled high temp alarm             
 27  R80_TOWER:TEMP_HIGH_C                                                  sub                     R80 tower high temp alarm sub           
 28  R80_TOWER:TEMP_HIGH                                                    bi                      R80 tower high temp alarm               
 29  R80_CHILLED:FLOW_LOW_C                                                 sub                     R80 chilled low flow alarm sub          
 30  R80_CHILLED:FLOW_LOW                                                   bi                      R80 chilled low flow alarm              
 31  R80_TOWER:FLOW_LOW_C                                                   sub                     R80 tower low flow alarm sub            
 32  R80_TOWER:FLOW_LOW                                                     bi                      R80 tower low flow alarm                
 33  R80_SUMP:LEVEL_HIGH_C                                                  sub                     R80 sump level high alarm sub           
 34  R80_SUMP:LEVEL_HIGH                                                    bi                      R80 sump level high alarm               
 35  SPARE_1_C                                                              sub                     Spare sub                               
 36  SPARE_1                                                                bi                      Spare                                   
 37  SPARE_2_C                                                              sub                     Spare sub                               
 38  SPARE_2                                                                bi                      Spare                                   
 39  SPARE_3_C                                                              sub                     Spare sub                               
 40  SPARE_3                                                                bi                      Spare                                   
 41  SPARE_4_C                                                              sub                     Spare sub                               
 42  SPARE_4                                                                bi                      Spare                                   
 43  SPARE_5_C                                                              sub                     Spare sub                               
 44  SPARE_5                                                                bi                      Spare                                   
 45  PSU_DEMIN:TEMP_HIGH_C                                                  sub                     PSU demin temp high alarm sub           
 46  PSU_DEMIN:TEMP_HIGH                                                    bi                      PSU demin temp high alarm               
 47  PSU_DEMIN:TEMP_LOW_C                                                   sub                     PSU demin temp low alarm sub            
 48  PSU_DEMIN:TEMP_LOW                                                     bi                      PSU demin temp low alarm                
 49  PSU_DEMIN:PRESS_LOW_C                                                  sub                     PSU demin pressure low alarm sub        
 50  PSU_DEMIN:PRESS_LOW                                                    bi                      PSU demin pressure low alarm            
 51  PSU:COND_HIGH_C                                                        sub                     PSU conductivity high alarm sub         
 52  PSU:COND_HIGH                                                          bi                      PSU conductivity high alarm             
 53  PSU_HEADER:LLS_C                                                       sub                     PSU header tank LLS alarm sub           
 54  PSU_HEADER:LLS                                                         bi                      PSU header tank LLS alarm               
 55  SPARE_6_C                                                              sub                     Spare sub                               
 56  SPARE_6                                                                bi                      Spare                                   
 57  SPARE_7_C                                                              sub                     Spare sub                               
 58  SPARE_7                                                                bi                      Spare                                   
 59  SPARE_8_C                                                              sub                     Spare sub                               
 60  SPARE_8                                                                bi                      Spare                                   
 61  SPARE_9_C                                                              sub                     Spare sub                               
 62  SPARE_9                                                                bi                      Spare                                   
 63  SPARE_10_C                                                             sub                     Spare sub                               
 64  SPARE_10                                                               bi                      Spare                                   
 65  SPARE_11_C                                                             sub                     Spare sub                               
 66  SPARE_11                                                               bi                      Spare                                   
 67  SPARE_12_C                                                             sub                     Spare sub                               
 68  SPARE_12                                                               bi                      Spare                                   
 69  SPARE_13_C                                                             sub                     Spare sub                               
 70  SPARE_13                                                               bi                      Spare                                   
 71  SPARE_14_C                                                             sub                     Spare sub                               
 72  SPARE_14                                                               bi                      Spare                                   
 73  SPARE_15_C                                                             sub                     Spare sub                               
 74  SPARE_15                                                               bi                      Spare                                   
 75  SPARE_16_C                                                             sub                     Spare sub                               
 76  SPARE_16                                                               bi                      Spare                                   