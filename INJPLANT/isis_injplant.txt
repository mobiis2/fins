 
  Database(P): INJPLANT
  IP Address: 130.246.88.95
 
NUM  PV Name                                                       Address  Type        Scan        DESC                                    
  1  SYSTEM:ALIVE                                                  19700    longin      1 second    Injector Plant clock increment mm_ss    
  2  DEMIN_WATER_REG1:READ                                         19701    longin      1 second    Injector Plant Demin Water Register 1 read
  3  DEMIN_WATER_REG2:READ                                         19702    longin      1 second    Injector Plant Demin Water Register 2 read
  4  DEMIN_WATER_REG1:MULTIPLY                                              dfanout                 HEDS Klixons Register 1 redirection channel list
  5  DEMIN_WATER_REG1_1:MULTIPLY                                            dfanout                 HEDS Klixons Register 1.1 redirection channel list
  6  DEMIN_WATER_REG1_2:MULTIPLY                                            dfanout                 HEDS Klixons Register 1.2 redirection channel list
  7  DEMIN_WATER:HEADER_TANK_LOW:ALARM_C                                    sub                     70MeV Demin Water - Header Tank LOW sub 
  8  DEMIN_WATER:HEADER_TANK_LOW:ALARM                                      bi                      70MeV Demin Water - Header Tank LOW     
  9  DEMIN_WATER:HEADER_TANK_EMPTY:ALARM_C                                  sub                     70MeV Demin Water - Header Tank EMPTY sub
 10  DEMIN_WATER:HEADER_TANK_EMPTY:ALARM                                    bi                      70MeV Demin Water - Header Tank EMPTY   
 11  DEMIN_WATER:HEADER_TANK_OVERFLOW:ALARM_C                               sub                     70MeV Demin Water - Header Tank OVERFLOW sub
 12  DEMIN_WATER:HEADER_TANK_OVERFLOW:ALARM                                 bi                      70MeV Demin Water - Header Tank OVERFLOW
 13  DEMIN_WATER:HEADER_TANK_LLS:ALARM_C                                    sub                     70MeV Demin Water - Header Tank LOW LOW SWITCH sub
 14  DEMIN_WATER:HEADER_TANK_LLS:ALARM                                      bi                      70MeV Demin Water - Header Tank LOW LOW SWITCH
 15  DEMIN_WATER:CONDUCTIVITY_HIGH:ALARM_C                                  sub                     70MeV Demin Water - Conductivity HIGH sub
 16  DEMIN_WATER:CONDUCTIVITY_HIGH:ALARM                                    bi                      70MeV Demin Water - Conductivity HIGH   
 17  DEMIN_WATER:O2_HIGH:ALARM_C                                            sub                     70MeV Demin Water - Oxygen Content HIGH sub
 18  DEMIN_WATER:O2_HIGH:ALARM                                              bi                      70MeV Demin Water - Oxygen Content HIGH 
 19  DEMIN_WATER:PRESSURE_HIGH:ALARM_C                                      sub                     70MeV Demin Water - System PRESSURE HIGH ALARM sub
 20  DEMIN_WATER:PRESSURE_HIGH:ALARM                                        bi                      70MeV Demin Water - System PRESSURE HIGH ALARM
 21  DEMIN_WATER:TEMP_HIGH:ALARM_C                                          sub                     70MeV Demin Water - Supply Temperature HIGH ALARM sub
 22  DEMIN_WATER:TEMP_HIGH:ALARM                                            bi                      70MeV Demin Water - Supply Temperature HIGH ALARM
 23  DEMIN_WATER:TEMP_LOW:ALARM_C                                           sub                     70MeV Demin Water - Supply Temperature LOW ALARM sub
 24  DEMIN_WATER:TEMP_LOW:ALARM                                             bi                      70MeV Demin Water - Supply Temperature LOW ALARM
 25  DEMIN_WATER:PRESSURE_LOW:ALARM_C                                       sub                     70MeV Demin Water - System PRESSURE LOW ALARM sub
 26  DEMIN_WATER:PRESSURE_LOW:ALARM                                         bi                      70MeV Demin Water - System PRESSURE LOW ALARM
 27  DEMIN_WATER:PRESSURE_OVER:TRIP_C                                       sub                     70MeV Demin Water - System OVER PRESSURE TRIP sub
 28  DEMIN_WATER:PRESSURE_OVER:TRIP                                         bi                      70MeV Demin Water - System OVER PRESSURE TRIP
 29  CHILLED_WATER:FAULT:ALARM_C                                            sub                     70MeV Chilled Water - FAULT sub         
 30  CHILLED_WATER:FAULT:ALARM                                              bi                      70MeV Chilled Water - FAULT             
 31  SUMP:FLOODED:ALARM_C                                                   sub                     70MeV Sump Flooded sub                  
 32  SUMP:FLOODED:ALARM                                                     bi                      70MeV Sump Flooded                      
 33  DEMIN_WATER:TEMP_HIGH:TRIP_C                                           sub                     70MeV Demin Water - SYSTEM TRIP Temperature High sub
 34  DEMIN_WATER:TEMP_HIGH:TRIP                                             bi                      70MeV Demin Water - SYSTEM TRIP Temperature High
 35  DEMIN_WATER:TEMP_LOW:TRIP_C                                            sub                     70MeV Demin Water - SYSTEM TRIP Temperature Low sub
 36  DEMIN_WATER:TEMP_LOW:TRIP                                              bi                      70MeV Demin Water - SYSTEM TRIP Temperature Low
 37  DEMIN_WATER:FILTER_BLOCKED:ALARM_C                                     sub                     70MeV Demin Water - Filter Blocked sub  
 38  DEMIN_WATER:FILTER_BLOCKED:ALARM                                       bi                      70MeV Demin Water - Filter Blocked      
 39  DEMIN_WATER_REG2:MULTIPLY                                              dfanout                 HEDS Klixons Register 2 redirection channel list
 40  DEMIN_WATER_REG2_1:MULTIPLY                                            dfanout                 HEDS Klixons Register 2.1 redirection channel list
 41  DEMIN_WATER_REG2_2:MULTIPLY                                            dfanout                 HEDS Klixons Register 2.2 redirection channel list
 42  DEMIN_WATER:M800_FLOW:ALARM_C                                          sub                     70MeV Demin Water - Parameter Monitoring Low Flow sub
 43  DEMIN_WATER:M800_FLOW:ALARM                                            bi                      70MeV Demin Water - Parameter Monitoring Low Flow
 44  LT01:READ                                                     19704    longin      1 second    Header tank sensor                      
 45  PT01:READ                                                     19705    ai          1 second    Demin Supply Pressure                   
 46  FT01:READ                                                     19706    ai          1 second    Demin Return Flow                       
 47  PT02:READ                                                     19707    ai          1 second    Demin Return Pressure                   
 48  FT02:READ                                                     19708    ai          1 second    Demin Return Flow                       
 49  TT01:READ                                                     19709    ai          1 second    Demin Supply Temperature                
 50  SPARE_1                                                       19709    longin      1 second                                            
 51  TT02:READ                                                     19711    ai          1 second    Demin Return Temperature                
 52  COND:READ                                                     19712    ai          1 second    M800 Conductivity                       
 53  PUMP1_FEEDBACK:READ                                           19713    ai          1 second    Pump 1 Invertor feedback                
 54  O2:READ                                                       19714    ai          1 second    M800 Oxygen Content                     
 55  PUMP2_FEEDBACK:READ                                           19715    ai          1 second    Pump 2 Invertor feedback                
 56  PH:READ                                                       19716    ai          1 second    M800 Acidity                            
 57  SPARE_2                                                       19717    longin      1 second                                            
 58  SPARE_3                                                       19718    longin      1 second                                            
 59  SPARE_4                                                       19719    longin      1 second                                            
 60  PUMP1_REF:READ                                                19720    ai          1 second    Pump 1 Invertor speed reference         
 61  V18_REF:READ                                                  19721    ai          1 second    Heatex Valve Posirion reference         
 62  PUMP2_REF:READ                                                19722    ai          1 second    Pump 2 Invertor speed reference         
 63  SPARE_5                                                       19723    longin      1 second                                            
 64  PUMP1_VALVEPOS:READ                                           19724    longin      1 second    Pump 1 Valve Position reference         
 65  SPARE_6                                                       19725    longin      1 second                                            
 66  PUMP2_VALVEPOS:READ                                           19726    longin      1 second    Pump 2 Valve Position reference         
 67  SPARE_7                                                       19727    longin      1 second                                            
 68  HEAT:READ                                                     19728    ai          1 second    Calculate Heat Load                     