 
  Database(P): R80COSHHEAST
  IP Address: 172.16.88.147
 
NUM  PV Name                                                       Address  Type        Scan        DESC                                    
  1  EXTRACTION:FLOW_STATUS_READ                                   2        longin      1 second    R80 COSHH East Extraction Flow Status   
  2  EXTRACTION:FLOW_STATUS_MULTIPLY                                        dfanout                 Duplicate R80 COSHH East Extraction Flow Status
  3  EXTRACTION:FLOW_STATUS_MULTIPLY_SUB1                                   dfanout                                                         
  4  EXTRACTION:FLOW_STATUS_MULTIPLY_SUB2                                   dfanout                                                         
  5  EXTRACTION:A:DIFF_PRESSURE:HIGH_C                                      sub                     EXTRACTION A DIFFERENTIAL PRESSURE HIGH sub
  6  EXTRACTION:A:DIFF_PRESSURE:HIGH                                        bi                      EXTRACTION A DIFFERENTIAL PRESSURE HIGH 
  7  EXTRACTION:A:DIFF_PRESSURE:LOW_C                                       sub                     EXTRACTION A DIFFERENTIAL PRESSURE LOW sub
  8  EXTRACTION:A:DIFF_PRESSURE:LOW                                         bi                      EXTRACTION A DIFFERENTIAL PRESSURE LOW  
  9  EXTRACTION:B:DIFF_PRESSURE:HIGH_C                                      sub                     EXTRACTION B DIFFERENTIAL PRESSURE HIGH sub
 10  EXTRACTION:B:DIFF_PRESSURE:HIGH                                        bi                      EXTRACTION B DIFFERENTIAL PRESSURE HIGH 
 11  EXTRACTION:B:DIFF_PRESSURE:LOW_C                                       sub                     EXTRACTION B DIFFERENTIAL PRESSURE LOW sub
 12  EXTRACTION:B:DIFF_PRESSURE:LOW                                         bi                      EXTRACTION B DIFFERENTIAL PRESSURE LOW  
 13  SPARE:BIT:4_C                                                          sub                     SPARE BIT 4 sub                         
 14  SPARE:BIT:4                                                            bi                      SPARE BIT 4                             
 15  SPARE:BIT:5_C                                                          sub                     SPARE BIT 5 sub                         
 16  SPARE:BIT:5                                                            bi                      SPARE BIT 5                             
 17  EXTRACTION:A:FLOW:LOW_C                                                sub                     EXTRACTION A FLOW LOW sub               
 18  EXTRACTION:A:FLOW:LOW                                                  bi                      EXTRACTION A FLOW LOW                   
 19  EXTRACTION:B:FLOW:LOW_C                                                sub                     EXTRACTION B FLOW LOW sub               
 20  EXTRACTION:B:FLOW:LOW                                                  bi                      EXTRACTION B FLOW LOW                   
 21  EXTRACTION:ESTOP_C                                                     sub                     EXTRACTION EMERGENCY STOP sub           
 22  EXTRACTION:ESTOP                                                       bi                      EXTRACTION EMERGENCY STOP               
 23  EXTRACTION:A:VSD_FAULT_C                                               sub                     EXTRACTION A VSD FAULT sub              
 24  EXTRACTION:A:VSD_FAULT                                                 bi                      EXTRACTION A VSD FAULT                  
 25  EXTRACTION:B1:VSD_FAULT_C                                              sub                     EXTRACTION B1 VSD FAULT sub             
 26  EXTRACTION:B1:VSD_FAULT                                                bi                      EXTRACTION B1 VSD FAULT                 
 27  EXTRACTION:B2:VSD_FAULT_C                                              sub                     EXTRACTION B2 VSD FAULT sub             
 28  EXTRACTION:B2:VSD_FAULT                                                bi                      EXTRACTION B2 VSD FAULT                 
 29  SPARE:BIT:12_C                                                         sub                     SPARE BIT 12 sub                        
 30  SPARE:BIT:12                                                           bi                      SPARE BIT 12                            
 31  SPARE:BIT:13_C                                                         sub                     SPARE BIT 13 sub                        
 32  SPARE:BIT:13                                                           bi                      SPARE BIT 13                            
 33  SPARE:BIT:14_C                                                         sub                     SPARE BIT 14 sub                        
 34  SPARE:BIT:14                                                           bi                      SPARE BIT 14                            
 35  SPARE:BIT:15_C                                                         sub                     SPARE BIT 15 sub                        
 36  SPARE:BIT:15                                                           bi                      SPARE BIT 15                            
 37  EXTRACTION:A:FAN_1:SPEED                                      19       ai          1 second    EXTRACTION A FAN 1 SPEED                
 38  EXTRACTION:B:FAN_1:SPEED                                      20       ai          1 second    EXTRACTION B FAN 1 SPEED                
 39  EXTRACTION:B:FAN_2:SPEED                                      21       ai          1 second    EXTRACTION B FAN 2 SPEED                
 40  EXTRACTION:A:DIFF_PRESSURE                                    22       ai          1 second    EXTRACTION A DIFFERENTIAL PRESSURE      
 41  EXTRACTION:B:DIFF_PRESSURE                                    23       ai          1 second    EXTRACTION B DIFFERENTIAL PRESSURE      
 42  SPARE:INT:24                                                  24       longin      1 second    SPARE INT 24                            
 43  EXTRACTION:A:FT1_FLOW                                         25       ai          1 second    EXTRACTION A FT1 FLOW                   
 44  EXTRACTION:B:FT2_FLOW                                         26       ai          1 second    EXTRACTION A FT1 FLOW                   
 45  EXTRACTION:A:FAN_1:CURRENT                                    27       ai          1 second    EXTRACTION A FAN 1 CURRENT              
 46  EXTRACTION:B:FAN_1:CURRENT                                    28       ai          1 second    EXTRACTION B FAN 1 CURRENT              
 47  EXTRACTION:B:FAN_2:CURRENT                                    29       ai          1 second    EXTRACTION B FAN 2 CURRENT              
 48  SPARE:INT:30                                                  30       longin      1 second    SPARE INT 30                            
 49  SPARE:INT:31                                                  31       longin      1 second    SPARE INT 31                            
 50  SPARE:INT:32                                                  32       longin      1 second    SPARE INT 32                            
 51  SPARE:INT:33                                                  33       longin      1 second    SPARE INT 33                            
 52  SPARE:INT:34                                                  34       longin      1 second    SPARE INT 34                            
 53  SPARE:INT:35                                                  35       longin      1 second    SPARE INT 35                            
 54  SPARE:INT:36                                                  36       longin      1 second    SPARE INT 36                            
 55  SPARE:INT:37                                                  37       longin      1 second    SPARE INT 37                            
 56  SPARE:INT:38                                                  38       longin      1 second    SPARE INT 38                            
 57  SPARE:INT:39                                                  39       longin      1 second    SPARE INT 39                            
 58  SPARE:INT:40                                                  40       longin      1 second    SPARE INT 40                            
 59  SPARE:INT:41                                                  41       longin      1 second    SPARE INT 41                            
 60  SPARE:INT:42                                                  42       longin      1 second    SPARE INT 42                            
 61  SPARE:INT:43                                                  43       longin      1 second    SPARE INT 43                            
 62  SPARE:INT:44                                                  44       longin      1 second    SPARE INT 44                            
 63  SPARE:INT:45                                                  45       longin      1 second    SPARE INT 45                            
 64  SPARE:INT:46                                                  46       longin      1 second    SPARE INT 46                            
 65  SPARE:INT:47                                                  47       longin      1 second    SPARE INT 47                            
 66  SPARE:INT:48                                                  48       longin      1 second    SPARE INT 48                            
 67  SPARE:INT:49                                                  49       longin      1 second    SPARE INT 49                            