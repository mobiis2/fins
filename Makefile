# @project EPICS Configs Archiver Makefile 

SUBDIRS := $(wildcard */)

ZIPFILES := $(SUBDIRS:%/=%.zip)

all: $(ZIPFILES)

%.zip: %
	zip -0 -j -r $@ $<

clean: 
	rm $(ZIPFILES)

.PHONY: all clean
