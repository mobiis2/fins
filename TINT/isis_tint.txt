 
  Database(P): TINT
  IP Address: 130.246.91.15
 
NUM  PV Name                                                       Address  Type        Scan        DESC                                    
  1  SYSTEM:TELL-TALE                                              19500    bi          1 second    System tell-tale                        
  2  TGT_CURRENT:READ_POS                                          19502    ai          1 second    Target Current Position                 
  3  SUPPLY_10V:READ_VOLTS                                         19504    ai          1 second    10V Supply - Read Volts                 
  4  PROF_CURRENT:READ_POS                                         19506    ai          1 second    Profile Current Position                
  5  OUT_POS:READ_HILIM                                            19508    ai          1 second    Out Position - Read High Limit          
  6  OUT_POS:READ_LOLIM                                            19510    ai          1 second    Out Position - Read Low Limit           
  7  OUT_POS:READ_SET_POINT                                        19512    ai          1 second    Out Position - Read Set Point           
  8  TGT_1_POS:READ_HILIM                                          19514    ai          1 second    Target 1 Position - Read High Limit     
  9  TGT_1_POS:READ_LOLIM                                          19516    ai          1 second    Target 1 Position - Read Lower Limit    
 10  TGT_1_POS:READ_SET_POINT                                      19518    ai          1 second    Target 1 Position - Read Set Point      
 11  TGT_2_POS:READ_HILIM                                          19520    ai          1 second    Target 2 Position - Read High Limit     
 12  TGT_2_POS:READ_LOLIM                                          19522    ai          1 second    Target 2 Position - Read Lower Limit    
 13  TGT_2_POS:READ_SET_POINT                                      19524    ai          1 second    Target 2 Position - Read Set Point      
 14  TGT_3_POS:READ_HILIM                                          19526    ai          1 second    Target 3 Position - Read High Limit     
 15  TGT_3_POS:READ_LOLIM                                          19528    ai          1 second    Target 3 Position - Read Lower Limit    
 16  TGT_3_POS:READ_SET_POINT                                      19530    ai          1 second    Target 3 Position - Read Set Point      
 17  FAULT:READ_TIME                                               19532    longin      1 second    Fault Timer Read                        
 18  OUT_POS:STA                                                   19534    longin      1 second    Out Position - Read Ststus              
 19  TGT_1_POS:STA                                                 19535    longin      1 second    Target 1 Position - Read Ststus         
 20  TGT_2_POS:STA                                                 19536    longin      1 second    Target 2 Position - Read Ststus         
 21  TGT_3_POS:STA                                                 19537    longin      1 second    Target 3 Position - Read Status         
 22  RADIATED_1:READ_TEMP                                          19538    longin      1 second    Radiated 1 - Read Temperature           
 23  RADIATED_2:READ_TEMP                                          19539    longin      1 second    Radiated 2 - Read Temperature           
 24  TGT_1_FLOW:READ_TEMP                                          19540    longin      1 second    Target 1 Flow - Read Temperature        
 25  TGT_1_RETURN:READ_TEMP                                        19541    longin      1 second    Target 1 Return - Read Temperature      
 26  TGT_2_FLOW:READ_TEMP                                          19542    longin      1 second    Target 2 Flow - Read Temperature        
 27  TGT_2_RETURN:READ_TEMP                                        19543    longin      1 second    Target 2 Return - Read Temperature      
 28  TGT_3_FLOW:READ_TEMP                                          19544    longin      1 second    Target 3 Flow - Read Temperature        
 29  TGT_3_RETURN:READ_TEMP                                        19545    longin      1 second    Target 3 Return - Read Temperature      
 30  COLLIMATOR_1:READ_TEMP                                        19546    longin      1 second    Collimator 1 - Read Temperature         
 31  COLLIMATOR_2:READ_TEMP                                        19547    longin      1 second    Collimator 2 - Read Temperature         
 32  COLLIMATOR_3:READ_TEMP                                        19548    longin      1 second    Collimator 3 - Read Temperature         
 33  COLLIMATOR_4:READ_TEMP                                        19549    longin      1 second    Collimator 4 - Read Temperature         
 34  COLLIMATOR_5:READ_TEMP                                        19550    longin      1 second    Collimator 5 - Read Temperature         
 35  ROTOMETER:READ_TEMP                                           19551    longin      1 second    Rotometer - Read Temperature            
 36  RADIATED_1:READ_UA                                            19552    longin      1 second    Radiated 1 - Temperature Upper Alarm Value
 37  RADIATED_1:READ_UW                                            19553    longin      1 second    Radiated 1 - Temperature Upper Warning Value
 38  RADIATED_2:READ_UA                                            19554    longin      1 second    Radiated 1 - Temperature Upper Alarm Value
 39  RADIATED_2:READ_UW                                            19555    longin      1 second    Radiated 1 - Temperature Upper Warning Value
 40  TGT_1_FLOW:READ_UA                                            19556    longin      1 second    Target 1 Flow - Temperature Upper Alarm Value
 41  TGT_1_FLOW:READ_UW                                            19557    longin      1 second    Target 1 Flow - Temperature Upper Warning Value
 42  TGT_1_RETURN:READ_UA                                          19558    longin      1 second    Target 1 Return - Temperature Upper Alarm Value
 43  TGT_1_RETURN:READ_UW                                          19559    longin      1 second    Target 1 Return - Temperature Upper Warning Value
 44  TGT_2_FLOW:READ_UA                                            19560    longin      1 second    Target 2 Flow - Temperature Upper Alarm Value
 45  TGT_2_FLOW:READ_UW                                            19561    longin      1 second    Target 2 Flow - Temperature Upper Warning Value
 46  TGT_2_RETURN:READ_UA                                          19562    longin      1 second    Target 2 Return - Temperature Upper Alarm Value
 47  TGT_2_RETURN:READ_UW                                          19563    longin      1 second    Target 2 Return - Temperature Upper Warning Value
 48  TGT_3_FLOW:READ_UA                                            19564    longin      1 second    Target 3 Flow - Temperature Upper Alarm Value
 49  TGT_3_FLOW:READ_UW                                            19565    longin      1 second    Target 3 Flow - Temperature Upper Warning Value
 50  TGT_3_RETURN:READ_UA                                          19566    longin      1 second    Target 3 Return - Temperature Upper Alarm Value
 51  TGT_3_RETURN:READ_UW                                          19567    longin      1 second    Target 3 Return - Temperature Upper Warning Value
 52  COLLIMATOR_1:READ_UA                                          19568    longin      1 second    Collimator 1- Temperature Upper Alarm Value
 53  COLLIMATOR_1:READ_UW                                          19569    longin      1 second    Collimator 1 - Temperature Upper Warning Value
 54  COLLIMATOR_2:READ_UA                                          19570    longin      1 second    Collimator 2 - Temperature Upper Alarm Value
 55  COLLIMATOR_2:READ_UW                                          19571    longin      1 second    Collimator 2 - Temperature Upper Warning Value
 56  COLLIMATOR_3:READ_UA                                          19572    longin      1 second    Collimator 3 - Temperature Upper Alarm Value
 57  COLLIMATOR_3:READ_UW                                          19573    longin      1 second    Collimator 3 - Temperature Upper Warning Value
 58  COLLIMATOR_4:READ_UA                                          19574    longin      1 second    Collimator 4 - Temperature Upper Alarm Value
 59  COLLIMATOR_4:READ_UW                                          19575    longin      1 second    Collimator 4 - Temperature Upper Warning Value
 60  COLLIMATOR_5:READ_UA                                          19576    longin      1 second    Collimator 5 - Temperature Upper Alarm Value
 61  COLLIMATOR_5:READ_UW                                          19577    longin      1 second    Collimator 5 - Temperature Upper Warning Value
 62  ROTOMETER:READ_UA                                             19578    longin      1 second    Rotometer - Temperature Upper Alarm Value
 63  ROTOMETER:READ_UW                                             19579    longin      1 second    Rotometer - Temperature Upper Warning Value
 64  IT_RAISE_CONT:STA                                             19580    bi          1 second    IT Raise Contactor ON                   
 65  IT_LOWER_CONT:STA                                             19581    bi          1 second    IT Lower Contactor ON                   
 66  IT_RAISE_LIM_SWITCH:STA                                       19582    bi          1 second    IT Raise Limit Switch                   
 67  IT_LOWER_LIM_SWITCH:STA                                       19583    bi          1 second    IT Lower Limit Switch                   
 68  PROF_RAISE_CONT:STA                                           19584    bi          1 second    Profile Raise Contactor ON              
 69  PROF_LOWER_CONT:STA                                           19585    bi          1 second    Profile Lower Contactor ON              
 70  PROF_RAISED:STA                                               19586    bi          1 second    Profile Raised                          
 71  PROF_LOWERED:STA                                              19587    bi          1 second    Profile Lowered                         
 72  MAIN_CIRC_PUMP:STA                                            19588    bi          1 second    Main Circulator Pump ON                 
 73  MAIN_CIRC_MCB:STA                                             19589    bi          1 second    Main Circulator MCB OK                  
 74  MAIN_CIRC_OLOAD:STA                                           19590    bi          1 second    Main Circulator Overload OK             
 75  MAIN_CIRC_ESTOP:STA                                           19591    bi          1 second    Main Circulator E-Stop OK               
 76  MAIN_CIRC_HMD_MON:STA                                         19592    bi          1 second    Main Circulator HMD Monitor OK          
 77  DRAIN_TANK_PUMP:STA                                           19593    bi          1 second    Drain Tank Pump Running                 
 78  DRAIN_TANK_MCB:STA                                            19594    bi          1 second    Drain Tank MCB OK                       
 79  DRAIN_TANK_OLOAD:STA                                          19595    bi          1 second    Drain Tank Overload OK                  
 80  DRAIN_TANK_ESTOP:STA                                          19596    bi          1 second    Drain Tank E-Stop OK                    
 81  DRAIN_TANK_HMD_MON:STA                                        19597    bi          1 second    Drain Tank HMD Monitor OK               
 82  COOLANT_FLOW_LOW:STA                                          19601    bi          1 second    Target Coolant Flow Low                 
 83  COOLANT_TEMP_HIGH:STA                                         19602    bi          1 second    Target Coolant Temperature High         
 84  DRAIN_TANK_WATER_HIGH:STA                                     19603    bi          1 second    Drain Tank Water High                   
 85  DRAIN_TANK_WATER_LOW:STA                                      19604    bi          1 second    Drain Tank Water Low                    
 86  IT_CIRCUIT_BREAKER:STA                                        19605    bi          1 second    IT Circuit Breaker OK                   
 87  IT_OLOAD:STA                                                  19606    bi          1 second    IT Overload OK                          
 88  IT_ESTOP:STA                                                  19607    bi          1 second    IT E.Stop OK                            
 89  PROF_CIRCUIT_BREAKER:STA                                      19608    bi          1 second    Profile Circuit Breaker OK              
 90  PROF_OLOAD:STA                                                19609    bi          1 second    Profile Overload OK                     
 91  PROF_ESTOP:STA                                                19610    bi          1 second    Profile E.Stop OK                       
 92  BT_COOLANT_PRESS_LOW:STA                                      19612    bi          1 second    BEAM TRIP 1 - Target Coolant Pressure Low
 93  BT_COOLANT_FLOW_LOW:STA                                       19613    bi          1 second    BEAM TRIP 2 - Target Coolant Flow Low   
 94  BT_COOLANT_TEMP_HIGH:STA                                      19614    bi          1 second    BEAM TRIP 3 - Target Coolant Temperature High
 95  BT_SPARE_1:STA                                                19615    bi          1 second    BEAM TRIP 4 - Spare 1                   
 96  BT_TGT_POS:STA                                                19616    bi          1 second    BEAM TRIP 5 - Target Out of Position    
 97  BT_PROF_POS:STA                                               19617    bi          1 second    BEAM TRIP 6 - Profile Out of Position   
 98  BT_TGT_ASSEMBLY_LOW_LIM_SWITCH:STA                            19618    bi          1 second    BEAM TRIP 7 - Target Assembly Lower Limit Switch Activated
 99  BT_CIRC_CIRCUIT_BREAK_TRIP:STA                                19619    bi          1 second    BEAM TRIP 8 - Circulator Circuit Breaker Trip
100  BT_CIRC_PUMP_OLOAD_TRIP:STA                                   19620    bi          1 second    BEAM TRIP 9 - Circulator Pump Overload Trip
101  BT_CIRC_PUMP_ESTOP:STA                                        19621    bi          1 second    BEAM TRIP 10 - Circulator Pump E-Stop Activated
102  BT_CIRC_CONTACT_ENERGISE:STA                                  19622    bi          1 second    BEAM TRIP 11 - Circulator Contact Not Energising
103  BT_CIRC_PUMP_HMD_MON:STA                                      19623    bi          1 second    BEAM TRIP 12 - Circulator Pump HMD Monitor Fault
104  BT_DEVICE_NET_FAIL:STA                                        19624    bi          1 second    BEAM TRIP 13 - Device Net Fail          
105  BT_PLC_FATAL:STA                                              19625    bi          1 second    BEAM TRIP 14 - PLC Fatal Error          
106  BT_N_TUN_EPB_COLLIMATOR:STA                                   19626    bi          1 second    BEAM TRIP 15 - EPB Collimator in North Tunnel
107  BT_SPARE_2:STA                                                19627    bi          1 second    BEAM TRIP 16 - Spare 2 - 1 bit          
108  BT_SPARE_3:STA                                                19628    bi          1 second    BEAM TRIP 17 - Spare 3 - 1 bit          
109  BT_TGT_1_FLOW_OTEMP:STA                                       19629    bi          1 second    BEAM TRIP 18 - Target 1 Flow Temperature Over Range
110  BT_TGT_1_RETURN_OTEMP:STA                                     19630    bi          1 second    BEAM TRIP 19 - Target 1 Return Temperature Over Range
111  BT_TGT_2_FLOW_OTEMP:STA                                       19631    bi          1 second    BEAM TRIP 20 - Target 2 Flow Temperature Over Range
112  BT_TGT_2_RETURN_OTEMP:STA                                     19632    bi          1 second    BEAM TRIP 21 - Target 2 Return Temperature Over Range
113  BT_TGT_3_FLOW_OTEMP:STA                                       19633    bi          1 second    BEAM TRIP 22 - Target 3 Flow Temperaure Over Range
114  BT_TGT_3_RETURN_OTEMP:STA                                     19634    bi          1 second    BEAM TRIP 23 - Target 3 Return Temperature Over Range
115  BT_RADIATED_1_OTEMP:STA                                       19635    bi          1 second    BEAM TRIP 24 - Radiated Beam 1 Temperature Over Range
116  BT_RADIATED_2_OTEMP:STA                                       19636    bi          1 second    BEAM TRIP 25 - Radiated Beam 2 Temperature Over Range
117  BT_COLLIMATOR_1_OTEMP:STA                                     19637    bi          1 second    BEAM TRIP 26 - Collimator 1 Temperature Over Range
118  BT_COLLIMATOR_2_OTEMP:STA                                     19638    bi          1 second    BEAM TRIP 27 - Collimator 2 Temperature Over Range
119  BT_COLLIMATOR_3_OTEMP:STA                                     19639    bi          1 second    BEAM TRIP 28 - Collimator 3 Temperature Over Range
120  BT_COLLIMATOR_4_OTEMP:STA                                     19640    bi          1 second    BEAM TRIP 29 - Collimator 4 Temperature Over Range
121  BT_COLLIMATOR_5_OTEMP:STA                                     19641    bi          1 second    BEAM TRIP 30 - Collimator 5 Temperature Over Range
122  BT_ROTOMETER_OTEMP:STA                                        19642    bi          1 second    BEAM TRIP 31 - Rotometer Temperature Over Range
123  BT_COLLIMATOR_6_OTEMP:STA                                     19644    bi          1 second    BEAM TRIP 31 - Collimator 6 Temperature Over Range
124  BT_COLLIMATOR_7_OTEMP:STA                                     19645    bi          1 second    BEAM TRIP 32 - Collimator 7 Temperature Over Range
125  BT_COLLIMATOR_8_OTEMP:STA                                     19646    bi          1 second    BEAM TRIP 33 - Collimator 8 Temperature Over Range
126  BT_COLLIMATOR_9_OTEMP:STA                                     19647    bi          1 second    BEAM TRIP 34 - Collimator 9 Temperature Over Range
127  COLLIMATOR_6:READ_UA                                          19676    longin      1 second    Collimator 6 - Temperature Upper Alarm Value
128  COLLIMATOR_6:READ_UW                                          19677    longin      1 second    Collimator 6 - Temperature Upper Warning Value
129  COLLIMATOR_7:READ_UA                                          19678    longin      1 second    Collimator 7 - Temperature Upper Alarm Value
130  COLLIMATOR_7:READ_UW                                          19679    longin      1 second    Collimator 7 - Temperature Upper Warning Value
131  COLLIMATOR_8:READ_UA                                          19680    longin      1 second    Collimator 8 - Temperature Upper Alarm Value
132  COLLIMATOR_8:READ_UW                                          19681    longin      1 second    Collimator 8 - Temperature Upper Warning Value
133  COLLIMATOR_9:READ_UA                                          19682    longin      1 second    Collimator 9 - Temperature Upper Alarm Value
134  COLLIMATOR_9:READ_UW                                          19683    longin      1 second    Collimator 9 - Temperature Upper Warning Value
135  COLLIMATOR_6:READ_TEMP                                        19684    longin      1 second    Collimator 6 - Read Temperature         
136  COLLIMATOR_7:READ_TEMP                                        19685    longin      1 second    Collimator 7 - Read Temperature         
137  COLLIMATOR_8:READ_TEMP                                        19686    longin      1 second    Collimator 8 - Read Temperature         
138  COLLIMATOR_9:READ_TEMP                                        19687    longin      1 second    Collimator 9 - Read Temperature         
139  MUON:VALVE_CLOSED                                             19688    bi          1 second    Muon Fast Closing / All Metal Valve Closed