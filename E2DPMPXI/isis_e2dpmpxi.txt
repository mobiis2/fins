 
  Database(P): E2DPMPXI
  IP Address: None
 
NUM  PV Name                                                       Address  Type        Scan        DESC                                    
  1  SYSTEM:INFO                                                            bi          1 second    EPB 2 PROFILE MONITORS - PXI            
  2  PRM300V:ON_OFF                                                         bo                      300V for PMS                            
  3  PRM300V:SET_VOLTS                                                      longout                 Set Volts                               
  4  PRM300V:READ_VOLTS                                                     longin      1 second    Read Volts                              
  5  READ:DATA                                                              longout                 Read data on next pulse                 
  6  TIME:STAMP                                                             longin      1 second    Counter changes each PXI new Data       
  7  E2PM1:DATA                                                             waveform    1 second    Read E2PM1 data                         
  8  E2PM2:DATA                                                             waveform    1 second    Read E2PM2 data                         
  9  E2PM3:DATA                                                             waveform    1 second    Read E2PM3 data                         
 10  E2PM4:DATA                                                             waveform    1 second    Read E2PM4 data                         
 11  E2PM5:DATA                                                             waveform    1 second    Read E2PM5 data                         
 12  E2PM6:DATA                                                             waveform    1 second    Read E2PM6 data                         
 13  E2PM7:DATA                                                             waveform    1 second    Read E2PM7 data                         
 14  E2PM8:DATA                                                             waveform    1 second    Read E2PM8 data                         
 15  E2PM9:DATA                                                             waveform    1 second    Read E2PM9 data                         
 16  E2PM10:DATA                                                            waveform    1 second    Read E2PM10 data                        
 17  E2PM11:DATA                                                            waveform    1 second    Read E2PM11 data                        
 18  E2PM12:DATA                                                            waveform    1 second    Read E2PM12 data                        
 19  E2PM13:DATA                                                            waveform    1 second    Read E2PM13 data                        
 20  E2PM14:DATA                                                            waveform    1 second    Read E2PM14 data                        
 21  E2PM15:DATA                                                            waveform    1 second    Read E2PM15 data                        
 22  E2PM16:DATA                                                            waveform    1 second    Read E2PM16 data                        
 23  E2PM17:DATA                                                            waveform    1 second    Read E2PM17 data                        
 24  E2PM18:DATA                                                            waveform    1 second    Read E2PM18 data                        
 25  E2PM19:DATA                                                            waveform    1 second    Read E2PM19 data                        
 26  E2PM20:DATA                                                            waveform    1 second    Read E2PM20 data                        
 27  E2PM21:DATA                                                            waveform    1 second    Read E2PM21 data                        
 28  E2PM22:DATA                                                            waveform    1 second    Read E2PM22 data                        
 29  E2PM23:DATA                                                            waveform    1 second    Read E2PM23 data                        
 30  E2PM24:DATA                                                            waveform    1 second    Read E2PM24 data                        
 31  E2PM25:DATA                                                            waveform    1 second    Read E2PM25 data                        
 32  E2PM26:DATA                                                            waveform    1 second    Read E2PM26 data                        
 33  E2PM27:DATA                                                            waveform    1 second    Read E2PM27 data                        
 34  E2PM28:DATA                                                            waveform    1 second    Read E2PM28 data                        
 35  E2PM29:DATA                                                            waveform    1 second    Read E2PM29 data                        
 36  E2PM30:DATA                                                            waveform    1 second    Read E2PM30 data                        
 37  E2PM31:DATA                                                            waveform    1 second    Read E2PM31 data                        
 38  E2PM32:DATA                                                            waveform    1 second    Read E2PM32 data                        
 39  E2PM33:DATA                                                            waveform    1 second    Read E2PM33 data                        
 40  E2PM34:DATA                                                            waveform    1 second    Read E2PM34 data                        
 41  E2PM35:DATA                                                            waveform    1 second    Read E2PM35 data                        
 42  E2PM2:H_WID                                                            ao                      E2PM2 Horizontal Width                  
 43  E2PM2:H_POS                                                            ao                      E2PM2 Horizontal Position               
 44  E2PM2:V_WID                                                            ao                      E2PM2 Vertical Width                    
 45  E2PM2:V_POS                                                            ao                      E2PM2 Vertical Position                 
 46  E2PM3:H_WID                                                            ao                      E2PM3 Horizontal Width                  
 47  E2PM3:H_POS                                                            ao                      E2PM3 Horizontal Position               
 48  E2PM3:V_WID                                                            ao                      E2PM3 Vertical Width                    
 49  E2PM3:V_POS                                                            ao                      E2PM3 Vertical Position                 
 50  E2PM4:H_WID                                                            ao                      E2PM4 Horizontal Width                  
 51  E2PM4:H_POS                                                            ao                      E2PM4 Horizontal Position               
 52  E2PM4:V_WID                                                            ao                      E2PM4 Vertical Width                    
 53  E2PM4:V_POS                                                            ao                      E2PM4 Vertical Position                 
 54  E2PM5:H_WID                                                            ao                      E2PM5 Horizontal Width                  
 55  E2PM5:H_POS                                                            ao                      E2PM5 Horizontal Position               
 56  E2PM5:V_WID                                                            ao                      E2PM5 Vertical Width                    
 57  E2PM5:V_POS                                                            ao                      E2PM5 Vertical Position                 
 58  E2PM6:H_WID                                                            ao                      E2PM6 Horizontal Width                  
 59  E2PM6:H_POS                                                            ao                      E2PM6 Horizontal Position               
 60  E2PM6:V_WID                                                            ao                      E2PM6 Vertical Width                    
 61  E2PM6:V_POS                                                            ao                      E2PM6 Vertical Position                 
 62  E2PM7:H_WID                                                            ao                      E2PM7 Horizontal Width                  
 63  E2PM7:H_POS                                                            ao                      E2PM7 Horizontal Position               
 64  E2PM7:V_WID                                                            ao                      E2PM7 Vertical Width                    
 65  E2PM7:V_POS                                                            ao                      E2PM7 Vertical Position                 
 66  E2PM8:H_WID                                                            ao                      E2PM8 Horizontal Width                  
 67  E2PM8:H_POS                                                            ao                      E2PM8 Horizontal Position               
 68  E2PM8:V_WID                                                            ao                      E2PM8 Vertical Width                    
 69  E2PM8:V_POS                                                            ao                      E2PM8 Vertical Position                 
 70  E2PM9:H_WID                                                            ao                      E2PM9 Horizontal Width                  
 71  E2PM9:H_POS                                                            ao                      E2PM9 Horizontal Position               
 72  E2PM9:V_WID                                                            ao                      E2PM9 Vertical Width                    
 73  E2PM9:V_POS                                                            ao                      E2PM9 Vertical Position                 
 74  E2PM10:H_WID                                                           ao                      E2PM10 Horizontal Width                 
 75  E2PM10:H_POS                                                           ao                      E2PM10 Horizontal Position              
 76  E2PM10:V_WID                                                           ao                      E2PM10 Vertical Width                   
 77  E2PM10:V_POS                                                           ao                      E2PM10 Vertical Position                
 78  E2PM11:H_WID                                                           ao                      E2PM11 Horizontal Width                 
 79  E2PM11:H_POS                                                           ao                      E2PM11 Horizontal Position              
 80  E2PM11:V_WID                                                           ao                      E2PM11 Vertical Width                   
 81  E2PM11:V_POS                                                           ao                      E2PM11 Vertical Position                
 82  E2PM12:H_WID                                                           ao                      E2PM12 Horizontal Width                 
 83  E2PM12:H_POS                                                           ao                      E2PM12 Horizontal Position              
 84  E2PM12:V_WID                                                           ao                      E2PM12 Vertical Width                   
 85  E2PM12:V_POS                                                           ao                      E2PM12 Vertical Position                
 86  E2PM13:H_WID                                                           ao                      E2PM13 Horizontal Width                 
 87  E2PM13:H_POS                                                           ao                      E2PM13 Horizontal Position              
 88  E2PM13:V_WID                                                           ao                      E2PM13 Vertical Width                   
 89  E2PM13:V_POS                                                           ao                      E2PM13 Vertical Position                
 90  E2PM14:H_WID                                                           ao                      E2PM14 Horizontal Width                 
 91  E2PM14:H_POS                                                           ao                      E2PM14 Horizontal Position              
 92  E2PM14:V_WID                                                           ao                      E2PM14 Vertical Width                   
 93  E2PM14:V_POS                                                           ao                      E2PM14 Vertical Position                
 94  E2PM15:H_WID                                                           ao                      E2PM15 Horizontal Width                 
 95  E2PM15:H_POS                                                           ao                      E2PM15 Horizontal Position              
 96  E2PM15:V_WID                                                           ao                      E2PM15 Vertical Width                   
 97  E2PM15:V_POS                                                           ao                      E2PM15 Vertical Position                
 98  E2PM16:H_WID                                                           ao                      E2PM16 Horizontal Width                 
 99  E2PM16:H_POS                                                           ao                      E2PM16 Horizontal Position              
100  E2PM16:V_WID                                                           ao                      E2PM16 Vertical Width                   
101  E2PM16:V_POS                                                           ao                      E2PM16 Vertical Position                
102  E2PM17:H_WID                                                           ao                      E2PM17 Horizontal Width                 
103  E2PM17:H_POS                                                           ao                      E2PM17 Horizontal Position              
104  E2PM17:V_WID                                                           ao                      E2PM17 Vertical Width                   
105  E2PM17:V_POS                                                           ao                      E2PM17 Vertical Position                
106  E2PM18:H_WID                                                           ao                      E2PM18 Horizontal Width                 
107  E2PM18:H_POS                                                           ao                      E2PM18 Horizontal Position              
108  E2PM18:V_WID                                                           ao                      E2PM18 Vertical Width                   
109  E2PM18:V_POS                                                           ao                      E2PM18 Vertical Position                
110  E2PM19:H_WID                                                           ao                      E2PM19 Horizontal Width                 
111  E2PM19:H_POS                                                           ao                      E2PM19 Horizontal Position              
112  E2PM19:V_WID                                                           ao                      E2PM19 Vertical Width                   
113  E2PM19:V_POS                                                           ao                      E2PM19 Vertical Position                
114  E2PM20:H_WID                                                           ao                      E2PM20 Horizontal Width                 
115  E2PM20:H_POS                                                           ao                      E2PM20 Horizontal Position              
116  E2PM20:V_WID                                                           ao                      E2PM20 Vertical Width                   
117  E2PM20:V_POS                                                           ao                      E2PM20 Vertical Position                
118  E2PM21:H_WID                                                           ao                      E2PM21 Horizontal Width                 
119  E2PM21:H_POS                                                           ao                      E2PM21 Horizontal Position              
120  E2PM21:V_WID                                                           ao                      E2PM21 Vertical Width                   
121  E2PM21:V_POS                                                           ao                      E2PM21 Vertical Position                
122  E2PM22:H_WID                                                           ao                      E2PM22 Horizontal Width                 
123  E2PM22:H_POS                                                           ao                      E2PM22 Horizontal Position              
124  E2PM22:V_WID                                                           ao                      E2PM22 Vertical Width                   
125  E2PM22:V_POS                                                           ao                      E2PM22 Vertical Position                
126  E2PM23:H_WID                                                           ao                      E2PM23 Horizontal Width                 
127  E2PM23:H_POS                                                           ao                      E2PM23 Horizontal Position              
128  E2PM23:V_WID                                                           ao                      E2PM23 Vertical Width                   
129  E2PM23:V_POS                                                           ao                      E2PM23 Vertical Position                
130  E2PM24:H_WID                                                           ao                      E2PM24 Horizontal Width                 
131  E2PM24:H_POS                                                           ao                      E2PM24 Horizontal Position              
132  E2PM24:V_WID                                                           ao                      E2PM24 Vertical Width                   
133  E2PM24:V_POS                                                           ao                      E2PM24 Vertical Position                
134  E2PM25:H_WID                                                           ao                      E2PM25 Horizontal Width                 
135  E2PM25:H_POS                                                           ao                      E2PM25 Horizontal Position              
136  E2PM25:V_WID                                                           ao                      E2PM25 Vertical Width                   
137  E2PM25:V_POS                                                           ao                      E2PM25 Vertical Position                
138  E2PM26:H_WID                                                           ao                      E2PM26 Horizontal Width                 
139  E2PM26:H_POS                                                           ao                      E2PM26 Horizontal Position              
140  E2PM26:V_WID                                                           ao                      E2PM26 Vertical Width                   
141  E2PM26:V_POS                                                           ao                      E2PM26 Vertical Position                
142  E2PM27:H_WID                                                           ao                      E2PM27 Horizontal Width                 
143  E2PM27:H_POS                                                           ao                      E2PM27 Horizontal Position              
144  E2PM27:V_WID                                                           ao                      E2PM27 Vertical Width                   
145  E2PM27:V_POS                                                           ao                      E2PM27 Vertical Position                
146  E2PM28:H_WID                                                           ao                      E2PM28 Horizontal Width                 
147  E2PM28:H_POS                                                           ao                      E2PM28 Horizontal Position              
148  E2PM28:V_WID                                                           ao                      E2PM28 Vertical Width                   
149  E2PM28:V_POS                                                           ao                      E2PM28 Vertical Position                
150  E2PM29:H_WID                                                           ao                      E2PM29 Horizontal Width                 
151  E2PM29:H_POS                                                           ao                      E2PM29 Horizontal Position              
152  E2PM29:V_WID                                                           ao                      E2PM29 Vertical Width                   
153  E2PM29:V_POS                                                           ao                      E2PM29 Vertical Position                
154  E2PM30:H_WID                                                           ao                      E2PM30 Horizontal Width                 
155  E2PM30:H_POS                                                           ao                      E2PM30 Horizontal Position              
156  E2PM30:V_WID                                                           ao                      E2PM30 Vertical Width                   
157  E2PM30:V_POS                                                           ao                      E2PM30 Vertical Position                
158  E2PM31:H_WID                                                           ao                      E2PM31 Horizontal Width                 
159  E2PM31:H_POS                                                           ao                      E2PM31 Horizontal Position              
160  E2PM31:V_WID                                                           ao                      E2PM31 Vertical Width                   
161  E2PM31:V_POS                                                           ao                      E2PM31 Vertical Position                
162  E2PM32:H_WID                                                           ao                      E2PM32 Horizontal Width                 
163  E2PM32:H_POS                                                           ao                      E2PM32 Horizontal Position              
164  E2PM32:V_WID                                                           ao                      E2PM32 Vertical Width                   
165  E2PM32:V_POS                                                           ao                      E2PM32 Vertical Position                
166  E2PM33:H_WID                                                           ao                      E2PM33 Horizontal Width                 
167  E2PM33:H_POS                                                           ao                      E2PM33 Horizontal Position              
168  E2PM33:V_WID                                                           ao                      E2PM33 Vertical Width                   
169  E2PM33:V_POS                                                           ao                      E2PM33 Vertical Position                
170  E2PM34:H_WID                                                           ao                      E2PM34 Horizontal Width                 
171  E2PM34:H_POS                                                           ao                      E2PM34 Horizontal Position              
172  E2PM34:V_WID                                                           ao                      E2PM34 Vertical Width                   
173  E2PM34:V_POS                                                           ao                      E2PM34 Vertical Position                
174  E2PM35:H_WID                                                           ao                      E2PM35 Horizontal Width                 
175  E2PM35:H_POS                                                           ao                      E2PM35 Horizontal Position              
176  E2PM35:V_WID                                                           ao                      E2PM35 Vertical Width                   
177  E2PM35:V_POS                                                           ao                      E2PM35 Vertical Position                
178  E2PM2:OFFSETS                                                          waveform    1 second    E2PM2 Offsets                           
179  E2PM3:OFFSETS                                                          waveform    1 second    E2PM3 Offsets                           
180  E2PM4:OFFSETS                                                          waveform    1 second    E2PM4 Offsets                           
181  E2PM5:OFFSETS                                                          waveform    1 second    E2PM5 Offsets                           
182  E2PM6:OFFSETS                                                          waveform    1 second    E2PM6 Offsets                           
183  E2PM7:OFFSETS                                                          waveform    1 second    E2PM7 Offsets                           
184  E2PM8:OFFSETS                                                          waveform    1 second    E2PM8 Offsets                           
185  E2PM9:OFFSETS                                                          waveform    1 second    E2PM9 Offsets                           
186  E2PM10:OFFSETS                                                         waveform    1 second    E2PM10 Offsets                          
187  E2PM11:OFFSETS                                                         waveform    1 second    E2PM11 Offsets                          
188  E2PM12:OFFSETS                                                         waveform    1 second    E2PM12 Offsets                          
189  E2PM13:OFFSETS                                                         waveform    1 second    E2PM13 Offsets                          
190  E2PM14:OFFSETS                                                         waveform    1 second    E2PM14 Offsets                          
191  E2PM15:OFFSETS                                                         waveform    1 second    E2PM15 Offsets                          
192  E2PM16:OFFSETS                                                         waveform    1 second    E2PM16 Offsets                          
193  E2PM17:OFFSETS                                                         waveform    1 second    E2PM17 Offsets                          
194  E2PM18:OFFSETS                                                         waveform    1 second    E2PM18 Offsets                          
195  E2PM19:OFFSETS                                                         waveform    1 second    E2PM19 Offsets                          
196  E2PM20:OFFSETS                                                         waveform    1 second    E2PM20 Offsets                          
197  E2PM21:OFFSETS                                                         waveform    1 second    E2PM21 Offsets                          
198  E2PM22:OFFSETS                                                         waveform    1 second    E2PM22 Offsets                          
199  E2PM23:OFFSETS                                                         waveform    1 second    E2PM23 Offsets                          
200  E2PM24:OFFSETS                                                         waveform    1 second    E2PM24 Offsets                          
201  E2PM25:OFFSETS                                                         waveform    1 second    E2PM25 Offsets                          
202  E2PM26:OFFSETS                                                         waveform    1 second    E2PM26 Offsets                          
203  E2PM27:OFFSETS                                                         waveform    1 second    E2PM27 Offsets                          
204  E2PM28:OFFSETS                                                         waveform    1 second    E2PM28 Offsets                          
205  E2PM29:OFFSETS                                                         waveform    1 second    E2PM29 Offsets                          
206  E2PM30:OFFSETS                                                         waveform    1 second    E2PM30 Offsets                          
207  E2PM31:OFFSETS                                                         waveform    1 second    E2PM31 Offsets                          
208  E2PM32:OFFSETS                                                         waveform    1 second    E2PM32 Offsets                          
209  E2PM33:OFFSETS                                                         waveform    1 second    E2PM33 Offsets                          
210  E2PM34:OFFSETS                                                         waveform    1 second    E2PM34 Offsets                          
211  E2PM35:OFFSETS                                                         waveform    1 second    E2PM35 Offsets                          
212  E2PM35:UPDATE_STATUS                                                   bi          1 second    Tell-tale channel for reader activity   
213  E2PM35:HORIZ_1                                                         ao                      E2PM35 H1                               
214  E2PM35:HORIZ_2                                                         ao                      E2PM35 H2                               
215  E2PM35:HORIZ_3                                                         ao                      E2PM35 H3                               
216  E2PM35:HORIZ_4                                                         ao                      E2PM35 H4                               
217  E2PM35:HORIZ_5                                                         ao                      E2PM35 H5                               
218  E2PM35:HORIZ_6                                                         ao                      E2PM35 H6                               
219  E2PM35:HORIZ_7                                                         ao                      E2PM35 H7                               
220  E2PM35:HORIZ_8                                                         ao                      E2PM35 H8                               
221  E2PM35:HORIZ_9                                                         ao                      E2PM35 H9                               
222  E2PM35:HORIZ_10                                                        ao                      E2PM35 H10                              
223  E2PM35:HORIZ_11                                                        ao                      E2PM35 H11                              
224  E2PM35:HORIZ_12                                                        ao                      E2PM35 H12                              
225  E2PM35:VERT_1                                                          ao                      E2PM35 V1                               
226  E2PM35:VERT_2                                                          ao                      E2PM35 V2                               
227  E2PM35:VERT_3                                                          ao                      E2PM35 V3                               
228  E2PM35:VERT_4                                                          ao                      E2PM35 V4                               
229  E2PM35:VERT_5                                                          ao                      E2PM35 V5                               
230  E2PM35:VERT_6                                                          ao                      E2PM35 V6                               
231  E2PM35:VERT_7                                                          ao                      E2PM35 V7                               
232  E2PM35:VERT_8                                                          ao                      E2PM35 V8                               
233  E2PM35:VERT_9                                                          ao                      E2PM35 V9                               
234  E2PM35:VERT_10                                                         ao                      E2PM35 V10                              
235  E2PM35:VERT_11                                                         ao                      E2PM35 V11                              
236  E2PM35:VERT_12                                                         ao                      E2PM35 V12                              
237  E2PM35:RUN_UPDATE                                                      bi          1 second    Flag 0= stop update programme running   