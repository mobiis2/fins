 
  Database(P): SRBD
  IP Address: None
 
NUM  PV Name                                                       Address  Type        Scan        DESC                                    
  1  THERMO_1:READ_TEMP                                            3        ai          1 second    SRBD Thermocouple 1 - read temperature  
  2  THERMO_2:READ_TEMP                                            3        ai          1 second    SRBD Thermocouple 2 - read temperature  
  3  THERMO_3:READ_TEMP                                            3        ai          1 second    SRBD Thermocouple 3 - read temperature  
  4  THERMO_4:READ_TEMP                                            3        ai          1 second    SRBD Thermocouple 4 - read temperature  
  5  THERMO_5:READ_TEMP                                            3        ai          1 second    SRBD Thermocouple 5 - read temperature  
  6  THERMO_6:READ_TEMP                                            3        ai          1 second    SRBD Thermocouple 6 - read temperature  