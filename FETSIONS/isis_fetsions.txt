 
  Database(P): FETSIONS
  IP Address: None
 
NUM  PV Name                                                       Address  Type        Scan        DESC                                    
  1  SYSTEM:INFO                                                            bo                      FETS platform                           
  2  TYPEG_1:INIT                                                           longout                 Type G init                             
  3  TYPEG_2:INIT                                                           longout                 Type G init                             
  4  TYPEG_3:INIT                                                           longout                 Type G init                             
  5  TYPEG_4:INIT                                                           longout                 Type G init                             
  6  TYPES_1:INIT                                                           longout                 Initialise Type-S card                  
  7  TYPES_1:INIT:HPARAM1                                                   longout                                                         
  8  TYPES_2:INIT                                                           longout                 Initialise Type-S card                  
  9  TYPES_2:INIT:HPARAM1                                                   longout                                                         
 10  TYPES_1:STATUS                                                         longin      1 second    Read S status                           
 11  STATUS_1:MULTIPLY                                                      dfanout                 Redirection channel list - DS1 Status 1 
 12  STATUS_1:MULTIPLY_1                                                    dfanout                                                         
 13  STATUS_1:MULTIPLY_1_SUB1                                               dfanout                                                         
 14  STATUS_1:MULTIPLY_1_SUB2                                               dfanout                                                         
 15  STATUS_1:MULTIPLY_2                                                    dfanout                                                         
 16  TYPES_2:STATUS                                                         longin      1 second    Read S status                           
 17  STATUS_2:MULTIPLY                                                      dfanout                 Redirection channel list - DS1 Status 2 
 18  STATUS_2:MULTIPLY_SUB1                                                 dfanout                                                         
 19  STATUS_2:MULTIPLY_SUB2                                                 dfanout                                                         
 20  MAG:READ_ON_C                                                          sub                     I/S MAGNET ON (READ) sub                
 21  MAG:READ_ON                                                            bi                      I/S MAGNET ON (READ)                    
 22  MAG:REMOTE_C                                                           sub                     I/S MAGNET REMOTE sub                   
 23  MAG:REMOTE                                                             bi                      I/S MAGNET REMOTE                       
 24  MAG:INTERLOCKS_C                                                       sub                     I/S MAGNET INTERLOCKS sub               
 25  MAG:INTERLOCKS                                                         bi                      I/S MAGNET INTERLOCKS                   
 26  HTR1:READ_ON_C                                                         sub                     I/S HEATERS 1 ON (READ) sub             
 27  HTR1:READ_ON                                                           bi                      I/S HEATERS 1 ON (READ)                 
 28  HTR1:REMOTE_C                                                          sub                     I/S HEATERS 1 REMOTE sub                
 29  HTR1:REMOTE                                                            bi                      I/S HEATERS 1 REMOTE                    
 30  HTR1:INTERLOCKS_C                                                      sub                     I/S HEATERS 1 INTERLOCKS sub            
 31  HTR1:INTERLOCKS                                                        bi                      I/S HEATERS 1 INTERLOCKS                
 32  HTR2:READ_ON_C                                                         sub                     I/S HEATERS 2 ON (READ) sub             
 33  HTR2:READ_ON                                                           bi                      I/S HEATERS 2 ON (READ)                 
 34  HTR2:REMOTE_C                                                          sub                     I/S HEATERS 2 REMOTE sub                
 35  HTR2:REMOTE                                                            bi                      I/S HEATERS 2 REMOTE                    
 36  HTR2:INTERLOCKS_C                                                      sub                     I/S HEATERS 2 INTERLOCKS sub            
 37  HTR2:INTERLOCKS                                                        bi                      I/S HEATERS 2 INTERLOCKS                
 38  GAS:READ_ON_C                                                          sub                     GAS valve ON (read) sub                 
 39  GAS:READ_ON                                                            bi                      GAS valve ON (read)                     
 40  GAS:REMOTE_C                                                           sub                     GAS valve remote status sub             
 41  GAS:REMOTE                                                             bi                      GAS valve remote status                 
 42  VAC:GATE_CLOSED_C                                                      sub                     Vac. gate valve closed status sub       
 43  VAC:GATE_CLOSED                                                        bi                      Vac. gate valve closed status           
 44  GAS:INTERLOCKS_C                                                       sub                     GAS valve interlocks sub                
 45  GAS:INTERLOCKS                                                         bi                      GAS valve interlocks                    
 46  ARC:SUM_INTLK_NC_C                                                     sub                     SUM. INTERLOCKS - NORMALLY CLOSED sub   
 47  ARC:SUM_INTLK_NC                                                       bi                      SUM. INTERLOCKS - NORMALLY CLOSED       
 48  EXT:READ_ON_C                                                          sub                     I/S EXTRACT ON (READ) sub               
 49  EXT:READ_ON                                                            bi                      I/S EXTRACT ON (READ)                   
 50  EXT:REMOTE_C                                                           sub                     I/S EXTRACT REMOTE sub                  
 51  EXT:REMOTE                                                             bi                      I/S EXTRACT REMOTE                      
 52  EXT:MAIN_INTLK_C                                                       sub                     I/S EXTRACT MAIN INTERLOCK sub          
 53  EXT:MAIN_INTLK                                                         bi                      I/S EXTRACT MAIN INTERLOCK              
 54  EXT:PERS_INTLK_C                                                       sub                     I/S EXTRACT PERSONNEL INTERLOCK sub     
 55  EXT:PERS_INTLK                                                         bi                      I/S EXTRACT PERSONNEL INTERLOCK         
 56  EXT:FAST_INTLK_C                                                       sub                     I/S EXTRACT FAST INTERLOCK sub          
 57  EXT:FAST_INTLK                                                         bi                      I/S EXTRACT FAST INTERLOCK              
 58  ARC:ON_STATUS_C                                                        sub                     On/Off status for ARC PSU sub           
 59  ARC:ON_STATUS                                                          bi                      On/Off status for ARC PSU               
 60  ARC:LOCREM_STATUS_C                                                    sub                     Local/Remote status for ARC PSU sub     
 61  ARC:LOCREM_STATUS                                                      bi                      Local/Remote status for ARC PSU         
 62  TMP:READ_BOILER                                                        ai          1 second    BOILER TEMPERATURE READ                 
 63  TMP:SET_BOILER                                                         ao                      BOILER TEMPERATURE SET                  
 64  TMP:READ_TRANSPORT                                                     ai          1 second    TRANSPORT CONTROL TEMPERATURE READ      
 65  TMP:SET_TRANSPORT                                                      ao                      TRANSPORT CONTROL TEMPERATURE SET       
 66  TMP:READ_ANODE                                                         ai          1 second    ANODE TEMPERATURE READ                  
 67  TMP:READ_CATHODE                                                       ai          1 second    CATHODE TEMPERATURE READ                
 68  TMP:READ_BODY                                                          ai          1 second    SOURCE BODY TEMPERATURE READ            
 69  TMP:READ_TRANSPORT_MNTR                                                ai          1 second    TRANSPORT MNTR TEMPERATURE READ         
 70  GAS:READ_PRESS                                                         ai          1 second    GAS PRESSURE SET                        
 71  GAS:SET_PRESS                                                          ao                      GAS PRESSURE SET                        
 72  GAS:SET_ON                                                             bo                      GAS CONTROL ON SET                      
 73  GAS:SET_ON:HPARAM1                                                     longout                                                         
 74  MAG:READ_CURRENT                                                       ai          1 second    I/S MAGNET read current                 
 75  MAG:SET_CURRENT                                                        ao                      I/S MAGNET set current                  
 76  MAG:READ_VOLTS                                                         ai          1 second    MAGNET VOLTAGE READ                     
 77  EXT:READ_VOLTS                                                         ai          1 second    EXTRACT VOLTAGE READ                    
 78  EXT:SET_VOLTS                                                          ao                      EXTRACT VOLTAGE SET                     
 79  EXT:SET_ON                                                             bo                      EXTRACT PSU ON SET                      
 80  EXT:SET_ON:HPARAM1                                                     longout                                                         
 81  EXT:RESET                                                              bo                      EXTRACT PSU INTERLOCK RESET             
 82  EXT:RESET:HPARAM1                                                      longout                                                         
 83  ARC:DC_READ_VOLTS                                                      ai          1 second    I/S ARC DC read volts                   
 84  ARC:DC_READ_CURRENT                                                    ai          1 second    I/S ARC DC read current                 
 85  ARC:DC_SET_CURRENT                                                     ao                      I/S ARC DC set current                  
 86  ARC:ON_OFF_RESET                                                       bo                      ARC PSU ON                              
 87  ARC:ON_OFF_RESET:HPARAM1                                               longout                                                         
 88  ARC:AC_READ_CURRENT                                                    ai          1 second    ARC AC CURRENT READ                     
 89  ARC:AC_SET_CURRENT                                                     ao                      ARC AC CURRENT SET                      
 90  IRIS:TEST_VOLTS                                                        ao                      PSU POWER SUPPLY VOLTAGE SET            
 91  IRIS:STEP_VOLTS                                                        ao                      STEP VOLTS                              
 92  IRIS:ON_OFF                                                            bo                      looping on/off                          