 
  Database(P): PXIRDBLM
  IP Address: None
 
NUM  PV Name                                                       Address  Type        Scan        DESC                                    
  1  SYSTEM:INFO                                                            bi          1 second    Ring BLMs PXI                           
  2  SET:AVE_PULSES                                                         longout                 Set number of pulses to average over    
  3  PULSES:DAILY_TOTAL                                                     ai          1 second    Total Beam pulses today                 
  4  PULSES:YEST_TOTAL                                                      ai          1 second    Total Beam pulses yesterday             
  5  MICEDIPS:YEST_TOTAL                                                    longin      1 second    Total no. MICE dips yesterday           
  6  MICEDIPS:DAILY_TOTAL                                                   longin      1 second    Total no. MICE dips today               
  7  R0BLM1:DAILY_TOTAL                                                     ai          1 second    Total Beam loss today for R0BLM1        
  8  R0BLM3:DAILY_TOTAL                                                     ai          1 second    Total Beam loss today for R0BLM3        
  9  R0BLM4:DAILY_TOTAL                                                     ai          1 second    Total Beam loss today for R0BLM4        
 10  R1BLM1:DAILY_TOTAL                                                     ai          1 second    Total Beam loss today for R1BLM1        
 11  R1BLM2:DAILY_TOTAL                                                     ai          1 second    Total Beam loss today for R1BLM2        
 12  R1BLM3:DAILY_TOTAL                                                     ai          1 second    Total Beam loss today for R1BLM3        
 13  R1BLM4:DAILY_TOTAL                                                     ai          1 second    Total Beam loss today for R1BLM4        
 14  R2BLM1:DAILY_TOTAL                                                     ai          1 second    Total Beam loss today for R2BLM1        
 15  R2BLM2:DAILY_TOTAL                                                     ai          1 second    Total Beam loss today for R2BLM2        
 16  R2BLM3:DAILY_TOTAL                                                     ai          1 second    Total Beam loss today for R2BLM3        
 17  R2BLM4:DAILY_TOTAL                                                     ai          1 second    Total Beam loss today for R2BLM4        
 18  R3BLM1:DAILY_TOTAL                                                     ai          1 second    Total Beam loss today for R3BLM1        
 19  R3BLM2:DAILY_TOTAL                                                     ai          1 second    Total Beam loss today for R3BLM2        
 20  R3BLM3:DAILY_TOTAL                                                     ai          1 second    Total Beam loss today for R3BLM3        
 21  R3BLM4:DAILY_TOTAL                                                     ai          1 second    Total Beam loss today for R3BLM4        
 22  R4BLM1:DAILY_TOTAL                                                     ai          1 second    Total Beam loss today for R4BLM1        
 23  R4BLM2:DAILY_TOTAL                                                     ai          1 second    Total Beam loss today for R4BLM2        
 24  R4BLM3:DAILY_TOTAL                                                     ai          1 second    Total Beam loss today for R4BLM3        
 25  R4BLM4:DAILY_TOTAL                                                     ai          1 second    Total Beam loss today for R4BLM4        
 26  R5BLM1:DAILY_TOTAL                                                     ai          1 second    Total Beam loss today for R5BLM1        
 27  R5BLM2:DAILY_TOTAL                                                     ai          1 second    Total Beam loss today for R5BLM2        
 28  R5BLM3:DAILY_TOTAL                                                     ai          1 second    Total Beam loss today for R5BLM3        
 29  R5BLM4:DAILY_TOTAL                                                     ai          1 second    Total Beam loss today for R5BLM4        
 30  R6BLM1:DAILY_TOTAL                                                     ai          1 second    Total Beam loss today for R6BLM1        
 31  R6BLM2:DAILY_TOTAL                                                     ai          1 second    Total Beam loss today for R6BLM2        
 32  R6BLM3:DAILY_TOTAL                                                     ai          1 second    Total Beam loss today for R6BLM3        
 33  R6BLM4:DAILY_TOTAL                                                     ai          1 second    Total Beam loss today for R6BLM4        
 34  R7BLM1:DAILY_TOTAL                                                     ai          1 second    Total Beam loss today for R7BLM1        
 35  R7BLM2:DAILY_TOTAL                                                     ai          1 second    Total Beam loss today for R7BLM2        
 36  R7BLM3:DAILY_TOTAL                                                     ai          1 second    Total Beam loss today for R7BLM3        
 37  R7BLM3:MICE_TOTAL_TODAY                                                ai          1 second    Total Beam loss today for R7BLM3 on MICE pulses
 38  R7BLM4:DAILY_TOTAL                                                     ai          1 second    Total Beam loss today for R7BLM4        
 39  R7BLM4:MICE_TOTAL_TODAY                                                ai          1 second    Total Beam loss today for R7BLM4 on MICE pulses
 40  R8BLM1:DAILY_TOTAL                                                     ai          1 second    Total Beam loss today for R8BLM1        
 41  R8BLM1:MICE_TOTAL_TODAY                                                ai          1 second    Total Beam loss today for R8BLM1 on MICE pulses
 42  R8BLM2:DAILY_TOTAL                                                     ai          1 second    Total Beam loss today for R8BLM2        
 43  R8BLM3:DAILY_TOTAL                                                     ai          1 second    Total Beam loss today for R8BLM3        
 44  R8BLM4:DAILY_TOTAL                                                     ai          1 second    Total Beam loss today for R8BLM4        
 45  R9BLM1:DAILY_TOTAL                                                     ai          1 second    Total Beam loss today for R9BLM1        
 46  R9BLM2:DAILY_TOTAL                                                     ai          1 second    Total Beam loss today for R9BLM2        
 47  R9BLM3:DAILY_TOTAL                                                     ai          1 second    Total Beam loss today for R9BLM3        
 48  R9BLM4:DAILY_TOTAL                                                     ai          1 second    Total Beam loss today for R9BLM4        
 49  R0BLM1:YEST_TOTAL                                                      ai          1 second    Total Beam loss yesterday for R0BLM1    
 50  R0BLM3:YEST_TOTAL                                                      ai          1 second    Total Beam loss yesterday for R0BLM3    
 51  R0BLM4:YEST_TOTAL                                                      ai          1 second    Total Beam loss yesterday for R0BLM4    
 52  R1BLM1:YEST_TOTAL                                                      ai          1 second    Total Beam loss yesterday for R1BLM1    
 53  R1BLM2:YEST_TOTAL                                                      ai          1 second    Total Beam loss yesterday for R1BLM2    
 54  R1BLM3:YEST_TOTAL                                                      ai          1 second    Total Beam loss yesterday for R1BLM3    
 55  R1BLM4:YEST_TOTAL                                                      ai          1 second    Total Beam loss yesterday for R1BLM4    
 56  R2BLM1:YEST_TOTAL                                                      ai          1 second    Total Beam loss yesterday for R2BLM1    
 57  R2BLM2:YEST_TOTAL                                                      ai          1 second    Total Beam loss yesterday for R2BLM2    
 58  R2BLM3:YEST_TOTAL                                                      ai          1 second    Total Beam loss yesterday for R2BLM3    
 59  R2BLM4:YEST_TOTAL                                                      ai          1 second    Total Beam loss yesterday for R2BLM4    
 60  R3BLM1:YEST_TOTAL                                                      ai          1 second    Total Beam loss yesterday for R3BLM1    
 61  R3BLM2:YEST_TOTAL                                                      ai          1 second    Total Beam loss yesterday for R3BLM2    
 62  R3BLM3:YEST_TOTAL                                                      ai          1 second    Total Beam loss yesterday for R3BLM3    
 63  R3BLM4:YEST_TOTAL                                                      ai          1 second    Total Beam loss yesterday for R3BLM4    
 64  R4BLM1:YEST_TOTAL                                                      ai          1 second    Total Beam loss yesterday for R4BLM1    
 65  R4BLM2:YEST_TOTAL                                                      ai          1 second    Total Beam loss yesterday for R4BLM2    
 66  R4BLM3:YEST_TOTAL                                                      ai          1 second    Total Beam loss yesterday for R4BLM3    
 67  R4BLM4:YEST_TOTAL                                                      ai          1 second    Total Beam loss yesterday for R4BLM4    
 68  R5BLM1:YEST_TOTAL                                                      ai          1 second    Total Beam loss yesterday for R5BLM1    
 69  R5BLM2:YEST_TOTAL                                                      ai          1 second    Total Beam loss yesterday for R5BLM2    
 70  R5BLM3:YEST_TOTAL                                                      ai          1 second    Total Beam loss yesterday for R5BLM3    
 71  R5BLM4:YEST_TOTAL                                                      ai          1 second    Total Beam loss yesterday for R5BLM4    
 72  R6BLM1:YEST_TOTAL                                                      ai          1 second    Total Beam loss yesterday for R6BLM1    
 73  R6BLM2:YEST_TOTAL                                                      ai          1 second    Total Beam loss yesterday for R6BLM2    
 74  R6BLM3:YEST_TOTAL                                                      ai          1 second    Total Beam loss yesterday for R6BLM3    
 75  R6BLM4:YEST_TOTAL                                                      ai          1 second    Total Beam loss yesterday for R6BLM4    
 76  R7BLM1:YEST_TOTAL                                                      ai          1 second    Total Beam loss yesterday for R7BLM1    
 77  R7BLM2:YEST_TOTAL                                                      ai          1 second    Total Beam loss yesterday for R7BLM2    
 78  R7BLM3:YEST_TOTAL                                                      ai          1 second    Total Beam loss yesterday for R7BLM3    
 79  R7BLM3:MICE_TOTAL_YEST                                                 ai          1 second    Total Beam loss yesterday for R7BLM3 on MICE pulses
 80  R7BLM4:YEST_TOTAL                                                      ai          1 second    Total Beam loss yesterday for R7BLM4    
 81  R7BLM4:MICE_TOTAL_YEST                                                 ai          1 second    Total Beam loss yesterday for R7BLM4 on MICE pulses
 82  R8BLM1:YEST_TOTAL                                                      ai          1 second    Total Beam loss yesterday for R8BLM1    
 83  R8BLM1:MICE_TOTAL_YEST                                                 ai          1 second    Total Beam loss yesterday for R8BLM1 on MICE pulses
 84  R8BLM2:YEST_TOTAL                                                      ai          1 second    Total Beam loss yesterday for R8BLM2    
 85  R8BLM3:YEST_TOTAL                                                      ai          1 second    Total Beam loss yesterday for R8BLM3    
 86  R8BLM4:YEST_TOTAL                                                      ai          1 second    Total Beam loss yesterday for R8BLM4    
 87  R9BLM1:YEST_TOTAL                                                      ai          1 second    Total Beam loss yesterday for R9BLM1    
 88  R9BLM2:YEST_TOTAL                                                      ai          1 second    Total Beam loss yesterday for R9BLM2    
 89  R9BLM3:YEST_TOTAL                                                      ai          1 second    Total Beam loss yesterday for R9BLM3    
 90  R9BLM4:YEST_TOTAL                                                      ai          1 second    Total Beam loss yesterday for R9BLM4    
 91  R0BLM1:LOSS                                                            ai          1 second    Last reading & tols for R0BLM1          
 92  R0BLM3:LOSS                                                            ai          1 second    Last reading & tols for R0BLM3          
 93  R0BLM4:LOSS                                                            ai          1 second    Last reading & tols for R0BLM4          
 94  R1BLM1:LOSS                                                            ai          1 second    Last reading & tols for R1BLM1          
 95  R1BLM2:LOSS                                                            ai          1 second    Last reading & tols for R1BLM2          
 96  R1BLM3:LOSS                                                            ai          1 second    Last reading & tols for R1BLM3          
 97  R1BLM4:LOSS                                                            ai          1 second    Last reading & tols for R1BLM4          
 98  R2BLM1:LOSS                                                            ai          1 second    Last reading & tols for R2BLM1          
 99  R2BLM2:LOSS                                                            ai          1 second    Last reading & tols for R2BLM2          
100  R2BLM3:LOSS                                                            ai          1 second    Last reading & tols for R2BLM3          
101  R2BLM4:LOSS                                                            ai          1 second    Last reading & tols for R2BLM4          
102  R3BLM1:LOSS                                                            ai          1 second    Last reading & tols for R3BLM1          
103  R3BLM2:LOSS                                                            ai          1 second    Last reading & tols for R3BLM2          
104  R3BLM3:LOSS                                                            ai          1 second    Last reading & tols for R3BLM3          
105  R3BLM4:LOSS                                                            ai          1 second    Last reading & tols for R3BLM4          
106  R4BLM1:LOSS                                                            ai          1 second    Last reading & tols for R4BLM1          
107  R4BLM2:LOSS                                                            ai          1 second    Last reading & tols for R4BLM2          
108  R4BLM3:LOSS                                                            ai          1 second    Last reading & tols for R4BLM3          
109  R4BLM4:LOSS                                                            ai          1 second    Last reading & tols for R4BLM4          
110  R5BLM1:LOSS                                                            ai          1 second    Last reading & tols for R5BLM1          
111  R5BLM2:LOSS                                                            ai          1 second    Last reading & tols for R5BLM2          
112  R5BLM3:LOSS                                                            ai          1 second    Last reading & tols for R5BLM3          
113  R5BLM4:LOSS                                                            ai          1 second    Last reading & tols for R5BLM4          
114  R6BLM1:LOSS                                                            ai          1 second    Last reading & tols for R6BLM1          
115  R6BLM2:LOSS                                                            ai          1 second    Last reading & tols for R6BLM2          
116  R6BLM3:LOSS                                                            ai          1 second    Last reading & tols for R6BLM3          
117  R6BLM4:LOSS                                                            ai          1 second    Last reading & tols for R6BLM4          
118  R7BLM1:LOSS                                                            ai          1 second    Last reading & tols for R7BLM1          
119  R7BLM2:LOSS                                                            ai          1 second    Last reading & tols for R7BLM2          
120  R7BLM3:LOSS                                                            ai          1 second    Last reading & tols for R7BLM3          
121  R7BLM3:MICE                                                            ai          1 second    R7BLM3 on MICE pulse                    
122  R7BLM4:LOSS                                                            ai          1 second    Last reading & tols for R7BLM4          
123  R7BLM4:MICE                                                            ai          1 second    R7BLM4 on MICE pulse                    
124  R8BLM1:LOSS                                                            ai          1 second    Last reading & tols for R8BLM1          
125  R8BLM1:MICE                                                            ai          1 second    R8BLM1 on MICE pulse                    
126  R8BLM2:LOSS                                                            ai          1 second    Last reading & tols for R8BLM2          
127  R8BLM3:LOSS                                                            ai          1 second    Last reading & tols for R8BLM3          
128  R8BLM4:LOSS                                                            ai          1 second    Last reading & tols for R8BLM4          
129  R9BLM1:LOSS                                                            ai          1 second    Last reading & tols for R9BLM1          
130  R9BLM2:LOSS                                                            ai          1 second    Last reading & tols for R9BLM2          
131  R9BLM3:LOSS                                                            ai          1 second    Last reading & tols for R9BLM3          
132  R9BLM4:LOSS                                                            ai          1 second    Last reading & tols for R9BLM4          
133  R0BLM1:AVE_LOSS                                                        ai          1 second    Last reading & tols for R0BLM1          
134  R0BLM3:AVE_LOSS                                                        ai          1 second    Last reading & tols for R0BLM3          
135  R0BLM4:AVE_LOSS                                                        ai          1 second    Last reading & tols for R0BLM4          
136  R1BLM1:AVE_LOSS                                                        ai          1 second    Last reading & tols for R1BLM1          
137  R1BLM2:AVE_LOSS                                                        ai          1 second    Last reading & tols for R1BLM2          
138  R1BLM3:AVE_LOSS                                                        ai          1 second    Last reading & tols for R1BLM3          
139  R1BLM4:AVE_LOSS                                                        ai          1 second    Last reading & tols for R1BLM4          
140  R2BLM1:AVE_LOSS                                                        ai          1 second    Last reading & tols for R2BLM1          
141  R2BLM2:AVE_LOSS                                                        ai          1 second    Last reading & tols for R2BLM2          
142  R2BLM3:AVE_LOSS                                                        ai          1 second    Last reading & tols for R2BLM3          
143  R2BLM4:AVE_LOSS                                                        ai          1 second    Last reading & tols for R2BLM4          
144  R3BLM1:AVE_LOSS                                                        ai          1 second    Last reading & tols for R3BLM1          
145  R3BLM2:AVE_LOSS                                                        ai          1 second    Last reading & tols for R3BLM2          
146  R3BLM3:AVE_LOSS                                                        ai          1 second    Last reading & tols for R3BLM3          
147  R3BLM4:AVE_LOSS                                                        ai          1 second    Last reading & tols for R3BLM4          
148  R4BLM1:AVE_LOSS                                                        ai          1 second    Last reading & tols for R4BLM1          
149  R4BLM2:AVE_LOSS                                                        ai          1 second    Last reading & tols for R4BLM2          
150  R4BLM3:AVE_LOSS                                                        ai          1 second    Last reading & tols for R4BLM3          
151  R4BLM4:AVE_LOSS                                                        ai          1 second    Last reading & tols for R4BLM4          
152  R5BLM1:AVE_LOSS                                                        ai          1 second    Last reading & tols for R5BLM1          
153  R5BLM2:AVE_LOSS                                                        ai          1 second    Last reading & tols for R5BLM2          
154  R5BLM3:AVE_LOSS                                                        ai          1 second    Last reading & tols for R5BLM3          
155  R5BLM4:AVE_LOSS                                                        ai          1 second    Last reading & tols for R5BLM4          
156  R6BLM1:AVE_LOSS                                                        ai          1 second    Last reading & tols for R6BLM1          
157  R6BLM2:AVE_LOSS                                                        ai          1 second    Last reading & tols for R6BLM2          
158  R6BLM3:AVE_LOSS                                                        ai          1 second    Last reading & tols for R6BLM3          
159  R6BLM4:AVE_LOSS                                                        ai          1 second    Last reading & tols for R6BLM4          
160  R7BLM1:AVE_LOSS                                                        ai          1 second    Last reading & tols for R7BLM1          
161  R7BLM2:AVE_LOSS                                                        ai          1 second    Last reading & tols for R7BLM2          
162  R7BLM3:AVE_LOSS                                                        ai          1 second    Last reading & tols for R7BLM3          
163  R7BLM4:AVE_LOSS                                                        ai          1 second    Last reading & tols for R7BLM4          
164  R8BLM1:AVE_LOSS                                                        ai          1 second    Last reading & tols for R8BLM1          
165  R8BLM2:AVE_LOSS                                                        ai          1 second    Last reading & tols for R8BLM2          
166  R8BLM3:AVE_LOSS                                                        ai          1 second    Last reading & tols for R8BLM3          
167  R8BLM4:AVE_LOSS                                                        ai          1 second    Last reading & tols for R8BLM4          
168  R9BLM1:AVE_LOSS                                                        ai          1 second    Last reading & tols for R9BLM1          
169  R9BLM2:AVE_LOSS                                                        ai          1 second    Last reading & tols for R9BLM2          
170  R9BLM3:AVE_LOSS                                                        ai          1 second    Last reading & tols for R9BLM3          
171  R9BLM4:AVE_LOSS                                                        ai          1 second    Last reading & tols for R9BLM4          
172  R0BLM1:ALARM_STATUS                                                    bo                      R0BLM1 alarm status                     
173  R0BLM3:ALARM_STATUS                                                    bo                      R0BLM3 alarm status                     
174  R0BLM4:ALARM_STATUS                                                    bo                      R0BLM4 alarm status                     
175  R1BLM1:ALARM_STATUS                                                    bo                      R1BLM1 alarm status                     
176  R1BLM2:ALARM_STATUS                                                    bo                      R1BLM2 alarm status                     
177  R1BLM3:ALARM_STATUS                                                    bo                      R1BLM3 alarm status                     
178  R1BLM4:ALARM_STATUS                                                    bo                      R1BLM4 alarm status                     
179  R2BLM1:ALARM_STATUS                                                    bo                      R2BLM1 alarm status                     
180  R2BLM2:ALARM_STATUS                                                    bo                      R2BLM2 alarm status                     
181  R2BLM3:ALARM_STATUS                                                    bo                      R2BLM3 alarm status                     
182  R2BLM4:ALARM_STATUS                                                    bo                      R2BLM4 alarm status                     
183  R3BLM1:ALARM_STATUS                                                    bo                      R3BLM1 alarm status                     
184  R3BLM2:ALARM_STATUS                                                    bo                      R3BLM2 alarm status                     
185  R3BLM3:ALARM_STATUS                                                    bo                      R3BLM3 alarm status                     
186  R3BLM4:ALARM_STATUS                                                    bo                      R3BLM4 alarm status                     
187  R4BLM1:ALARM_STATUS                                                    bo                      R4BLM1 alarm status                     
188  R4BLM2:ALARM_STATUS                                                    bo                      R4BLM2 alarm status                     
189  R4BLM3:ALARM_STATUS                                                    bo                      R4BLM3 alarm status                     
190  R4BLM4:ALARM_STATUS                                                    bo                      R4BLM4 alarm status                     
191  R5BLM1:ALARM_STATUS                                                    bo                      R5BLM1 alarm status                     
192  R5BLM2:ALARM_STATUS                                                    bo                      R5BLM2 alarm status                     
193  R5BLM3:ALARM_STATUS                                                    bo                      R5BLM3 alarm status                     
194  R5BLM4:ALARM_STATUS                                                    bo                      R5BLM4 alarm status                     
195  R6BLM1:ALARM_STATUS                                                    bo                      R6BLM1 alarm status                     
196  R6BLM2:ALARM_STATUS                                                    bo                      R6BLM2 alarm status                     
197  R6BLM3:ALARM_STATUS                                                    bo                      R6BLM3 alarm status                     
198  R6BLM4:ALARM_STATUS                                                    bo                      R6BLM4 alarm status                     
199  R7BLM1:ALARM_STATUS                                                    bo                      R7BLM1 alarm status                     
200  R7BLM2:ALARM_STATUS                                                    bo                      R7BLM2 alarm status                     
201  R7BLM3:ALARM_STATUS                                                    bo                      R7BLM3 alarm status                     
202  R7BLM4:ALARM_STATUS                                                    bo                      R7BLM4 alarm status                     
203  R8BLM1:ALARM_STATUS                                                    bo                      R8BLM1 alarm status                     
204  R8BLM2:ALARM_STATUS                                                    bo                      R8BLM2 alarm status                     
205  R8BLM3:ALARM_STATUS                                                    bo                      R8BLM3 alarm status                     
206  R8BLM4:ALARM_STATUS                                                    bo                      R8BLM4 alarm status                     
207  R9BLM1:ALARM_STATUS                                                    bo                      R9BLM1 alarm status                     
208  R9BLM2:ALARM_STATUS                                                    bo                      R9BLM2 alarm status                     
209  R9BLM3:ALARM_STATUS                                                    bo                      R9BLM3 alarm status                     
210  R9BLM4:ALARM_STATUS                                                    bo                      R9BLM4 alarm status                     
211  ALARM:STATUS                                                           bo                      any alarm status                        
212  R1BLM4:KICKER_ALARM_STATUS                                             bo                      R1BLM4 Kicker Limit status              
213  ALARM:TIME3                                                            stringout               String for time and date of trip for R1BLM4 Kicker
214  ALARM:TIME                                                             stringin    1 second    String for time and date of trip        
215  CONSEQ:PULSE_TRIP                                                      longout                 Set number of pulses to average over    
216  R0BLM1:TOL                                                             ao                      tolerance for R0BLM1                    
217  R0BLM3:TOL                                                             ao                      tolerance for R0BLM3                    
218  R0BLM4:TOL                                                             ao                      tolerance for R0BLM4                    
219  R1BLM1:TOL                                                             ao                      tolerance for R0BLM1                    
220  R1BLM2:TOL                                                             ao                      tolerance for R0BLM2                    
221  R1BLM3:TOL                                                             ao                      tolerance for R0BLM3                    
222  R1BLM4:TOL                                                             ao                      tolerance for R0BLM4                    
223  R2BLM1:TOL                                                             ao                      tolerance for R0BLM1                    
224  R2BLM2:TOL                                                             ao                      tolerance for R0BLM2                    
225  R2BLM3:TOL                                                             ao                      tolerance for R0BLM3                    
226  R2BLM4:TOL                                                             ao                      tolerance for R0BLM4                    
227  R3BLM1:TOL                                                             ao                      tolerance for R0BLM1                    
228  R3BLM2:TOL                                                             ao                      tolerance for R0BLM2                    
229  R3BLM3:TOL                                                             ao                      tolerance for R0BLM3                    
230  R3BLM4:TOL                                                             ao                      tolerance for R0BLM4                    
231  R4BLM1:TOL                                                             ao                      tolerance for R0BLM1                    
232  R4BLM2:TOL                                                             ao                      tolerance for R0BLM2                    
233  R4BLM3:TOL                                                             ao                      tolerance for R0BLM3                    
234  R4BLM4:TOL                                                             ao                      tolerance for R0BLM4                    
235  R5BLM1:TOL                                                             ao                      tolerance for R0BLM1                    
236  R5BLM2:TOL                                                             ao                      tolerance for R0BLM2                    
237  R5BLM3:TOL                                                             ao                      tolerance for R0BLM3                    
238  R5BLM4:TOL                                                             ao                      tolerance for R0BLM4                    
239  R6BLM1:TOL                                                             ao                      tolerance for R0BLM1                    
240  R6BLM2:TOL                                                             ao                      tolerance for R0BLM2                    
241  R6BLM3:TOL                                                             ao                      tolerance for R0BLM3                    
242  R6BLM4:TOL                                                             ao                      tolerance for R0BLM4                    
243  R7BLM1:TOL                                                             ao                      tolerance for R0BLM1                    
244  R7BLM2:TOL                                                             ao                      tolerance for R0BLM2                    
245  R7BLM3:TOL                                                             ao                      tolerance for R0BLM3                    
246  R7BLM4:TOL                                                             ao                      tolerance for R0BLM4                    
247  R8BLM1:TOL                                                             ao                      tolerance for R0BLM1                    
248  R8BLM2:TOL                                                             ao                      tolerance for R0BLM2                    
249  R8BLM3:TOL                                                             ao                      tolerance for R0BLM3                    
250  R8BLM4:TOL                                                             ao                      tolerance for R0BLM4                    
251  R9BLM1:TOL                                                             ao                      tolerance for R0BLM1                    
252  R9BLM2:TOL                                                             ao                      tolerance for R0BLM2                    
253  R9BLM3:TOL                                                             ao                      tolerance for R0BLM3                    
254  R9BLM4:TOL                                                             ao                      tolerance for R0BLM4                    
255  RELOAD:TOLS                                                            longout                 Reload tolerences                       
256  STRING:MESSAGE                                                         stringout               String channel for Beam Stop Legend     
257  FLAG:READ                                                              longin      1 second    Flag for Beam stop button pressed       