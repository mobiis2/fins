 
  Database(P): R5-1WATER
  IP Address: 130.246.88.96
 
NUM  PV Name                                                       Address  Type        Scan        DESC                                    
  1  ALARM:RESET                                                   19049    bo                      Reset R5.1 chiller alarm                
  2  ALARM:RESET_R                                                 19049    bi          .1 second   Reset R5.1 chiller alarm readback       
  3  ALARM:RESET_C                                                          calcout                 Reset R5.1 chiller alarm clear          
  4  PLC:CLOCK                                                     0        longin      1 second    PLC Clock                               
  5  REGISTER:0001                                                 1        longin      1 second    plant alarm bits                        
  6  REGISTER:0002                                                 2        longin      1 second    plant alarm bits                        
  7  REGISTER:0003                                                 3        longin      1 second    plant alarm bits                        
  8  PT010:READ_PRESS                                              4        ai          1 second    PT010 Primary supply pressure           
  9  PT030:READ_PRESS                                              5        ai          1 second    PT030 Secondary supply pressure         
 10  PT020:READ_PRESS                                              6        ai          1 second    PT020 Seconday Filter output pressure   
 11  TT010:READ_TEMP                                               7        ai          1 second    TT010 Primary Return Temperature        
 12  TT020:READ_TEMP                                               8        ai          1 second    TT020 Primary Supply Temperature        
 13  TT040:READ_TEMP                                               9        ai          1 second    TT040 Secondary Supply Temperature      
 14  TT030:READ_TEMP                                               10       ai          1 second    TT030 Secondary Return Temperature      
 15  LT010:READ_TEMP                                               11       ai          1 second    LT010 Header Tank Level                 
 16  FT010:READ_FLOW                                               12       ai          1 second    FT010 Primary Flow - Chiller 1          
 17  FT020:READ_FLOW                                               13       ai          1 second    FT020 Primary Flow - Chiller 2          
 18  FT030:READ_FLOW                                               14       ai          1 second    FT030 Primary Flow - Chiller 3          
 19  FT040:READ_FLOW                                               15       ai          1 second    FT040 Secondary Return Flow             
 20  CHILLER1:INLET_READ_TEMP                                      16       ai          1 second    Chiller 1 Inlet Temp                    
 21  CHILLER1:OUTLET_READ_TEMP                                     17       ai          1 second    Chiller 1 Outlet Temp                   
 22  CHILLER1:READ_CURRENT                                         18       ai          1 second    Chiller 2 Drawn Current                 
 23  CHILLER2:INLET_READ_TEMP                                      19       ai          1 second    Chiller 2 Inlet Temp                    
 24  CHILLER2:OUTLET_READ_TEMP                                     20       ai          1 second    Chiller 2 Outlet Temp                   
 25  CHILLER2:READ_CURRENT                                         21       ai          1 second    Chiller 2 Drawn Current                 
 26  CHILLER3:INLET_READ_TEMP                                      22       ai          1 second    Chiller 3 Inlet Temp                    
 27  CHILLER3:OUTLET_READ_TEMP                                     23       ai          1 second    Chiller 3 Outlet Temp                   
 28  CHILLER3:READ_CURRENT                                         24       ai          1 second    Chiller 3 Drawn Current                 
 29  PR_PUMP1:READ_CURRENT                                         25       ai          1 second    Primary Pump 1 Drawn Current (from Invertor)
 30  PR_PUMP2:READ_CURRENT                                         26       ai          1 second    Primary Pump 2 Drawn Current (from Invertor)
 31  SEC_PUMP1:READ_CURRENT                                        27       ai          1 second    Secondary Pump 1 Drawn Current (from Invertor)
 32  SEC_PUMP2:READ_CURRENT                                        28       ai          1 second    Secondary Pump 2 Drawn Current (from Invertor)
 33  PR_PUMP1:READ_FREQ                                            29       ai          1 second    Primary Pump 1 Frequency (from Invertor)
 34  PR_PUMP2:READ_FREQ                                            30       ai          1 second    Primary Pump 2 Frequency (from Invertor)
 35  SEC_PUMP1:READ_FREQ                                           31       ai          1 second    Secondary Pump 1 Frequency (from Invertor)
 36  SEC_PUMP2:READ_FREQ                                           32       ai          1 second    Secondary Pump 2 Frequency (from Invertor)
 37  CHILLER1:READ_ACT_POW                                         33       longin      1 second    Chiller 1 Active Power                  
 38  CHILLER2:READ_ACT_POW                                         34       longin      1 second    Chiller 2 Active Power                  
 39  CHILLER3:READ_ACT_POW                                         35       longin      1 second    Chiller 3 Active Power                  
 40  CHILLER1:DEMAND                                               36       longin      1 second    Chiller 1 Demand                        
 41  CHILLER2:DEMAND                                               37       longin      1 second    Chiller 2 Demand                        
 42  CHILLER3:DEMAND                                               38       longin      1 second    Chiller 3 Demand                        
 43  0001:MULTIPLE                                                          dfanout                 Redirection channel reg 0001            
 44  0001:MULTIPLE_SUB1                                                     dfanout                                                         
 45  0001:MULTIPLE_SUB2                                                     dfanout                                                         
 46  0002:MULTIPLE                                                          dfanout                 Redirection channel reg 0002            
 47  0002:MULTIPLE_SUB1                                                     dfanout                                                         
 48  0002:MULTIPLE_SUB2                                                     dfanout                                                         
 49  0003:MULTIPLE                                                          dfanout                 Redirection channel reg 0003            
 50  HEADTANK:HI_C                                                          sub                                                             
 51  HEADTANK:HI                                                            bi                                                              
 52  HEADTANK:LO_C                                                          sub                                                             
 53  HEADTANK:LO                                                            bi                                                              
 54  HEADTANK:LOLO_C                                                        sub                                                             
 55  HEADTANK:LOLO                                                          bi                                                              
 56  HEADTANK:LLS_C                                                         sub                                                             
 57  HEADTANK:LLS                                                           bi                                                              
 58  PRIMARY:SUPPLY_PRESS_LO_C                                              sub                                                             
 59  PRIMARY:SUPPLY_PRESS_LO                                                bi                                                              
 60  SECONDARY:SUPPLY_PRESS_HI_C                                            sub                                                             
 61  SECONDARY:SUPPLY_PRESS_HI                                              bi                                                              
 62  SECONDARY:SUPPLY_PRESS_LO_C                                            sub                                                             
 63  SECONDARY:SUPPLY_PRESS_LO                                              bi                                                              
 64  SECONDARY:FILT_DIFF_PRESS_HI_C                                         sub                                                             
 65  SECONDARY:FILT_DIFF_PRESS_HI                                           bi                                                              
 66  PRIMARY:RETURN_TEMP_HI_C                                               sub                                                             
 67  PRIMARY:RETURN_TEMP_HI                                                 bi                                                              
 68  PRIMARY:RETURN_TEMP_HI_TRIP_C                                          sub                                                             
 69  PRIMARY:RETURN_TEMP_HI_TRIP                                            bi                                                              
 70  PRIMARY:SUPPLY_TEMP_HI_C                                               sub                                                             
 71  PRIMARY:SUPPLY_TEMP_HI                                                 bi                                                              
 72  SECONDARY:SUPPLY_TEMP_HI_C                                             sub                                                             
 73  SECONDARY:SUPPLY_TEMP_HI                                               bi                                                              
 74  SECONDARY:SUPPLY_TEMP_LO_C                                             sub                                                             
 75  SECONDARY:SUPPLY_TEMP_LO                                               bi                                                              
 76  SECONDARY:RETURN_FLOW_LO_C                                             sub                                                             
 77  SECONDARY:RETURN_FLOW_LO                                               bi                                                              
 78  PRIMARY:EMERGENCY_STOP_C                                               sub                                                             
 79  PRIMARY:EMERGENCY_STOP                                                 bi                                                              
 80  SECONDARY:EMERGENCY_STOP_C                                             sub                                                             
 81  SECONDARY:EMERGENCY_STOP                                               bi                                                              
 82  CHILLER_1:FAULT_C                                                      sub                                                             
 83  CHILLER_1:FAULT                                                        bi                                                              
 84  CHILLER_2:FAULT_C                                                      sub                                                             
 85  CHILLER_2:FAULT                                                        bi                                                              
 86  CHILLER_3:FAULT_C                                                      sub                                                             
 87  CHILLER_3:FAULT                                                        bi                                                              
 88  PRIMARY:PUMP1_FAULT_C                                                  sub                                                             
 89  PRIMARY:PUMP1_FAULT                                                    bi                                                              
 90  PRIMARY:PUMP2_FAULT_C                                                  sub                                                             
 91  PRIMARY:PUMP2_FAULT                                                    bi                                                              
 92  SECONDARY:PUMP1_FAULT_C                                                sub                                                             
 93  SECONDARY:PUMP1_FAULT                                                  bi                                                              
 94  SECONDARY:PUMP2_FAULT_C                                                sub                                                             
 95  SECONDARY:PUMP2_FAULT                                                  bi                                                              
 96  0001:MULTIPLE_CALC                                                     calcout     1 second    Redirect_chan script                    
 97  0002:MULTIPLE_CALC                                                     calcout     1 second    Redirect_chan script                    
 98  0003:MULTIPLE_CALC                                                     calcout     1 second    Redirect_chan script                    