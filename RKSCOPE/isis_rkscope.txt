 
  Database(P): RKSCOPE
  IP Address: None
 
NUM  PV Name                                                       Address  Type        Scan        DESC                                    
  1  SYSTEM:LOAD:HPARAM1                                                    longout                                                         
  2  SYSTEM:STATUS                                                          longout                 STATUS                                  
  3  SYSTEM:STATUS:HPARAM1                                                  longout                                                         
  4  SYSTEM:ERROR                                                           longout                 System error channel                    
  5  SPIB:PCC                                                               longout                 PCC channel                             
  6  TIMER_1:CH1_DELP                                                       longout                 STEbus TIMER 1 Channel 1 set Delta P    
  7  TIMER_1:CH2_DELP                                                       longout                 STEbus TIMER 1 Channel 2 set Delta P    
  8  TIMER_1:CH1_MS_K                                                       longout                 STEbus TIMER 1 Channel 1 set MS/K       
  9  TIMER_1:CH2_MS_K                                                       longout                 STEbus TIMER 1 Channel 2 set MS/K       
 10  TIMER_1:CH1_ENB                                                        bo                      STEbus TIMER 1 Channel 1 enable         
 11  TIMER_1:CH2_ENB                                                        bo                      STEbus TIMER 1 Channel 2 enable         
 12  TIMER_1:CH1_SST                                                        bo                      STEbus TIMER 1 Channel 1 single-shot enable
 13  TIMER_1:CH2_SST                                                        bo                      STEbus TIMER 1 Channel 2 single-shot enable
 14  TIMER_1:CH1_MMS                                                        longout                 STEbus TIMER 1 Channel 2 set MMS        
 15  TIMER_1:CH2_MMS                                                        longout                 STEbus TIMER 1 Channel 2 set MMS        
 16  TIMER_1:CH1_STA                                                        longout                 STEbus TIMER 1 Channel 1 status         
 17  TIMER_1:CH2_STA                                                        longout                 STEbus TIMER 1 Channel 2 status         
 18  TIMER_2:CH1_DELP                                                       longout                 STEbus TIMER 2 Channel 1 set Delta P    
 19  TIMER_2:CH2_DELP                                                       longout                 STEbus TIMER 2 Channel 2 set Delta P    
 20  TIMER_2:CH1_MS_K                                                       longout                 STEbus TIMER 2 Channel 1 set MS/K       
 21  TIMER_2:CH2_MS_K                                                       longout                 STEbus TIMER 2 Channel 2 set MS/K       