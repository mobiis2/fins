 
  Database(P): RMMPLC
  IP Address: None
 
NUM  PV Name                                                       Address  Type        Scan        DESC                                    
  1  REGISTER:38                                                   3        ai          1 second    Register @ 40038                        
  2  REGISTER:39                                                   3        ai          1 second    Register @ 40039                        
  3  REGISTER:64                                                   3        ai          1 second    Register @ 40064                        
  4  REGISTER:252                                                  3        longin      1 second    Register @ 40252                        
  5  REGISTER:253                                                  3        longin      1 second    Register @ 40253                        
  6  REGISTER:254                                                  3        longin      1 second    Register @ 40254                        
  7  REGISTER:255                                                  3        longin      1 second    Register @ 40255                        
  8  REGISTER:260                                                  3        longin      1 second    Register @ 40260 - tell-tale            
  9  REGISTER:270                                                  3        longin      1 second    Register @ 40270 - warnings             
 10  REGISTER:271                                                  3        longin      1 second    Register @ 40271 - DC alarms            
 11  REGISTER:272                                                  3        longin      1 second    Register @ 40272 - AC UPS alarms        
 12  CAPRM_AIR:TEMP                                                         ao                      Capacitor room air temperature (40038)  
 13  PLTRM_AIR:TEMP                                                         ao                      Plant room air temperature (40039)      
 14  CHOKE_OIL:TEMP                                                         ao                      Choke oil temperature (40064)           
 15  PLC:TELL_TALE_C                                                        sub                                                             
 16  PLC:TELL_TALE                                                          bi                                                              
 17  252:MULTIPLE                                                           dfanout                 redirection channel reg 252             
 18  252_A:MULTIPLE                                                         dfanout                 Redirection channel reg 252 MSB         
 19  252_B:MULTIPLE                                                         dfanout                 Redirection channel reg 252 LSB         
 20  253:MULTIPLE                                                           dfanout                 redirection channel reg 253             
 21  253_A:MULTIPLE                                                         dfanout                 Redirection channel reg 253 MSB         
 22  253_B:MULTIPLE                                                         dfanout                 Redirection channel reg 253 LSB         
 23  254:MULTIPLE                                                           dfanout                 redirection channel reg 254             
 24  254_A:MULTIPLE                                                         dfanout                 Redirection channel reg 254 MSB         
 25  254_B:MULTIPLE                                                         dfanout                 Redirection channel reg 254 LSB         
 26  255:MULTIPLE                                                           dfanout                 redirection channel reg 255             
 27  255_A:MULTIPLE                                                         dfanout                 Redirection channel reg 255 MSB         
 28  270:MULTIPLE                                                           dfanout                 redirection channel reg 270             
 29  270_A:MULTIPLE                                                         dfanout                 Redirection channel reg 270 MSB         
 30  270_B:MULTIPLE                                                         dfanout                 Redirection channel reg 270 LSB         
 31  DCTRAN_1:TEMP_ALARM_C                                                  sub                                                             
 32  DCTRAN_1:TEMP_ALARM                                                    bi                                                              
 33  DCTRAN_2:TEMP_ALARM_C                                                  sub                                                             
 34  DCTRAN_2:TEMP_ALARM                                                    bi                                                              
 35  DCTRAN_3:TEMP_ALARM_C                                                  sub                                                             
 36  DCTRAN_3:TEMP_ALARM                                                    bi                                                              
 37  DCTRAN_AIR:TEMP_ALARM_C                                                sub                                                             
 38  DCTRAN_AIR:TEMP_ALARM                                                  bi                                                              
 39  DCTRAN_1:TEMP_TRIP_C                                                   sub                                                             
 40  DCTRAN_1:TEMP_TRIP                                                     bi                                                              
 41  DCTRAN_2:TEMP_TRIP_C                                                   sub                                                             
 42  DCTRAN_2:TEMP_TRIP                                                     bi                                                              
 43  DCTRAN_3:TEMP_TRIP_C                                                   sub                                                             
 44  DCTRAN_3:TEMP_TRIP                                                     bi                                                              
 45  DCTRAN_AIR:TEMP_TRIP_C                                                 sub                                                             
 46  DCTRAN_AIR:TEMP_TRIP                                                   bi                                                              
 47  MOTOR_WALL:TEMP_ALARM_C                                                sub                                                             
 48  MOTOR_WALL:TEMP_ALARM                                                  bi                                                              
 49  MOTOR_COUPL:TEMP_ALARM_C                                               sub                                                             
 50  MOTOR_COUPL:TEMP_ALARM                                                 bi                                                              
 51  MOTOR_WALL:TEMP_TRIP_C                                                 sub                                                             
 52  MOTOR_WALL:TEMP_TRIP                                                   bi                                                              
 53  MOTOR_COUPL:TEMP_TRIP_C                                                sub                                                             
 54  MOTOR_COUPL:TEMP_TRIP                                                  bi                                                              
 55  MOTOR_VIBRA:HIGH_ALARM_C                                               sub                                                             
 56  MOTOR_VIBRA:HIGH_ALARM                                                 bi                                                              
 57  MOTOR_VIBRA:LOW_ALARM_C                                                sub                                                             
 58  MOTOR_VIBRA:LOW_ALARM                                                  bi                                                              
 59  MOTOR_VIBRA:HIGH_TRIP_C                                                sub                                                             
 60  MOTOR_VIBRA:HIGH_TRIP                                                  bi                                                              
 61  MOTOR_PHASE:CONTROL_FAULT_C                                            sub                                                             
 62  MOTOR_PHASE:CONTROL_FAULT                                              bi                                                              
 63  ALTER_COUPL:TEMP_ALARM_C                                               sub                                                             
 64  ALTER_COUPL:TEMP_ALARM                                                 bi                                                              
 65  ALTER_CONVE:TEMP_ALARM_C                                               sub                                                             
 66  ALTER_CONVE:TEMP_ALARM                                                 bi                                                              
 67  ALTER_STATA:TEMP_ALARM_C                                               sub                                                             
 68  ALTER_STATA:TEMP_ALARM                                                 bi                                                              
 69  ALTER_STATB:TEMP_ALARM_C                                               sub                                                             
 70  ALTER_STATB:TEMP_ALARM                                                 bi                                                              
 71  ALTER_COUPL:TEMP_TRIP_C                                                sub                                                             
 72  ALTER_COUPL:TEMP_TRIP                                                  bi                                                              
 73  ALTER_CONVE:TEMP_TRIP_C                                                sub                                                             
 74  ALTER_CONVE:TEMP_TRIP                                                  bi                                                              
 75  ALTER_STATA:TEMP_TRIP_C                                                sub                                                             
 76  ALTER_STATA:TEMP_TRIP                                                  bi                                                              
 77  ALTER_STATB:TEMP_TRIP_C                                                sub                                                             
 78  ALTER_STATB:TEMP_TRIP                                                  bi                                                              
 79  ALTER_VIBRA:HIGH_ALARM_C                                               sub                                                             
 80  ALTER_VIBRA:HIGH_ALARM                                                 bi                                                              
 81  ALTER_VIBRA:LOW_ALARM_C                                                sub                                                             
 82  ALTER_VIBRA:LOW_ALARM                                                  bi                                                              
 83  ALTER_VIBRA:HIGH_TRIP_C                                                sub                                                             
 84  ALTER_VIBRA:HIGH_TRIP                                                  bi                                                              
 85  ALTER_EARTH:FAULT_C                                                    sub                                                             
 86  ALTER_EARTH:FAULT                                                      bi                                                              
 87  ALTER_CURNT:OVER_CUR_C                                                 sub                                                             
 88  ALTER_CURNT:OVER_CUR                                                   bi                                                              
 89  CHOKE_OIL1:TEMP_ALARM_C                                                sub                                                             
 90  CHOKE_OIL1:TEMP_ALARM                                                  bi                                                              
 91  CHOKE_OIL2:TEMP_ALARM_C                                                sub                                                             
 92  CHOKE_OIL2:TEMP_ALARM                                                  bi                                                              
 93  CHOKE_OIL1:TEMP_TRIP_C                                                 sub                                                             
 94  CHOKE_OIL1:TEMP_TRIP                                                   bi                                                              
 95  CHOKE_OIL2:TEMP_TRIP_C                                                 sub                                                             
 96  CHOKE_OIL2:TEMP_TRIP                                                   bi                                                              
 97  CHOKE_OIL:FLOW_TRIP_C                                                  sub                                                             
 98  CHOKE_OIL:FLOW_TRIP                                                    bi                                                              
 99  CHOKE_GAS:ALARM_1_C                                                    sub                                                             
100  CHOKE_GAS:ALARM_1                                                      bi                                                              
101  CHOKE_GAS:ALARM_2_C                                                    sub                                                             
102  CHOKE_GAS:ALARM_2                                                      bi                                                              
103  CHOKE_FAN1:RUNNING_C                                                   sub                                                             
104  CHOKE_FAN1:RUNNING                                                     bi                                                              
105  CHOKE_FAN2:RUNNING_C                                                   sub                                                             
106  CHOKE_FAN2:RUNNING                                                     bi                                                              
107  CHOKE_FAN3:RUNNING_C                                                   sub                                                             
108  CHOKE_FAN3:RUNNING                                                     bi                                                              
109  CHOKE_FAN1:FAILURE_C                                                   sub                                                             
110  CHOKE_FAN1:FAILURE                                                     bi                                                              
111  CHOKE_FAN2:FAILURE_C                                                   sub                                                             
112  CHOKE_FAN2:FAILURE                                                     bi                                                              
113  CHOKE_FAN3:FAILURE_C                                                   sub                                                             
114  CHOKE_FAN3:FAILURE                                                     bi                                                              
115  CHOKE_EARTH:FAULT_C                                                    sub                                                             
116  CHOKE_EARTH:FAULT                                                      bi                                                              
117  SYNCH_CABL1:TEMP_ALARM_C                                               sub                                                             
118  SYNCH_CABL1:TEMP_ALARM                                                 bi                                                              
119  SYNCH_CABL2:TEMP_ALARM_C                                               sub                                                             
120  SYNCH_CABL2:TEMP_ALARM                                                 bi                                                              
121  SYPLG_CABLE:TEMP_ALARM_C                                               sub                                                             
122  SYPLG_CABLE:TEMP_ALARM                                                 bi                                                              
123  SYPLG_JOIN1:TEMP_ALARM_C                                               sub                                                             
124  SYPLG_JOIN1:TEMP_ALARM                                                 bi                                                              
125  SYPLG_JOIN2:TEMP_ALARM_C                                               sub                                                             
126  SYPLG_JOIN2:TEMP_ALARM                                                 bi                                                              
127  R6PLG_CABLE:TEMP_ALARM_C                                               sub                                                             
128  R6PLG_CABLE:TEMP_ALARM                                                 bi                                                              
129  CAPRM_CABLE:TEMP_ALARM_C                                               sub                                                             
130  CAPRM_CABLE:TEMP_ALARM                                                 bi                                                              
131  PLUG_AIR:TEMP_ALARM_C                                                  sub                                                             
132  PLUG_AIR:TEMP_ALARM                                                    bi                                                              
133  CAPRM_AIR:TEMP_ALARM_C                                                 sub                                                             
134  CAPRM_AIR:TEMP_ALARM                                                   bi                                                              
135  PLTRM_AIR:TEMP_ALARM_C                                                 sub                                                             
136  PLTRM_AIR:TEMP_ALARM                                                   bi                                                              
137  DC_EARTH:FAULT_C                                                       sub                                                             
138  DC_EARTH:FAULT                                                         bi                                                              
139  MAGNET:WFLOW_TRIP_C                                                    sub                                                             
140  MAGNET:WFLOW_TRIP                                                      bi                                                              
141  MAGNET:TEMP_TRIP_C                                                     sub                                                             
142  MAGNET:TEMP_TRIP                                                       bi                                                              
143  WARN:MOTOR_LOAD_C                                                      sub                                                             
144  WARN:MOTOR_LOAD                                                        bi                                                              
145  WARN:VIBRATION_C                                                       sub                                                             
146  WARN:VIBRATION                                                         bi                                                              
147  WARN:CABLE_TEMP_C                                                      sub                                                             
148  WARN:CABLE_TEMP                                                        bi                                                              
149  WARN:MOTALT_TEMP_C                                                     sub                                                             
150  WARN:MOTALT_TEMP                                                       bi                                                              
151  WARN:DCBIAS_TEMP_C                                                     sub                                                             
152  WARN:DCBIAS_TEMP                                                       bi                                                              
153  WARN:CHOKEOIL_TEMP_C                                                   sub                                                             
154  WARN:CHOKEOIL_TEMP                                                     bi                                                              
155  WARN:CHOKEOIL_FLOW_C                                                   sub                                                             
156  WARN:CHOKEOIL_FLOW                                                     bi                                                              
157  WARN:CHOKEOIL_FANS_C                                                   sub                                                             
158  WARN:CHOKEOIL_FANS                                                     bi                                                              
159  271:MULTIPLE                                                           dfanout                 redirection channel reg 271             
160  271:MULTIPLE_SUB1                                                      dfanout                                                         
161  271:MULTIPLE_SUB2                                                      dfanout                                                         
162  DC_BIAS:ALARM:INTERLOCK_C                                              sub                                                             
163  DC_BIAS:ALARM:INTERLOCK                                                bi                                                              
164  DC_BIAS:ALARM:DOOR1_C                                                  sub                                                             
165  DC_BIAS:ALARM:DOOR1                                                    bi                                                              
166  DC_BIAS:ALARM:DOOR2_C                                                  sub                                                             
167  DC_BIAS:ALARM:DOOR2                                                    bi                                                              
168  DC_BIAS:ALARM:PERSONNEL_KEY1_C                                         sub                                                             
169  DC_BIAS:ALARM:PERSONNEL_KEY1                                           bi                                                              
170  DC_BIAS:ALARM:PERSONNEL_KEY2_C                                         sub                                                             
171  DC_BIAS:ALARM:PERSONNEL_KEY2                                           bi                                                              
172  DC_BIAS:ALARM:CASTELL_KEY_C                                            sub                                                             
173  DC_BIAS:ALARM:CASTELL_KEY                                              bi                                                              
174  DC_BIAS:ALARM:MAGWATER_FLOW_C                                          sub                                                             
175  DC_BIAS:ALARM:MAGWATER_FLOW                                            bi                                                              
176  DC_BIAS:ALARM:MAG_OVERTEMP_C                                           sub                                                             
177  DC_BIAS:ALARM:MAG_OVERTEMP                                             bi                                                              
178  DC_BIAS:ALARM:EMERGENCY_STOP_C                                         sub                                                             
179  DC_BIAS:ALARM:EMERGENCY_STOP                                           bi                                                              
180  DC_BIAS:ALARM:CHOKE_INTERLOCKS_C                                       sub                                                             
181  DC_BIAS:ALARM:CHOKE_INTERLOCKS                                         bi                                                              
182  DC_BIAS:ALARM:TRANSFORMER_DOOR_C                                       sub                                                             
183  DC_BIAS:ALARM:TRANSFORMER_DOOR                                         bi                                                              
184  DC_BIAS:ALARM:TRENCHCHOKE_DOOR_C                                       sub                                                             
185  DC_BIAS:ALARM:TRENCHCHOKE_DOOR                                         bi                                                              
186  DC_BIAS:ALARM:RELAY2_OVERTEMP_C                                        sub                                                             
187  DC_BIAS:ALARM:RELAY2_OVERTEMP                                          bi                                                              
188  DC_BIAS:ALARM:TRENCHCHOKE_INTERLOCK_C                                  sub                                                             
189  DC_BIAS:ALARM:TRENCHCHOKE_INTERLOCK                                    bi                                                              
190  DC_BIAS:ALARM:SPARE1_C                                                 sub                                                             
191  DC_BIAS:ALARM:SPARE1                                                   bi                                                              
192  DC_BIAS:ALARM:SPARE2_C                                                 sub                                                             
193  DC_BIAS:ALARM:SPARE2                                                   bi                                                              
194  272:MULTIPLE                                                           dfanout                 redirection channel reg 272             
195  272:MULTIPLE_SUB1                                                      dfanout                                                         
196  272:MULTIPLE_SUB2                                                      dfanout                                                         
197  AC_UPS:ALARM:INTERLOCK_C                                               sub                                                             
198  AC_UPS:ALARM:INTERLOCK                                                 bi                                                              
199  AC_UPS:ALARM:BRENTFORD_BIAS_C                                          sub                                                             
200  AC_UPS:ALARM:BRENTFORD_BIAS                                            bi                                                              
201  AC_UPS:ALARM:DC_BIAS_OFF_C                                             sub                                                             
202  AC_UPS:ALARM:DC_BIAS_OFF                                               bi                                                              
203  AC_UPS:ALARM:STORAGERING_KEYON_C                                       sub                                                             
204  AC_UPS:ALARM:STORAGERING_KEYON                                         bi                                                              
205  AC_UPS:ALARM:SPARE1_C                                                  sub                                                             
206  AC_UPS:ALARM:SPARE1                                                    bi                                                              
207  AC_UPS:ALARM:DC_HIGH_C                                                 sub                                                             
208  AC_UPS:ALARM:DC_HIGH                                                   bi                                                              
209  AC_UPS:ALARM:COMMS_LOST_C                                              sub                                                             
210  AC_UPS:ALARM:COMMS_LOST                                                bi                                                              
211  AC_UPS:ALARM:FANS_OFF_C                                                sub                                                             
212  AC_UPS:ALARM:FANS_OFF                                                  bi                                                              
213  AC_UPS:ALARM:TRENCHCHOKE_INTERLOCKS_C                                  sub                                                             
214  AC_UPS:ALARM:TRENCHCHOKE_INTERLOCKS                                    bi                                                              
215  AC_UPS:ALARM:CHOKE_INTERLOCKS_C                                        sub                                                             
216  AC_UPS:ALARM:CHOKE_INTERLOCKS                                          bi                                                              
217  AC_UPS:ALARM:SPARE2_C                                                  sub                                                             
218  AC_UPS:ALARM:SPARE2                                                    bi                                                              
219  AC_UPS:ALARM:SPARE3_C                                                  sub                                                             
220  AC_UPS:ALARM:SPARE3                                                    bi                                                              
221  AC_UPS:ALARM:SPARE4_C                                                  sub                                                             
222  AC_UPS:ALARM:SPARE4                                                    bi                                                              
223  AC_UPS:ALARM:SPARE5_C                                                  sub                                                             
224  AC_UPS:ALARM:SPARE5                                                    bi                                                              
225  AC_UPS:ALARM:SPARE6_C                                                  sub                                                             
226  AC_UPS:ALARM:SPARE6                                                    bi                                                              
227  AC_UPS:ALARM:SPARE7_C                                                  sub                                                             
228  AC_UPS:ALARM:SPARE7                                                    bi                                                              