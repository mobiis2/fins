 
  Database(P): RKICK
  IP Address: None
 
NUM  PV Name                                                       Address  Type        Scan        DESC                                    
  1  SYSTEM:LOAD:HPARAM1                                                    longout                                                         
  2  SYSTEM:STATUS                                                          longout                 Remote database status                  
  3  SYSTEM:STATUS:HPARAM1                                                  longout                                                         
  4  SYSTEM:ERROR                                                           longout                 System error Channel                    
  5  SYSTEM:PCC                                                             longout                 PCC Channel                             
  6  SPIB1_REA5:MULTIPLY                                                    dfanout                 Redirection channel list - SPIB1 REA5   
  7  SPIB1_REA5:STATUS                                                      longin      1 second    READ SPIB GROUP 5                       
  8  K1P:OIL_ESTOP_C                                                        sub                     KICKER 1P OIL SYSTEM EMERGENCY STOP sub 
  9  K1P:OIL_ESTOP                                                          bi                      KICKER 1P OIL SYSTEM EMERGENCY STOP     
 10  K1P:PSU_WATER_C                                                        sub                     KICKER 1P POWER SUPPLY WATER sub        
 11  K1P:PSU_WATER                                                          bi                      KICKER 1P POWER SUPPLY WATER            
 12  K1P:THY_HDEL_C                                                         sub                     KICKER 1P THYRATRON HEATER DELAY sub    
 13  K1P:THY_HDEL                                                           bi                      KICKER 1P THYRATRON HEATER DELAY        
 14  K1P:THY_AUX_C                                                          sub                     KICKER 1P THYRATRON AUXILLIARIES sub    
 15  K1P:THY_AUX                                                            bi                      KICKER 1P THYRATRON AUXILLIARIES        
 16  K1P:OIL_FAULT_C                                                        sub                     KICKER 1P OIL SYSTEM FAULT sub          
 17  K1P:OIL_FAULT                                                          bi                      KICKER 1P OIL SYSTEM FAULT              
 18  K1P:PS_STOP_C                                                          sub                     KICKER 1P POWER SUPPLY EMERGENCY STOP sub
 19  K1P:PS_STOP                                                            bi                      KICKER 1P POWER SUPPLY EMERGENCY STOP   
 20  K1P:THY_GBC_C                                                          sub                     KICKER 1P THYRATRON GRID BOX COVER sub  
 21  K1P:THY_GBC                                                            bi                      KICKER 1P THYRATRON GRID BOX COVER      
 22  K1P:PFN_EARTH_C                                                        sub                     KICKER 1P PFN EARTH SWITCH sub          
 23  K1P:PFN_EARTH                                                          bi                      KICKER 1P PFN EARTH SWITCH              
 24  SPIB1_REA6:MULTIPLY                                                    dfanout                 Redirection channel list - SPIB1 REA5   
 25  SPIB1_REA6:STATUS                                                      longin      1 second    READ SPIB GROUP 6                       
 26  K1P:HV_ON_C                                                            sub                     KICKER 1P HV ON sub                     
 27  K1P:HV_ON                                                              bi                      KICKER 1P HV ON                         
 28  K1P:INTLK_COMP_C                                                       sub                     KICKER 1P INTERLOCKS COMPLETE sub       
 29  K1P:INTLK_COMP                                                         bi                      KICKER 1P INTERLOCKS COMPLETE           
 30  SPIB1_REA7:MULTIPLY                                                    dfanout                 Redirection channel list - SPIB1 REA7   
 31  SPIB1_REA7:STATUS                                                      longin      1 second    READ SPIB GROUP 7                       
 32  K1N:OIL_ESTOP_C                                                        sub                     KICKER 1N RESISTOR OIL FLOW sub         
 33  K1N:OIL_ESTOP                                                          bi                      KICKER 1N RESISTOR OIL FLOW             
 34  K1N:PSU_WATER_C                                                        sub                     KICKER 1N POWER SUPPLY WATER sub        
 35  K1N:PSU_WATER                                                          bi                      KICKER 1N POWER SUPPLY WATER            
 36  K1N:THY_HDEL_C                                                         sub                     KICKER 1N THYRATRON HEATER DELAY sub    
 37  K1N:THY_HDEL                                                           bi                      KICKER 1N THYRATRON HEATER DELAY        
 38  K1N:THY_AUX_C                                                          sub                     KICKER 1N THYRATRON AUXILLIARIES sub    
 39  K1N:THY_AUX                                                            bi                      KICKER 1N THYRATRON AUXILLIARIES        
 40  K1N:OIL_FAULT_C                                                        sub                     KICKER 1N OIL SYSTEM FAULT sub          
 41  K1N:OIL_FAULT                                                          bi                      KICKER 1N OIL SYSTEM FAULT              
 42  K1N:PS_STOP_C                                                          sub                     KICKER 1N POWER SUPPLY EMERGENCY STOP sub
 43  K1N:PS_STOP                                                            bi                      KICKER 1N POWER SUPPLY EMERGENCY STOP   
 44  K1N:THY_GBC_C                                                          sub                     KICKER 1N THYRATRON GRID BOX COVER sub  
 45  K1N:THY_GBC                                                            bi                      KICKER 1N THYRATRON GRID BOX COVER      
 46  K1N:PFN_EARTH_C                                                        sub                     KICKER 1N PFN EARTH SWITCH sub          
 47  K1N:PFN_EARTH                                                          bi                      KICKER 1N PFN EARTH SWITCH              
 48  SPIB1_REA8:MULTIPLY                                                    dfanout                 Redirection channel list - SPIB1 REA8   
 49  SPIB1_REA8:STATUS                                                      longin      1 second    READ SPIB GROUP 8                       
 50  K1N:HV_ON_C                                                            sub                     KICKER 1N HV ON sub                     
 51  K1N:HV_ON                                                              bi                      KICKER 1N HV ON                         
 52  K1N:INTLK_COMP_C                                                       sub                     KICKER 1N INTERLOCKS COMPLETE sub       
 53  K1N:INTLK_COMP                                                         bi                      KICKER 1N INTERLOCKS COMPLETE           
 54  SPIB1_REA9:MULTIPLY                                                    dfanout                 Redirection channel list - SPIB1 REA9   
 55  SPIB1_REA9:STATUS                                             60       longin      1 second    READ SPIB GROUP 9                       
 56  KIK:SYNC_WATER_C                                                       sub                     KICKER SYNCHROTRON WATER INTLK sub      
 57  KIK:SYNC_WATER                                                         bi                      KICKER SYNCHROTRON WATER INTLK          
 58  KIK:SYNC_VAC_C                                                         sub                     KICKER SYNCHROTRON VACUUM INTLK sub     
 59  KIK:SYNC_VAC                                                           bi                      KICKER SYNCHROTRON VACUUM INTLK         
 60  K1PN:CONT_CHASS_POW_C                                                  sub                     CONTROL CHASSIS POWER ON sub            
 61  K1PN:CONT_CHASS_POW                                                    bi                      CONTROL CHASSIS POWER ON                
 62  K1PN:RESET_C                                                           sub                     PSUs RESETTING sub                      
 63  K1PN:RESET                                                             bi                      PSUs RESETTING                          
 64  SPIB2_REA5:MULTIPLY                                                    dfanout                 Redirection channel list - SPIB2 REA5   
 65  SPIB2_REA5:STATUS                                                      longin      1 second    READ SPIB GROUP 5                       
 66  K2P:OIL_ESTOP_C                                                        sub                     KICKER 2P OIL SYSTEM EMERGENCY STOP sub 
 67  K2P:OIL_ESTOP                                                          bi                      KICKER 2P OIL SYSTEM EMERGENCY STOP     
 68  K2P:PSU_WATER_C                                                        sub                     KICKER 2P POWER SUPPLY WATER sub        
 69  K2P:PSU_WATER                                                          bi                      KICKER 2P POWER SUPPLY WATER            
 70  K2P:THY_HDEL_C                                                         sub                     KICKER 2P THYRATRON HEATER DELAY sub    
 71  K2P:THY_HDEL                                                           bi                      KICKER 2P THYRATRON HEATER DELAY        
 72  K2P:THY_AUX_C                                                          sub                     KICKER 2P THYRATRON AUXILLIARIES sub    
 73  K2P:THY_AUX                                                            bi                      KICKER 2P THYRATRON AUXILLIARIES        
 74  K2P:OIL_FAULT_C                                                        sub                     KICKER 2P OIL SYSTEM FAULT sub          
 75  K2P:OIL_FAULT                                                          bi                      KICKER 2P OIL SYSTEM FAULT              
 76  K2P:PS_STOP_C                                                          sub                     KICKER 2P POWER SUPPLY EMERGENCY STOP sub
 77  K2P:PS_STOP                                                            bi                      KICKER 2P POWER SUPPLY EMERGENCY STOP   
 78  K2P:THY_GBC_C                                                          sub                     KICKER 2P THYRATRON GRID BOX COVER sub  
 79  K2P:THY_GBC                                                            bi                      KICKER 2P THYRATRON GRID BOX COVER      
 80  K2P:PFN_EARTH_C                                                        sub                     KICKER 2P PFN EARTH SWITCH sub          
 81  K2P:PFN_EARTH                                                          bi                      KICKER 2P PFN EARTH SWITCH              
 82  SPIB2_REA6:MULTIPLY                                                    dfanout                 Redirection channel list - SPIB2 REA5   
 83  SPIB2_REA6:STATUS                                                      longin      1 second    READ SPIB GROUP 6                       
 84  K2P:HV_ON_C                                                            sub                     KICKER 2P HV ON sub                     
 85  K2P:HV_ON                                                              bi                      KICKER 2P HV ON                         
 86  K2P:INTLK_COMP_C                                                       sub                     KICKER 2P INTERLOCKS COMPLETE sub       
 87  K2P:INTLK_COMP                                                         bi                      KICKER 2P INTERLOCKS COMPLETE           
 88  SPIB2_REA7:MULTIPLY                                                    dfanout                 Redirection channel list - SPIB1 REA7   
 89  SPIB2_REA7:STATUS                                                      longin      1 second    READ SPIB GROUP 7                       
 90  K2N:OIL_ESTOP_C                                                        sub                     KICKER 2N OIL SYSTEM EMERGENCY STOP sub 
 91  K2N:OIL_ESTOP                                                          bi                      KICKER 2N OIL SYSTEM EMERGENCY STOP     
 92  K2N:PSU_WATER_C                                                        sub                     KICKER 2N POWER SUPPLY WATER sub        
 93  K2N:PSU_WATER                                                          bi                      KICKER 2N POWER SUPPLY WATER            
 94  K2N:THY_HDEL_C                                                         sub                     KICKER 2N THYRATRON HEATER DELAY sub    
 95  K2N:THY_HDEL                                                           bi                      KICKER 2N THYRATRON HEATER DELAY        
 96  K2N:THY_AUX_C                                                          sub                     KICKER 2N THYRATRON AUXILLIARIES sub    
 97  K2N:THY_AUX                                                            bi                      KICKER 2N THYRATRON AUXILLIARIES        
 98  K2N:OIL_FAULT_C                                                        sub                     KICKER 2N OIL SYSTEM FAULT sub          
 99  K2N:OIL_FAULT                                                          bi                      KICKER 2N OIL SYSTEM FAULT              
100  K2N:PS_STOP_C                                                          sub                     KICKER 2N POWER SUPPLY EMERGENCY STOP sub
101  K2N:PS_STOP                                                            bi                      KICKER 2N POWER SUPPLY EMERGENCY STOP   
102  K2N:THY_GBC_C                                                          sub                     KICKER 2N THYRATRON GRID BOX COVER sub  
103  K2N:THY_GBC                                                            bi                      KICKER 2N THYRATRON GRID BOX COVER      
104  K2N:PFN_EARTH_C                                                        sub                     KICKER 2N PFN EARTH SWITCH sub          
105  K2N:PFN_EARTH                                                          bi                      KICKER 2N PFN EARTH SWITCH              
106  SPIB2_REA8:MULTIPLY                                                    dfanout                 Redirection channel list - SPIB1 REA8   
107  SPIB2_REA8:STATUS                                                      longin      1 second    READ SPIB GROUP 8                       
108  K2N:HV_ON_C                                                            sub                     KICKER 2N HV ON sub                     
109  K2N:HV_ON                                                              bi                      KICKER 2N HV ON                         
110  K2N:INTLK_COMP_C                                                       sub                     KICKER 2N INTERLOCKS COMPLETE sub       
111  K2N:INTLK_COMP                                                         bi                      KICKER 2N INTERLOCKS COMPLETE           
112  SPIB2_REA9:MULTIPLY                                                    dfanout                 Redirection channel list - SPIB2 REA9   
113  SPIB2_REA9:STATUS                                             63       longin      1 second    READ SPIB GROUP 9                       
114  K2PN:CONT_CHASS_POW_C                                                  sub                     K2 CONTROL CHASSIS POWER sub            
115  K2PN:CONT_CHASS_POW                                                    bi                      K2 CONTROL CHASSIS POWER                
116  K2PN:RESET_C                                                           sub                     PSUs RESETTING sub                      
117  K2PN:RESET                                                             bi                      PSUs RESETTING                          
118  SPIB3_REA5:MULTIPLY                                                    dfanout                 Redirection channel list - SPIB3 REA5   
119  SPIB3_REA5:STATUS                                                      longin      1 second    READ SPIB GROUP 5                       
120  K3P:OIL_ESTOP_C                                                        sub                     KICKER 3P OIL SYSTEM EMERGENCY STOP sub 
121  K3P:OIL_ESTOP                                                          bi                      KICKER 3P OIL SYSTEM EMERGENCY STOP     
122  K3P:PSU_WATER_C                                                        sub                     KICKER 3P POWER SUPPLY WATER sub        
123  K3P:PSU_WATER                                                          bi                      KICKER 3P POWER SUPPLY WATER            
124  K3P:THY_HDEL_C                                                         sub                     KICKER 3P THYRATRON HEATER DELAY sub    
125  K3P:THY_HDEL                                                           bi                      KICKER 3P THYRATRON HEATER DELAY        
126  K3P:THY_AUX_C                                                          sub                     KICKER 3P THYRATRON AUXILLIARIES sub    
127  K3P:THY_AUX                                                            bi                      KICKER 3P THYRATRON AUXILLIARIES        
128  K3P:OIL_FAULT_C                                                        sub                     KICKER 3P OIL SYSTEM FAULT sub          
129  K3P:OIL_FAULT                                                          bi                      KICKER 3P OIL SYSTEM FAULT              
130  K3P:PS_STOP_C                                                          sub                     KICKER 3P POWER SUPPLY EMERGENCY STOP sub
131  K3P:PS_STOP                                                            bi                      KICKER 3P POWER SUPPLY EMERGENCY STOP   
132  K3P:THY_GBC_C                                                          sub                     KICKER 3P THYRATRON GRID BOX COVER sub  
133  K3P:THY_GBC                                                            bi                      KICKER 3P THYRATRON GRID BOX COVER      
134  K3P:PFN_EARTH_C                                                        sub                     KICKER 3P PFN EARTH SWITCH sub          
135  K3P:PFN_EARTH                                                          bi                      KICKER 3P PFN EARTH SWITCH              
136  SPIB3_REA6:MULTIPLY                                                    dfanout                 Redirection channel list - SPIB3 REA5   
137  SPIB3_REA6:STATUS                                                      longin      1 second    READ SPIB GROUP 6                       
138  K3P:HV_ON_C                                                            sub                     KICKER 1P HV ON sub                     
139  K3P:HV_ON                                                              bi                      KICKER 1P HV ON                         
140  K3P:INTLK_COMP_C                                                       sub                     KICKER 1P INTERLOCKS COMPLETE sub       
141  K3P:INTLK_COMP                                                         bi                      KICKER 1P INTERLOCKS COMPLETE           
142  SPIB3_REA7:MULTIPLY                                                    dfanout                 Redirection channel list - SPIB3 REA7   
143  SPIB3_REA7:STATUS                                                      longin      1 second    READ SPIB GROUP 7                       
144  K3N:OIL_ESTOP_C                                                        sub                     KICKER 3N OIL SYSTEM EMERGENCY STOP sub 
145  K3N:OIL_ESTOP                                                          bi                      KICKER 3N OIL SYSTEM EMERGENCY STOP     
146  K3N:PSU_WATER_C                                                        sub                     KICKER 3N POWER SUPPLY WATER sub        
147  K3N:PSU_WATER                                                          bi                      KICKER 3N POWER SUPPLY WATER            
148  K3N:THY_HDEL_C                                                         sub                     KICKER 3N THYRATRON HEATER DELAY sub    
149  K3N:THY_HDEL                                                           bi                      KICKER 3N THYRATRON HEATER DELAY        
150  K3N:THY_AUX_C                                                          sub                     KICKER 3N THYRATRON AUXILLIARIES sub    
151  K3N:THY_AUX                                                            bi                      KICKER 3N THYRATRON AUXILLIARIES        
152  K3N:OIL_FAULT_C                                                        sub                     KICKER 3N OIL SYSTEM FAULT sub          
153  K3N:OIL_FAULT                                                          bi                      KICKER 3N OIL SYSTEM FAULT              
154  K3N:PS_STOP_C                                                          sub                     KICKER 3N POWER SUPPLY EMERGENCY STOP sub
155  K3N:PS_STOP                                                            bi                      KICKER 3N POWER SUPPLY EMERGENCY STOP   
156  K3N:THY_GBC_C                                                          sub                     KICKER 3N THYRATRON GRID BOX COVER sub  
157  K3N:THY_GBC                                                            bi                      KICKER 3N THYRATRON GRID BOX COVER      
158  K3N:PFN_EARTH_C                                                        sub                     KICKER 3N PFN EARTH SWITCH sub          
159  K3N:PFN_EARTH                                                          bi                      KICKER 3N PFN EARTH SWITCH              
160  SPIB3_REA8:MULTIPLY                                                    dfanout                 Redirection channel list - SPIB3 REA8   
161  SPIB3_REA8:STATUS                                                      longin      1 second    READ SPIB GROUP 8                       
162  K3N:HV_ON_C                                                            sub                     KICKER 3N HV ON sub                     
163  K3N:HV_ON                                                              bi                      KICKER 3N HV ON                         
164  K3N:INTLK_COMP_C                                                       sub                     KICKER 3N INTERLOCKS COMPLETE sub       
165  K3N:INTLK_COMP                                                         bi                      KICKER 3N INTERLOCKS COMPLETE           
166  SPIB3_REA9:MULTIPLY                                                    dfanout                 Redirection channel list - SPIB3 REA9   
167  SPIB3_REA9:STATUS                                             66       longin      1 second    READ SPIB GROUP 9                       
168  K3PN:CONT_CHASS_POW_C                                                  sub                     K3 CONTROL CHASSIS POWER sub            
169  K3PN:CONT_CHASS_POW                                                    bi                      K3 CONTROL CHASSIS POWER                
170  K3PN:RESET_C                                                           sub                     PSUs RESETTING sub                      
171  K3PN:RESET                                                             bi                      PSUs RESETTING                          
172  K1P:REA_VOLTS                                                          ao                      KICKER 1 POS VOLTAGE READ               
173  K1N:REA_VOLTS                                                          ao                      KICKER 1 NEG VOLTAGE READ               
174  K2P:REA_VOLTS                                                          ao                      KICKER 2 POS VOLTAGE READ               
175  K2N:REA_VOLTS                                                          ao                      KICKER 2 NEG VOLTAGE READ               
176  K3P:REA_VOLTS                                                          ao                      KICKER 3 POS VOLTAGE READ               
177  K3N:REA_VOLTS                                                          ao                      KICKER 1 POS VOLTAGE READ               
178  K1P:REA_DVOLTS                                                         ao                      KICKER 1 POS VOLTAGE READ               
179  K1N:REA_DVOLTS                                                         ao                      KICKER 1 NEG VOLTAGE READ               
180  K2P:REA_DVOLTS                                                         ao                      KICKER 2 POS VOLTAGE READ               
181  K2N:REA_DVOLTS                                                         ao                      KICKER 2 NEG VOLTAGE READ               
182  K3P:REA_DVOLTS                                                         ao                      KICKER 3 POS VOLTAGE READ               
183  K3N:REA_DVOLTS                                                         ao                      KICKER 1 POS VOLTAGE READ               
184  K1P:REA_CUR                                                            ao                      KICKER 1 POS CURRENT READ               
185  K1N:REA_CUR                                                            ao                      KICKER 1 NEG CURRENT READ               
186  K2P:REA_CUR                                                            ao                      KICKER 2 POS CURRENT READ               
187  K2N:REA_CUR                                                            ao                      KICKER 2 NEG CURRENT READ               
188  K3P:REA_CUR                                                            ao                      KICKER 3 POS CURRENT READ               
189  K3N:REA_CUR                                                            ao                      KICKER 1 POS CURRENT READ               
190  K1P:REA_KW                                                             ao                      KICKER 1 POS POWER READ                 
191  K1N:REA_KW                                                             ao                      KICKER 1 NEG POWER READ                 
192  K2P:REA_KW                                                             ao                      KICKER 2 POS POWER READ                 
193  K2N:REA_KW                                                             ao                      KICKER 2 NEG POWER READ                 
194  K3P:REA_KW                                                             ao                      KICKER 3 POS POWER READ                 
195  K3N:REA_KW                                                             ao                      KICKER 1 POS POWER READ                 
196  K1NP:SET_VOLTS                                                         dfanout                 KICKER 1 P+N GANGED OPERATION           
197  K2NP:SET_VOLTS                                                         dfanout                 KICKER 1 P+N GANGED OPERATION           
198  K3NP:SET_VOLTS                                                         dfanout                 KICKER 1 P+N GANGED OPERATION           
199  K1P:SET_VOLTS                                                          ao                      KICKER 1 POS VOLTAGE SET                
200  K1N:SET_VOLTS                                                          ao                      KICKER 1 NEG VOLTAGE SET                
201  K2P:SET_VOLTS                                                          ao                      KICKER 2 POS VOLTAGE SET                
202  K2N:SET_VOLTS                                                          ao                      KICKER 2 NEG VOLTAGE SET                
203  K3P:SET_VOLTS                                                          ao                      KICKER 3 POS VOLTAGE SET                
204  K3N:SET_VOLTS                                                          ao                      KICKER 3 NEG VOLTAGE SET                
205  K1P:LOC_DEM                                                            longout                 KICKER 1 POS VOLTAGE LOCAL DEMAND       
206  K1P:LOC_SOFT                                                  42       ao                      KICKER 1 POS VOLTAGE LOCAL VALUE        
207  K1N:LOC_DEM                                                            longout                 KICKER 1 NEG VOLTAGE LOCAL DEMAND       
208  K1N:LOC_SOFT                                                  43       ao                      KICKER 1 NEG VOLTAGE LOCAL VALUE        
209  K2P:LOC_DEM                                                            longout                 KICKER 2 POS VOLTAGE LOCAL DEMAND       
210  K2P:LOC_SOFT                                                  44       ao                      KICKER 2 POS VOLTAGE LOCAL VALUE        
211  K2N:LOC_DEM                                                            longout                 KICKER 2 NEG VOLTAGE LOCAL DEMAND       
212  K2N:LOC_SOFT                                                  45       ao                      KICKER 2 NEG VOLTAGE LOCAL VALUE        
213  K3P:LOC_DEM                                                            longout                 KICKER 3 POS VOLTAGE LOCAL DEMAND       
214  K3P:LOC_SOFT                                                  46       ao                      KICKER 3 POS VOLTAGE LOCAL VALUE        
215  K3N:LOC_DEM                                                            longout                 KICKER 3 NEG VOLTAGE LOCAL DEMAND       
216  K3N:LOC_SOFT                                                  47       ao                      KICKER 3 NEG VOLTAGE LOCAL VALUE        
217  K1P:PS_ON                                                     61       bo                      KICKER 1 POS HVI ON/OFF                 
218  K1N:PS_ON                                                     63       bo                      KICKER 1 NEG HVI ON/OFF                 
219  K1PN:PS_RESET                                                 60       bo                      KICKER 1 GHVI RESET                     
220  K2P:PS_ON                                                     64       bo                      KICKER 2 POS HVI ON/OFF                 
221  K2N:PS_ON                                                     66       bo                      KICKER 2 NEG HVI ON/OFF                 
222  K2PN:PS_RESET                                                 63       bo                      KICKER 2 GHVI RESET                     
223  K3P:PS_ON                                                     67       bo                      KICKER 3 POS HVI ON/OFF                 
224  K3N:PS_ON                                                     0        bo                      KICKER 3 NEG HVI ON/OFF                 
225  K3PN:PS_RESET                                                 66       bo                      KICKER 3 GHVI RESET                     
226  ALL:CRASH_OFF                                                 60       bo                      CRASH OFF ALL KICKERS                   
227  SERC1:SERCOM_INIT                                                      longout                 Local STEbus Channel for card init      
228  SPIB1:LOC_PS_ON_RESET                                                  longout                 KICKER 1 GHVI ON/OFF AND RESET          
229  K1P:SOFT_PS_ON                                                60       bo                      KICKER 1P GHVI ON/OFF                   
230  K1N:SOFT_PS_ON                                                61       bo                      KICKER 1N GHVI ON/OFF                   
231  K1PN:SOFT_RESET_PS                                            62       bo                      KICKER 1 GHVI RESET                     
232  SPIB2:LOC_PS_ON_RESET                                                  longout                 KICKER 2 GHVI ON/OFF AND RESET          
233  K2P:SOFT_PS_ON                                                63       bo                      KICKER 2P GHVI ON/OFF                   
234  K2N:SOFT_PS_ON                                                64       bo                      KICKER 2N GHVI ON/OFF                   
235  K2PN:SOFT_RESET_PS                                            65       bo                      KICKER 2 GHVI RESET                     
236  SPIB3:LOC_PS_ON_RESET                                                  longout                 KICKER 3 GHVI ON/OFF AND RESET          
237  K3P:SOFT_PS_ON                                                66       bo                      KICKER 3P GHVI ON/OFF                   
238  K3N:SOFT_PS_ON                                                67       bo                      KICKER 3N GHVI ON/OFF                   
239  K3PN:SOFT_RESET_PS                                            68       bo                      KICKER 3 GHVI RESET                     
240  CHARGE_ENABLE_TRIGGER:DELP                                             longout                 STEbus TIMER 1 Channel 1 set Delta P    
241  CHARGE_ENABLE_TRIGGER:MS_K                                             longout                 STEbus TIMER 1 Channel 1 set MS/K       
242  CHARGE_ENABLE_TRIGGER:ENB                                              bo                      STEbus TIMER 1 Channel 1 enable         
243  CHARGE_ENABLE_TRIGGER:SST                                              bo                      STEbus TIMER 1 Channel 1 single-shot enable
244  CHARGE_ENABLE_TRIGGER:MMS                                              longout                 STEbus TIMER 1 Channel 1 set MMS        
245  CHARGE_ENABLE_TRIGGER:STA                                              longout                 STEbus TIMER 1 Channel 1 status         
246  CHARGE_INHIBIT_TRIGGER:DELP                                            longout                 STEbus TIMER 1 Channel 2 set Delta P    
247  CHARGE_INHIBIT_TRIGGER:MS_K                                            longout                 STEbus TIMER 1 Channel 2 set MS/K       
248  CHARGE_INHIBIT_TRIGGER:ENB                                             bo                      STEbus TIMER 1 Channel 2 enable         
249  CHARGE_INHIBIT_TRIGGER:SST                                             bo                      STEbus TIMER 1 Channel 2 single-shot enable
250  CHARGE_INHIBIT_TRIGGER:MMS                                             longout                 STEbus TIMER 1 Channel 2 set MMS        
251  CHARGE_INHIBIT_TRIGGER:STA                                             longout                 STEbus TIMER 1 Channel 2 status         
252  BACKUP_TRIGGER:DELP                                                    longout                 STEbus TIMER 2 Channel 1 set Delta P    
253  BACKUP_TRIGGER:MS_K                                                    longout                 STEbus TIMER 2 Channel 1 set MS/K       
254  BACKUP_TRIGGER:ENB                                                     bo                      STEbus TIMER 2 Channel 1 enable         
255  BACKUP_TRIGGER:SST                                                     bo                      STEbus TIMER 2 Channel 1 single-shot enable
256  BACKUP_TRIGGER:MMS                                                     longout                 STEbus TIMER 2 Channel 1 set MMS        
257  BACKUP_TRIGGER:STA                                                     longout                 STEbus TIMER 2 Channel 1 status         
258  PLLPSU_TRIGGER:DELP                                                    longout                 STEbus TIMER 2 Channel 2 set Delta P    
259  PLLPSU_TRIGGER:MS_K                                                    longout                 STEbus TIMER 2 Channel 2 set MS/K       
260  PLLPSU_TRIGGER:ENB                                                     bo                      STEbus TIMER 2 Channel 2 enable         
261  PLLPSU_TRIGGER:SST                                                     bo                      STEbus TIMER 2 Channel 2 single-shot enable
262  PLLPSU_TRIGGER:MMS                                                     longout                 STEbus TIMER 2 Channel 2 set MMS        
263  PLLPSU_TRIGGER:STA                                                     longout                 STEbus TIMER 2 Channel 2 status         
264  PCC_SPIB:MULTIPLY                                                      dfanout                 Redirection channel list - PCC SPIB REA0
265  PCC_SPIB:MCR_LOCAL                                                     longin      1 second    READ SPIB GROUP 0                       
266  K1:MCR_LOC_C                                                           sub                     KICKER 1 MCR LOCAL sub                  
267  K1:MCR_LOC                                                             bi                      KICKER 1 MCR LOCAL                      
268  K2:MCR_LOC_C                                                           sub                     KICKER 2 MCR LOCAL sub                  
269  K2:MCR_LOC                                                             bi                      KICKER 2 MCR LOCAL                      
270  K3:MCR_LOC_C                                                           sub                     KICKER 3 MCR LOCAL sub                  
271  K3:MCR_LOC                                                             bi                      KICKER 3 MCR LOCAL                      